
# mqtt5 - Reference

---

## Constructor

#### mqtt5 ( {params} )

The constructor of MQTT client requires a table with the following fields:

- `socket`: an object supplying the methods `close`, `read` & `write`
  (a subset of the [cqueues.socket](https://25thandclement.com/~william/projects/cqueues.html) API)
- `logger`: an optional object supplying the methods `debug` & `error`
  (a subset of the [LuaLogging](https://lunarmodules.github.io/lualogging/) API)
- `on_connect`: an optional callback function
- `on_message`: an optional callback function
- `on_disconnect`: an optional callback function
- `on_error`: an optional callback function

## Instance methods

#### connect ( [{options} [, { prop_id1, prop_value1, ... }]] )

Send a [CONNECT](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901033)
packet with an optional set of [Properties](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901046),
or throws an error when invalid parameter.

The optional table `options` may contains the following fields:

- `clean`: a boolean [Clean Start](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901039)
- `keep_alive`: an integer [Keep Alive](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901045),
  the default value is 0
- `id`: a string [Client Identifier](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901059),
  could be auto-generated
- `will`: a table (see below)
- `username`: a string [User Name](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901071)
- `password`: a string [Password](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901072)

The array part of the table `will` contains the [Will Topic](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901069)
and the [Will Payload](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901070),
the hash part may contains the following fields:

- `qos`: an value in the set 0, 1 and 2, the default value is 0
- `retain`: a boolean
- `props`: a set of [Will Properties](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901060)

#### reconnect (socket)

Replays the call to `connect` with a new `socket`.

This method is typically called by the callback `on_error`.

#### publish ( topic, payload [, {options} [, { prop_id1, prop_value1, ... }]] )

Send a [PUBLISH](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901100)
packet with a [Topic Name](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901107),
a [Payload](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901119),
and an optional set of [Properties](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901109),
or throws an error when invalid parameter.

The optional table `options` may contains the following fields:

- `qos` with an value in the set 0, 1 and 2, the default value is 0
- `retain` with a boolean meaning

#### subscribe ( { filter1, options1, ... } [, { prop_id1, prop_value1, ... }] )

Send a [SUBSCRIBE](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901161)
packet with a non empty set of pair of [Topic Filters](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901241)
and [Subscription Options](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901169),
and an optional set of [Properties](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901164),
or throws an error when invalid parameter.

#### unsubscribe ( { filter1, ... } [, { prop_id1, prop_value1, ... }] )

Send a [UNSUBSCRIBE](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901179)
packet with a non empty list of [Topic Filters](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901241),
and an optional set of [Properties](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901182),
or throws an error when invalid parameter.

#### ping ()

Send a [PINGREQ](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901195)
packet.

#### disconnect ( [reason_code [, { prop_id1, prop_value1, ... }]] )

Send a [DISCONNECT](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901205)
packet with a [Reason Code](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901208),
and an optional set of [Properties](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901209),
the default Reason Code is 0,
or throws an error when invalid parameter.

#### auth ( reason_code [, { prop_id1, prop_value1, ... }] )

Send an [AUTH](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901217)
packet with a [Reason Code](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901220),
and an optional set of [Properties](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901221),
or throws an error when invalid parameter.

#### read ()

Waits and handles a packet from the server.
Returns a table describing this packet or `nil` and a message describing the reason of a disconnection
or an unsuccessful reconnection.

The table contains at least the field `type` with a string value.

The callback `on_connect (reason_code, session_present, props)` is called on CONNACK packet received.
If the connection is successful, this callback does typically some subscriptions.
This callback is not intended to return something.

The callback `on_message (topic, payload, props)` is called on each PUBLISH packet received.
This callback is not intended to return something.

The callback `on_disconnect (reason_code, props)` is called on DISCONNECT packet received.
This callback is not intended to return something.

The callback `on_error ()` is called when the connection is broken.
When everything is going well, this callback open a new connection,
calls the method `reconnect` with it and finally return a true value.
In bad case, this callback returns a false value, optionally followed by a message.

## Class functions & members

#### match (name, filter)

Checks if a [Topic Name](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901241)
matches a [Topic Filter](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901241).
Returns a boolean or throws an error when invalid parameter.

#### PORT

Gives the IANA registered port 1883 as a string.

#### PORT_TLS

Gives the IANA registered port 8883 as a string.

#### PROP

Gives a table with all [Property Identifier](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901029).

