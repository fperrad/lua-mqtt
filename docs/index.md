
# lua-mqtt

---

## Overview

lua-mqtt supplies two independents modules :

- __mqtt331__ : a client library for [MQTT 3.1.1](http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html)
- __mqtt5__ : a client library for [MQTT 5](https://docs.oasis-open.org/mqtt/mqtt/v5.0/mqtt-v5.0.html)

Their API are compatible as much as possible.

There are designed to work with any kind of socket library :
[luasocket](https://lunarmodules.github.io/luasocket/),
[luasec](https://github.com/brunoos/luasec/),
[cqueues](https://25thandclement.com/~william/projects/cqueues.html).

## Status

lua-mqtt is in beta stage.

It's developed for Lua 5.3 & 5.4 (and transpilable by [Teal](https://github.com/teal-language/tl)
to Lua 5.1 & 5.2).

## Download

lua-mqtt source can be downloaded from
[Framagit](https://framagit.org/fperrad/lua-mqtt).

The Teal type definition of these modules are available
[here](https://github.com/teal-language/teal-types/tree/master/types/lua-mqtt).

## Installation

lua-mqtt is available via LuaRocks:

```sh
luarocks install lua-mqtt
```

or manually, with:

```sh
make install
```

## Test

The test suite requires the module
[lua-TestAssertion](https://fperrad.frama.io/lua-testassertion/).

    make test

## Copyright and License

Copyright &copy; 2022-2024 Fran&ccedil;ois Perrad

This library is licensed under the terms of the MIT/X11 license, like Lua itself.
