
# mqtt311 - Reference

---

## Constructor

#### mqtt311 ( {params} )

The constructor of MQTT client requires a table with the following fields:

- `socket`: an object supplying the methods `close`, `read` & `write`
  (a subset of the [cqueues.socket](https://25thandclement.com/~william/projects/cqueues.html) API)
- `logger`: an optional object supplying the methods `debug` & `error`
  (a subset of the [LuaLogging](https://lunarmodules.github.io/lualogging/) API)
- `on_connect`: an optional callback function
- `on_message`: an optional callback function
- `on_error`: an optional callback function
- `max_packet_size`: an optional integer

## Instance methods

#### connect ( [{options}] )

Send a [CONNECT](http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718028)
packet,
or throws an error when invalid parameter.

The optional table `options` may contains the following fields:

- `clean`: a boolean [Clean Session](http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718030)
- `keep_alive`: an integer [Keep Alive](http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718030),
  the default value is 0
- `id`: a string [Client Identifier](http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718031),
  could be auto-generated when Clean Session
- `will`: a table (see below)
- `username`: a string [User Name](http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718031)
- `password`: a string [Password](http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718031)

The array part of the table `will` contains the [Will Topic](http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718031)
and the [Will Message](http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718031),
the hash part may contains the following fields:

- `qos`: an value in the set 0, 1 and 2, the default value is 0
- `retain`: a boolean

#### reconnect (socket)

Replays the call to `connect` with a new `socket`.

This method is typically called by the callback `on_error`.

#### publish ( topic, payload [, {options}] )

Send a [PUBLISH](http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718037)
packet with a [Topic Name](http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718106),
a [Payload](http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718040),
or throws an error when invalid parameter.

The optional table `options` may contains the following fields:

- `qos`: an value in the set 0, 1 and 2, the default value is 0
- `retain`: a boolean

#### subscribe ( { filter1, qos1, ... } )

Send a [SUBSCRIBE](http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718063)
packet with a non empty set of pair of [Topic Filters](http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718106)
and Requested QoS,
or throws an error when invalid parameter.

The Requested QoS must be in the set 0, 1 and 2.

#### unsubscribe ( { filter1, ... } )

Send a [UNSUBSCRIBE](http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718072)
packet with a non empty list of [Topic Filters](http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718106),
or throws an error when invalid parameter.

#### ping ()

Send a [PINGREQ](http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718081)
packet.

#### disconnect ()

Send a [DISCONNECT](http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718090)
packet.

#### read ()

Waits and handles a packet from the server.
Returns a table describing this packet or `nil` and a message describing the reason of a disconnection
or an unsuccessful reconnection.

The table contains at least the field `type` with a string value.

The callback `on_connect (return_code, session_present)` is called on CONNACK packet received.
If the connection is successful, this callback does typically some subscriptions.
This callback is not intended to return something.

The callback `on_message (topic, payload)` is called on each PUBLISH packet received.
This callback is not intended to return something.

The callback `on_error ()` is called when the connection is broken.
When everything is going well, this callback open a new connection,
calls the method `reconnect` with it and finally return a true value.
In bad case, this callback returns a false value, optionally followed by a message.

## Class functions & members

#### match (name, filter)

Checks if a [Topic Name](http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718106)
matches a [Topic Filter](http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718106).
Returns a boolean or throws an error when invalid parameter.

#### PORT

Gives the IANA registered port 1883 as a string.

#### PORT_TLS

Gives the IANA registered port 8883 as a string.
