
ifeq ($(wildcard bed),bed)
LUA     := $(CURDIR)/bed/bin/lua
LUACHECK:= $(CURDIR)/bed/bin/luacheck
LUAROCKS:= $(CURDIR)/bed/bin/luarocks
LUACOV  := $(CURDIR)/bed/bin/luacov
LUACOV_C:= $(CURDIR)/bed/bin/luacov-console
TL      := $(CURDIR)/bed/bin/tl
else
LUA     := lua
LUACHECK:= luacheck
LUAROCKS:= luarocks
LUACOV  := luacov
LUACOV_C:= luacov-console
TL      := tl
endif

LUAVER  := 5.4
PREFIX  := /usr/local
DPREFIX := $(DESTDIR)$(PREFIX)
LIBDIR  := $(DPREFIX)/share/lua/$(LUAVER)
INSTALL := install

ifeq ($(LUAVER),5.1)
SRC     := src5.1
else ifeq ($(LUAVER),5.2)
SRC     := src5.2
else
SRC     := src
endif
VERSION := $(shell LUA_PATH=";;$(SRC)/?.lua" $(LUA) -e "m = require [[mqtt5]]; print(m._VERSION)")
TARBALL := lua-mqtt-$(VERSION).tar.gz
REV     := 1

BED_OPTS:= --lua latest

all: src5.1/mqtt311.lua src5.1/mqtt5.lua src5.2/mqtt311.lua src5.2/mqtt5.lua

src5.1/mqtt311.lua: src/mqtt311.lua
	$(TL) gen --gen-target=5.1 --gen-compat=off -o src5.1/mqtt311.lua src/mqtt311.lua
	sed -e 's/bit32/bit/g;' -i src5.1/mqtt311.lua

src5.1/mqtt5.lua: src/mqtt5.lua
	$(TL) gen --gen-target=5.1 --gen-compat=off -o src5.1/mqtt5.lua  src/mqtt5.lua
	sed -e 's/bit32/bit/g;' -i src5.1/mqtt5.lua

src5.2/mqtt311.lua: src/mqtt311.lua
	$(TL) gen --gen-target=5.1 --gen-compat=off -o src5.2/mqtt311.lua src/mqtt311.lua

src5.2/mqtt5.lua: src/mqtt5.lua
	$(TL) gen --gen-target=5.1 --gen-compat=off -o src5.2/mqtt5.lua src/mqtt5.lua

install:
	$(INSTALL) -m 644 -D $(SRC)/mqtt311.lua $(LIBDIR)/mqtt311.lua
	$(INSTALL) -m 644 -D $(SRC)/mqtt5.lua   $(LIBDIR)/mqtt5.lua

uninstall:
	rm -f $(LIBDIR)/mqtt311.lua
	rm -f $(LIBDIR)/mqtt5.lua

manifest_pl := \
use strict; \
use warnings; \
my @files = qw{MANIFEST}; \
while (<>) { \
    chomp; \
    next if m{^\.git}; \
    next if m{^debian/}; \
    next if m{^rockspec/}; \
    push @files, $$_; \
} \
print join qq{\n}, sort @files;

rockspec_pl := \
use strict; \
use warnings; \
use Digest::MD5; \
open my $$FH, q{<}, q{$(TARBALL)} \
    or die qq{Cannot open $(TARBALL) ($$!)}; \
binmode $$FH; \
my %config = ( \
    version => q{$(VERSION)}, \
    rev     => q{$(REV)}, \
    md5     => Digest::MD5->new->addfile($$FH)->hexdigest(), \
); \
close $$FH; \
while (<>) { \
    s{@(\w+)@}{$$config{$$1}}g; \
    print; \
}

version:
	@echo $(VERSION)

.PHONY: CHANGES
CHANGES:
	perl -i.bak -pe "s{^$(VERSION).*}{q{$(VERSION)  }.localtime()}e" CHANGES

.PHONY: tag
tag:
	git tag -a -m 'tag release $(VERSION)' $(VERSION)

.PHONY: MANIFEST
MANIFEST:
	git ls-files | perl -e '$(manifest_pl)' > MANIFEST

$(TARBALL): MANIFEST
	[ -d lua-mqtt-$(VERSION) ] || ln -s . lua-mqtt-$(VERSION)
	perl -ne 'print qq{lua-mqtt-$(VERSION)/$$_};' MANIFEST | \
	    tar -zc -T - -f $(TARBALL)
	rm lua-mqtt-$(VERSION)

.PHONY: dist
dist: $(TARBALL)

.PHONY: rockspec
rockspec: $(TARBALL)
	perl -e '$(rockspec_pl)' rockspec.in    > rockspec/lua-mqtt-$(VERSION)-$(REV).rockspec

.PHONY: rock
rock:
	$(LUAROCKS) pack rockspec/lua-mqtt-$(VERSION)-$(REV).rockspec

.PHONY: deb
deb:
	echo "lua-mqtt ($(shell git describe --dirty)) unstable; urgency=medium" >  debian/changelog
	echo ""                         >> debian/changelog
	echo "  * UNRELEASED"           >> debian/changelog
	echo ""                         >> debian/changelog
	echo " -- $(shell git config --get user.name) <$(shell git config --get user.email)>  $(shell date -R)" >> debian/changelog
	fakeroot debian/rules clean binary

bed:
	hererocks bed $(BED_OPTS) --no-readline --luarocks latest --verbose
	bed/bin/luarocks install lua-testassertion
	bed/bin/luarocks install luacheck
	bed/bin/luarocks install luacov
	bed/bin/luarocks install luacov-console
	bed/bin/luarocks install cqueues
	bed/bin/luarocks install copas
	bed/bin/luarocks install http
	bed/bin/luarocks install lualogging
	bed/bin/luarocks install luasec
	bed/bin/luarocks install lua-rotas
	bed/bin/luarocks install tl 0.15.3
	hererocks bed --show
	bed/bin/luarocks list

.PHONY: check
check: test

.PHONY: test
test: test311 test5

.PHONY: test311
test311:
	LUA_PATH="$(CURDIR)/$(SRC)/?.lua;;" \
		prove --exec=$(LUA) test311/*.lua

.PHONY: test5
test5:
	LUA_PATH="$(CURDIR)/$(SRC)/?.lua;;" \
		prove --exec=$(LUA) test5/*.lua

.PHONY: fuzzing
fuzzing: fuzzing311 fuzzing5

.PHONY: fuzzing311
fuzzing311:
	FUZZ_NB=1000000 LUA_PATH="$(CURDIR)/$(SRC)/?.lua;;" \
		prove --exec=$(LUA) test311/08*.lua

.PHONY: fuzzing5
fuzzing5:
	FUZZ_NB=1000000 LUA_PATH="$(CURDIR)/$(SRC)/?.lua;;" \
		prove --exec=$(LUA) test5/08*.lua

.PHONY: luacheck
luacheck:
	-$(LUACHECK) --std=max --codes --max-line-length 160 src --ignore 211/_ENV --ignore 231/_ENV
	-$(LUACHECK) --std=max --codes eg --ignore 211/puback
	$(LUACHECK) --std=max --config .test.luacheckrc test*

.PHONY: coverage
coverage:
	rm -f luacov.*
	-FUZZ_NB=0 LUA_PATH="$(CURDIR)/$(SRC)/?.lua;;" \
		prove --exec="$(LUA) -lluacov" test311/*.lua test5/*.lua
	$(LUACOV_C) test311
	$(LUACOV_C) -s test311
	$(LUACOV_C) test5
	$(LUACOV_C) -s test5
	$(LUACOV_C) $(CURDIR)/src
	$(LUACOV_C) -s $(CURDIR)/src
	$(LUACOV)

.PHONY: pages
pages:
	mkdocs build -d public

.PHONY: clean
clean:
	rm -f src5.1/*.lua
	rm -f src5.2/*.lua
	rm -f MANIFEST *.bak luacov.* *.rockspec

.PHONY: realclean
realclean: clean
	rm -rf bed

