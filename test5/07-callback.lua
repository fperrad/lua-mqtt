#!/usr/bin/env lua

require 'Test.Assertion'

plan(24)

local mqtt = require 'mqtt5'

local buffer
local pos
local function set_buffer (s)
    buffer = s
    pos = 1
end
local sock = {
    close = true,
    read  = function (_, n)
        local s = string.sub(buffer, pos, pos + n -1)
        pos = pos + n
        if s == '' then
            return nil, 'EMPTY'
        else
            return s
        end
    end,
    write = function ()
        diag('socket:write')
    end,
}
local cli = mqtt({
    socket = sock,
    logger = {
        debug = function (_, ...) diag(...) end,
        error = function (_, ...) diag(...) end,
        fatal = function (_, ...) diag(...) end,
        info  = function (_, ...) diag(...) end,
        warn  = function (_, ...) diag(...) end,
    },
})
is_table( cli )
cli:connect()

set_buffer(string.char(0x20, 3, 0x00, 0x00, 0))
same( cli:read(), {
    type    = 'CONNACK',
    sp      = false,
    rc      = 0,
    props   = {},
}, "no callback on_connect" )

cli.on_connect = function ()
    error('CONNECT')
end
cli.connack_received = false
set_buffer(string.char(0x20, 3, 0x00, 0x00, 0))
same( cli:read(), {
    type    = 'CONNACK',
    sp      = false,
    rc      = 0,
    props   = {},
}, "error in callback on_connect" )

cli.on_connect = function (reason_code, session_present, props)
    equals( reason_code, 0 )
    equals( session_present, false )
    same( props, {} )
end
cli.connack_received = false
set_buffer(string.char(0x20, 3, 0x00, 0x00, 0))
same( cli:read(), {
    type    = 'CONNACK',
    sp      = false,
    rc      = 0,
    props   = {},
}, "good callback on_connect" )

cli:subscribe({ '#', 2 })

set_buffer(string.char(
    0x30,
    12,
        0x00, 0x03, 97, 47, 98,
        0,
        70, 111, 111, 66, 97, 114
))
same( cli:read(), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 0,
    topic   = 'a/b',
    props   = {},
    payload = 'FooBar',
}, "no callback" )

cli.on_message = function ()
    error('MESSAGE')
end
set_buffer(string.char(
    0x30,
    12,
        0x00, 0x03, 97, 47, 98,
        0,
        70, 111, 111, 66, 97, 114
))
same( cli:read(), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 0,
    topic   = 'a/b',
    props   = {},
    payload = 'FooBar',
}, "error in callback" )

cli.on_message = function (topic, payload, props)
    equals( topic, 'a/b')
    equals( payload, 'FooBar' )
    is_table( props )
end
set_buffer(string.char(
    0x30,
    12,
        0x00, 0x03, 97, 47, 98,
        0,
        70, 111, 111, 66, 97, 114
))
same( cli:read(), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 0,
    topic   = 'a/b',
    props   = {},
    payload = 'FooBar',
}, "good callback" )

set_buffer(string.char(0xE0, 2, 0x00, 0))
same( cli:read(), {
    type    = 'DISCONNECT',
    rc      = 0,
    props   = {},
}, "no callback on_disconnect" )

cli.on_disconnect = function ()
    error('DISCONNECT')
end
set_buffer(string.char(0xE0, 2, 0x00, 0))
same( cli:read(), {
    type    = 'DISCONNECT',
    rc      = 0,
    props   = {},
}, "error in callback on_disconnect" )

cli.on_disconnect = function (reason_code, props)
    equals( reason_code, 0 )
    same( props, {} )
end
set_buffer(string.char(0xE0, 2, 0x00, 0))
same( cli:read(), {
    type    = 'DISCONNECT',
    rc      = 0,
    props   = {},
}, "good callback on_disconnect" )

set_buffer('')
local ret, msg = cli:read()
is_nil( ret )
equals( msg, "EMPTY", "no callback" )
cli.socket = sock

cli.on_error = function ()
    error('ERROR')
end
set_buffer('')
is_nil( cli:read(), "error in callback" )
cli.socket = sock

cli.on_error = function ()
    return nil, 'ERROR'
end
set_buffer('')
ret, msg = cli:read()
is_nil( ret )
equals( msg, 'ERROR' )
cli.socket = sock

cli.on_error = function ()
    cli.pingreq_sent = true
    set_buffer(string.char(
        0xD0,
        0
    ))
    return true
end
set_buffer('')
same( cli:read(), {
    type = 'PINGRESP',
}, "restored after callback")

