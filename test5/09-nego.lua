#!/usr/bin/env lua

require 'Test.Assertion'

plan(103)

local mqtt = require 'mqtt5'

local buffer
local pos
local function set_buffer (s)
    buffer = s
    pos = 1
end
local sock = {
    close = function ()
        diag('socket:close')
    end,
    read  = function (_, n)
        local s = string.sub(buffer, pos, pos + n -1)
        pos = pos + n
        return s ~= '' and s or nil
    end,
    write = function (_, s)
       return { string.byte(s, 1, #s) }
    end,
}

local logger = {
    debug = function (_, ...) diag(...) end,
    error = function (_, ...) diag(...) end,
    fatal = function (_, ...) diag(...) end,
    info  = function (_, ...) diag(...) end,
    warn  = function (_, ...) diag(...) end,
}

local cli = mqtt({ socket = sock, logger = logger })
is_table( cli )

cli:connect({ clean = true })
set_buffer(string.char(
    0x20,
    3,
        0x01,
        0x00,
        0
))
local ret, msg = cli:read()
is_nil( ret )
equals( msg, "invalid CONNACK (sp but clean)", "connack with sp but clean requested" )

cli = mqtt({ socket = sock, logger = logger })
cli:connect({ id = '' })
equals(cli.id, '')
set_buffer(string.char(
    0x20,
    9,
        0x00,
        0x00,
        6,
            18,
            0x00, 0x03, 102, 111, 111
))
same( cli:read(), {
    type  = 'CONNACK',
    sp    = false,
    rc    = 0,
    props = { 18, 'foo' },
}, "expected assigned client identifier" )
equals(cli.id, 'foo')

cli = mqtt({ socket = sock, logger = logger })
cli:connect({ id = 'bar' })
set_buffer(string.char(
    0x20,
    9,
        0x00,
        0x00,
        6,
            18,
            0x00, 0x03, 102, 111, 111
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "client id in CONNACK", "unexpected assigned client identifier" )

cli = mqtt({ socket = sock, logger = logger })
cli:connect({ id = '' })
set_buffer(string.char(
    0x20,
    3,
        0x00,
        0x00,
        0
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "no client id in CONNACK", "no assigned client identifier" )

cli = mqtt({ socket = sock, logger = logger })
error_matches( function () cli:connect({ id = 'foo' }, { mqtt.PROP.RECEIVE_MAXIMUM, 0 }) end,
               "^[^:]+:%d+: invalid value for Property %(33%) at index 2 in table to 'connect'" )
local res = cli:connect({ id = 'foo' }, { mqtt.PROP.RECEIVE_MAXIMUM, 1 })
equals( res[1], 0x10, "connect with receive maximum = 1" )
set_buffer(string.char(0x20, 3, 0x00, 0x00, 0))
truthy( cli:read(), "connack")
res = cli:subscribe({ '#', 2 })
equals( res[1], 0x82, "subscribe" )

set_buffer(string.char(
    0x34,
    14,
        0x00, 0x03, 97, 47, 98,
        0x10, 0x01,
        0,
        70, 111, 111, 66, 97, 114
))
same( cli:read(), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 2,
    topic   = 'a/b',
    id      = 0x1001,
    props   = {},
    payload = 'FooBar',
}, "message #1" )

set_buffer(string.char(
    0x34,
    14,
        0x00, 0x03, 97, 47, 98,
        0x10, 0x02,
        0,
        70, 111, 111, 66, 97, 114
))
cli._mk_disconnect = function (reason_code)
    equals( reason_code, 147, "disconnect 147: receive maximum exceeded" )
    return ''
end
ret, msg = cli:read()
is_nil( ret )
equals( msg, "receive maximum exceeded", "receive maximum exceeded" )


cli = mqtt({ socket = sock, logger = logger })
res = cli:connect({ id = 'foo' }, {})
equals( res[1], 0x10, "connect" )
set_buffer(string.char(0x20, 3, 0x00, 0x00, 0))
truthy( cli:read(), "connack")
res = cli:subscribe({ '#', 2 })
equals( res[1], 0x82, "subscribe QoS 2" )

set_buffer(string.char(
    0x30,
    12,
        0x00, 0x03, 97, 47, 98,
        0,
        70, 111, 111, 66, 97, 114
))
same( cli:read(), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 0,
    topic   = 'a/b',
    props   = {},
    payload = 'FooBar',
})

set_buffer(string.char(
    0x32,
    14,
        0x00, 0x03, 97, 47, 98,
        0x10, 0x01,
        0,
        70, 111, 111, 66, 97, 114
))
same( cli:read(), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 1,
    topic   = 'a/b',
    id      = 0x1001,
    props   = {},
    payload = 'FooBar',
})

set_buffer(string.char(
    0x34,
    14,
        0x00, 0x03, 97, 47, 98,
        0x10, 0x02,
        0,
        70, 111, 111, 66, 97, 114
))
same( cli:read(), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 2,
    topic   = 'a/b',
    id      = 0x1002,
    props   = {},
    payload = 'FooBar',
})

cli = mqtt({ socket = sock, logger = logger })
res = cli:connect({ id = 'foo' }, {})
equals( res[1], 0x10, "connect" )
set_buffer(string.char(0x20, 3, 0x00, 0x00, 0))
truthy( cli:read(), "connack")
res = cli:subscribe({ '#', 1 })
equals( res[1], 0x82, "subscribe QoS 1" )

set_buffer(string.char(
    0x30,
    12,
        0x00, 0x03, 97, 47, 98,
        0,
        70, 111, 111, 66, 97, 114
))
same( cli:read(), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 0,
    topic   = 'a/b',
    props   = {},
    payload = 'FooBar',
})

set_buffer(string.char(
    0x32,
    14,
        0x00, 0x03, 97, 47, 98,
        0x10, 0x01,
        0,
        70, 111, 111, 66, 97, 114
))
same( cli:read(), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 1,
    topic   = 'a/b',
    id      = 0x1001,
    props   = {},
    payload = 'FooBar',
})

set_buffer(string.char(
    0x34,
    14,
        0x00, 0x03, 97, 47, 98,
        0x10, 0x02,
        0,
        70, 111, 111, 66, 97, 114
))
cli._mk_disconnect = function (reason_code)
    equals( reason_code, 155, "disconnect 155: QoS not supported" )
    return ''
end
ret, msg = cli:read()
is_nil( ret )
equals( msg, "QoS not supported", "QoS 2" )

cli = mqtt({ socket = sock, logger = logger })
res = cli:connect({ id = 'foo' }, {})
equals( res[1], 0x10, "connect" )
set_buffer(string.char(0x20, 3, 0x00, 0x00, 0))
truthy( cli:read(), "connack")
res = cli:subscribe({ '#', 0, 'a/#', 1 }, { mqtt.PROP.SUBSCRIPTION_IDENTIFIER, 42 })
equals( res[1], 0x82, "subscribe QoS 1 with subid" )

set_buffer(string.char(
    0x32,
    16,
        0x00, 0x03, 97, 47, 98,
        0x10, 0x01,
        2,
            11, 42,
        70, 111, 111, 66, 97, 114
))
same( cli:read(), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 1,
    topic   = 'a/b',
    id      = 0x1001,
    props   = { mqtt.PROP.SUBSCRIPTION_IDENTIFIER, 42 },
    payload = 'FooBar',
})

set_buffer(string.char(
    0x32,
    16,
        0x00, 0x03, 97, 47, 98,
        0x10, 0x01,
        2,
            11, 43,
        70, 111, 111, 66, 97, 114
))
cli._mk_disconnect = function (reason_code)
    equals( reason_code, 130, "disconnect 130: protocol error" )
    return ''
end
ret, msg = cli:read()
is_nil( ret )
equals( msg, "unknown Subscription Identifier 43 in PUBLISH", "unknown subscription id 43" )

set_buffer(string.char(
    0x34,
    16,
        0x00, 0x03, 97, 47, 98,
        0x10, 0x02,
        2,
            11, 42,
        70, 111, 111, 66, 97, 114
))
cli.socket = sock
cli._mk_disconnect = function (reason_code)
    equals( reason_code, 155, "disconnect 155: QoS not supported" )
    return ''
end
ret, msg = cli:read()
is_nil( ret )
equals( msg, "QoS not supported", "QoS 2" )

cli = mqtt({ socket = sock, logger = logger })
res = cli:connect({ id = 'foo' }, {})
equals( res[1], 0x10, "connect" )
set_buffer(string.char(0x20, 3, 0x00, 0x00, 0))
truthy( cli:read(), "connack")
res = cli:subscribe({ '#', 0 })
equals( res[1], 0x82, "subscribe QoS 0" )

set_buffer(string.char(
    0x30,
    12,
        0x00, 0x03, 97, 47, 98,
        0,
        70, 111, 111, 66, 97, 114
))
same( cli:read(), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 0,
    topic   = 'a/b',
    props   = {},
    payload = 'FooBar',
})

set_buffer(string.char(
    0x32,
    14,
        0x00, 0x03, 97, 47, 98,
        0x10, 0x01,
        0,
        70, 111, 111, 66, 97, 114
))
cli._mk_disconnect = function (reason_code)
    equals( reason_code, 155, "disconnect 155: QoS not supported" )
    return ''
end
ret, msg = cli:read()
is_nil( ret )
equals( msg, "QoS not supported", "QoS 1" )

set_buffer(string.char(
    0x34,
    14,
        0x00, 0x03, 97, 47, 98,
        0x10, 0x02,
        0,
        70, 111, 111, 66, 97, 114
))
cli.socket = sock
cli._mk_disconnect = function (reason_code)
    equals( reason_code, 155, "disconnect 155: QoS not supported" )
    return ''
end
ret, msg = cli:read()
is_nil( ret )
equals( msg, "QoS not supported", "QoS 2" )


cli = mqtt({ socket = sock, logger = logger })
error_matches( function () cli:connect({ id = 'foo' }, { mqtt.PROP.MAXIMUM_PACKET_SIZE, 0 }) end,
               "^[^:]+:%d+: invalid value for Property %(39%) at index 2 in table to 'connect'" )
res = cli:connect({ id = 'foo' }, { mqtt.PROP.MAXIMUM_PACKET_SIZE, 0xFF })
equals( res[1], 0x10, "connect with maximum packet size = 255" )
set_buffer(string.char(0x20, 3, 0x00, 0x00, 0))
truthy( cli:read(), "connack")
res = cli:subscribe({ '#', 0 })
equals( res[1], 0x82, "subscribe QoS 0" )

local payload = string.rep('x', 255 - 6)
set_buffer(string.char(
    0x30,
    0xFF, 0x01,
        0x00, 0x03, 97, 47, 98,
        0
) .. payload)
same( cli:read(), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 0,
    topic   = 'a/b',
    props   = {},
    payload = payload,
}, "publish" )

payload = string.rep('x', 256 - 6)
set_buffer(string.char(
    0x30,
    0x80, 0x02,
        0x00, 0x03, 97, 47, 98,
        0
) .. payload)
cli._mk_disconnect = function (reason_code)
    equals( reason_code, 149, "disconnect 149: packet too large" )
    return ''
end
ret, msg = cli:read()
is_nil( ret )
equals( msg, "packet too large: 256", "packet too large" )


cli = mqtt({ socket = sock, logger = logger })
cli:connect({ id = 'foo' })
set_buffer(string.char(
    0x20,
    6,
        0x00,
        0x00,
        3,
            33,
            0x00, 0x02
))
same( cli:read(), {
    type  = 'CONNACK',
    sp    = false,
    rc    = 0x00,
    props = { mqtt.PROP.RECEIVE_MAXIMUM, 2 },
}, "connack with receive maximum = 2")

res = cli:publish('a/b', 'FooBar', { qos = 1 })
equals( res[1], 0x32, "publish #1" )
res = cli:publish('a/b', 'FooBar', { qos = 1 })
equals( res[1], 0x32, "publish #2" )
error_matches( function () cli:publish('a/b', 'FooBar', { qos = 1 }) end,
               "^[^:]+:%d+: Receive Maximum exceeded",
               "publish #3" )


cli = mqtt({ socket = sock, logger = logger })
cli:connect({ id = 'foo' })
set_buffer(string.char(
    0x20,
    3,
        0x00,
        0x00,
        0
))
same( cli:read(), {
    type  = 'CONNACK',
    sp    = false,
    rc    = 0x00,
    props = {},
}, "connack without maximum QoS")

res = cli:publish('a/b', 'FooBar', { qos = 0 })
equals( res[1], 0x30 )
res = cli:publish('a/b', 'FooBar', { qos = 1 })
equals( res[1], 0x32 )
res = cli:publish('a/b', 'FooBar', { qos = 2 })
equals( res[1], 0x34 )

cli = mqtt({ socket = sock, logger = logger })
cli:connect({ id = 'foo' })
set_buffer(string.char(
    0x20,
    5,
        0x00,
        0x00,
        2,
            36,
            1
))
same( cli:read(), {
    type  = 'CONNACK',
    sp    = false,
    rc    = 0x00,
    props = { mqtt.PROP.MAXIMUM_QOS, 1 },
}, "connack with maximum QoS = 1")

res = cli:publish('a/b', 'FooBar', { qos = 0 })
equals( res[1], 0x30 )
res = cli:publish('a/b', 'FooBar', { qos = 1 })
equals( res[1], 0x32 )
error_matches( function () cli:publish('a/b', 'FooBar', { qos = 2 }) end,
               "^[^:]+:%d+: QoS not supported %(2%)" )

cli = mqtt({ socket = sock, logger = logger })
cli:connect({ id = 'foo' })
set_buffer(string.char(
    0x20,
    5,
        0x00,
        0x00,
        2,
            36,
            0
))
same( cli:read(), {
    type  = 'CONNACK',
    sp    = false,
    rc    = 0x00,
    props = { mqtt.PROP.MAXIMUM_QOS, 0 },
}, "connack with maximum QoS = 0")

res = cli:publish('a/b', 'FooBar', { qos = 0 })
equals( res[1], 0x30 )
error_matches( function () cli:publish('a/b', 'FooBar', { qos = 1 }) end,
               "^[^:]+:%d+: QoS not supported %(1%)" )
error_matches( function () cli:publish('a/b', 'FooBar', { qos = 2 }) end,
               "^[^:]+:%d+: QoS not supported %(2%)" )


cli = mqtt({ socket = sock, logger = logger })
cli:connect({ id = 'foo' })
set_buffer(string.char(
    0x20,
    3,
        0x00,
        0x00,
        0
))
same( cli:read(), {
    type  = 'CONNACK',
    sp    = false,
    rc    = 0x00,
    props = {},
}, "connack without retain available")

res = cli:publish('a/b', 'FooBar', { retain = true })
equals( res[1], 0x31 )

cli = mqtt({ socket = sock, logger = logger })
cli:connect({ id = 'foo' })
set_buffer(string.char(
    0x20,
    5,
        0x00,
        0x00,
        2,
            37,
            1
))
same( cli:read(), {
    type  = 'CONNACK',
    sp    = false,
    rc    = 0x00,
    props = { mqtt.PROP.RETAIN_AVAILABLE, 1 },
}, "connack with retain available = 1")

res = cli:publish('a/b', 'FooBar', { retain = true })
equals( res[1], 0x31 )

cli = mqtt({ socket = sock, logger = logger })
cli:connect({ id = 'foo' })
set_buffer(string.char(
    0x20,
    5,
        0x00,
        0x00,
        2,
            37,
            0
))
same( cli:read(), {
    type  = 'CONNACK',
    sp    = false,
    rc    = 0x00,
    props = { mqtt.PROP.RETAIN_AVAILABLE, 0 },
}, "connack with retain available = 0")

error_matches( function () cli:publish('a/b', 'FooBar', { retain = true }) end,
               "^[^:]+:%d+: Retain not supported" )


cli = mqtt({ socket = sock, logger = logger })
cli:connect({ id = 'foo' })
set_buffer(string.char(
    0x20,
    8,
        0x00,
        0x00,
        5,
            39,
            0x00, 0x00, 0x00, 0xFF
))
same( cli:read(), {
    type  = 'CONNACK',
    sp    = false,
    rc    = 0x00,
    props = { mqtt.PROP.MAXIMUM_PACKET_SIZE, 0x000000FF },
}, "connack with maximum packet size = 255")

res = cli:publish('a/b', string.rep('x', 255 - 6))
equals( res[1], 0x30 )
error_matches( function () cli:publish('a/b', string.rep('x', 256 - 6) ) end,
               "^[^:]+:%d+: Packet too large" )


cli = mqtt({ socket = sock, logger = logger })
cli:connect({ id = 'foo' })
set_buffer(string.char(
    0x20,
    3,
        0x00,
        0x00,
        0
))
same( cli:read(), {
    type  = 'CONNACK',
    sp    = false,
    rc    = 0x00,
    props = {},
}, "connack")

res = cli:subscribe({ 'foo/bar', 0x1 })
equals( res[1], 0x82 )
res = cli:subscribe({ '+/bar', 0x1 })
equals( res[1], 0x82 )
res = cli:subscribe({ 'foo/#', 0x1 })
equals( res[1], 0x82 )
res = cli:subscribe({ '$share/foo/bar', 0x1 })
equals( res[1], 0x82 )
res = cli:subscribe({ 'foo/bar', 0x1 }, { 11, 0x1234 } )
equals( res[1], 0x82 )

cli = mqtt({ socket = sock, logger = logger })
cli:connect({ id = 'foo' })
set_buffer(string.char(
    0x20,
    9,
        0x00,
        0x00,
        6,
            40,
            1,
            41,
            1,
            42,
            1
))
same( cli:read(), {
    type  = 'CONNACK',
    sp    = false,
    rc    = 0x00,
    props = { mqtt.PROP.WILDCARD_SUB_AVAILABLE, 1, mqtt.PROP.SUBSCRIPTION_ID_AVAILABLE, 1, mqtt.PROP.SHARED_SUB_AVAILABLE, 1 },
}, "connack")

res = cli:subscribe({ 'foo/bar', 0x1 })
equals( res[1], 0x82 )
res = cli:subscribe({ '+/bar', 0x1 })
equals( res[1], 0x82 )
res = cli:subscribe({ 'foo/#', 0x1 })
equals( res[1], 0x82 )
res = cli:subscribe({ '$share/foo/bar', 0x1 })
equals( res[1], 0x82 )
res = cli:subscribe({ 'foo/bar', 0x1 }, { 11, 0x1234 } )
equals( res[1], 0x82 )

cli = mqtt({ socket = sock, logger = logger })
cli:connect({ id = 'foo' })
set_buffer(string.char(
    0x20,
    9,
        0x00,
        0x00,
        6,
            40,
            0,
            41,
            0,
            42,
            0
))
same( cli:read(), {
    type  = 'CONNACK',
    sp    = false,
    rc    = 0x00,
    props = { mqtt.PROP.WILDCARD_SUB_AVAILABLE, 0, mqtt.PROP.SUBSCRIPTION_ID_AVAILABLE, 0, mqtt.PROP.SHARED_SUB_AVAILABLE, 0 },
}, "connack")

res = cli:subscribe({ 'foo/bar', 0x1 })
equals( res[1], 0x82 )
error_matches( function () cli:subscribe({ '+/bar', 0x1 }) end,
               "^[^:]+:%d+: Wildcard subscriptions not supported %(%+/bar%)" )
error_matches( function () cli:subscribe({ 'foo/#', 0x1 })end,
               "^[^:]+:%d+: Wildcard subscriptions not supported %(foo/#%)" )
error_matches( function () cli:subscribe({ '$share/foo/bar', 0x1 }) end,
               "^[^:]+:%d+: Shared subscriptions not supported %(%$share/foo/bar%)" )
error_matches( function () cli:subscribe({ 'foo/bar', 0x1 }, { 11, 0x1234 } ) end,
               "^[^:]+:%d+: Subscription identifiers not supported" )

