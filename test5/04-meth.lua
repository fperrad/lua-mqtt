#!/usr/bin/env lua

require 'Test.Assertion'

plan(145)

local mqtt = require 'mqtt5'

local sock = {
    close = true,
    read  = true,
    write = function (_, s) return { string.byte(s, 1, #s) } end,
}
local cli = mqtt({
    socket = sock,
    logger = {
        debug = function (_, ...) diag(...) end,
        error = function (_, ...) diag(...) end,
        fatal = function (_, ...) diag(...) end,
        info  = function (_, ...) diag(...) end,
        warn  = function (_, ...) diag(...) end,
    },
})
is_table( cli )

local res = cli:connect()
equals( res[1], 0x10 )
equals( res[10], 0x02 )

res = cli:reconnect(sock)
equals( res[1], 0x10 )
equals( res[10], 0x02 )

cli.connect_sent = false
res = cli:connect({ keep_alive = 42, id = 'CLI', username = 'user', password = 'pAssWd', will = {'a/b', 'ended', qos = 2, retain = true } })
equals( res[1], 0x10 )
equals( res[10], 0xF4 )

res = cli:reconnect(sock)
equals( res[1], 0x10 )
equals( res[10], 0xF4 )

cli.connect_sent = false
res = cli:connect({ keep_alive = 42, id = 'CLI', username = 'user', password = 'pAssWd', will = {'a/b', 'ended' } })
equals( res[1], 0x10 )
equals( res[10], 0xC4 )

res = cli:reconnect(sock)
equals( res[1], 0x10 )
equals( res[10], 0xC4 )

cli.connect_sent = false
error_matches( function () cli:connect(42) end,
               "^[^:]+:%d+: bad argument #1 to 'connect' %(table expected, got number%)" )

error_matches( function () cli:connect({ keep_alive = 3.14 }) end,
               "^[^:]+:%d+: invalid Keep Alive %(3.14%) in table to 'connect'" )

error_matches( function () cli:connect({ keep_alive = 1000000 }) end,
               "^[^:]+:%d+: invalid Keep Alive %(1000000%) in table to 'connect'" )

error_matches( function () cli:connect({ id = 42 }) end,
               "^[^:]+:%d+: invalid Client Identifier %(42%) in table to 'connect'" )

error_matches( function () cli:connect({ username = 42 }) end,
               "^[^:]+:%d+: invalid User Name %(42%) in table to 'connect'" )

error_matches( function () cli:connect({ password = 42 }) end,
               "^[^:]+:%d+: invalid Password %(42%) in table to 'connect'" )

error_matches( function () cli:connect({ password = 'pAssWd' }) end,
               "^[^:]+:%d+: Password without User Name in table to 'connect'" )

error_matches( function () cli:connect({ will = 42 }) end,
               "^[^:]+:%d+: invalid Will %(42%) in table to 'connect'" )

error_matches( function () cli:connect({ will = {'a\0b', 'ended' } }) end,
               "^[^:]+:%d+: invalid Will Topic %(a\0b%) in table to 'connect'" )

error_matches( function () cli:connect({ will = {'a/b', 42 } }) end,
               "^[^:]+:%d+: invalid Will Message %(42%) in table to 'connect'" )

error_matches( function () cli:connect({ will = {'a/b', 'ended', qos = 42 } }) end,
               "^[^:]+:%d+: invalid Will QoS %(42%) in table to 'connect'" )

cli.connect_sent = true
error_matches( function () cli:reconnect(42) end,
               "^[^:]+:%d+: bad argument #1 to 'reconnect' %(socket expected, got number%)" )

cli.connect_sent = false
res = cli:connect({}, { 17, 123456 })
equals( res[1], 0x10 )

cli.connect_sent = false
error_matches( function () cli:connect({}, 42) end,
               "^[^:]+:%d+: bad argument #2 to 'connect' %(table expected, got number%)" )

error_matches( function () cli:connect({}, { 0, 'dummy' }) end,
               "^[^:]+:%d+: invalid Property %(0%) at index 1 in table to 'connect'" )

error_matches( function () cli:connect({}, { 17, 123456, 17, 123459 }) end,
               "^[^:]+:%d+: duplicated Property %(17%) at index 3 in table to 'connect'" )

error_matches( function () cli:connect({}, { 17, 'foo' }) end,
               "^[^:]+:%d+: invalid value for Property %(17%) at index 2 in table to 'connect'" )

error_matches( function () cli:connect({}, { 21, 42 }) end,
               "^[^:]+:%d+: invalid value for Property %(21%) at index 2 in table to 'connect'" )

error_matches( function () cli:connect({}, { 22, 42 }) end,
               "^[^:]+:%d+: invalid value for Property %(22%) at index 2 in table to 'connect'" )

error_matches( function () cli:connect({}, { 23, 42 }) end,
               "^[^:]+:%d+: invalid value for Property %(23%) at index 2 in table to 'connect'" )

error_matches( function () cli:connect({}, { 25, 42 }) end,
               "^[^:]+:%d+: invalid value for Property %(25%) at index 2 in table to 'connect'" )

error_matches( function () cli:connect({}, { 33, 'foo' }) end,
               "^[^:]+:%d+: invalid value for Property %(33%) at index 2 in table to 'connect'" )

error_matches( function () cli:connect({}, { 34, 'foo' }) end,
               "^[^:]+:%d+: invalid value for Property %(34%) at index 2 in table to 'connect'" )

error_matches( function () cli:connect({}, { 38, 42 }) end,
               "^[^:]+:%d+: invalid value for Property %(38%) at index 2 in table to 'connect'" )

error_matches( function () cli:connect({}, { 39, 'foo' }) end,
               "^[^:]+:%d+: invalid value for Property %(39%) at index 2 in table to 'connect'" )

cli.connect_sent = false
res = cli:connect({ will = { 'a/b', 'ended', props = { 3, 'plain/text' } } })
equals( res[1], 0x10 )

cli.connect_sent = false
error_matches( function () cli:connect({ will = { 'a/b', 'ended', props = 42 } }) end,
               "^[^:]+:%d+: invalid Will Props %(42%) in table to 'connect'" )

error_matches( function () cli:connect({ will = { 'a/b', 'ended', props = { 0, 'dummy' } } }) end,
               "^[^:]+:%d+: invalid Property %(0%) at index 1 in will to 'connect'" )

error_matches( function () cli:connect({ will = { 'a/b', 'ended', props = { 3, 'plain/text', 3, 'plain/text' } } }) end,
               "^[^:]+:%d+: duplicated Property %(3%) at index 3 in will to 'connect'" )

error_matches( function () cli:connect({ will = { 'a/b', 'ended', props = { 1, 'foo' } } }) end,
               "^[^:]+:%d+: invalid value for Property %(1%) at index 2 in will to 'connect'" )

error_matches( function () cli:connect({ will = { 'a/b', 'ended', props = { 2, 'foo' } } }) end,
               "^[^:]+:%d+: invalid value for Property %(2%) at index 2 in will to 'connect'" )

error_matches( function () cli:connect({ will = { 'a/b', 'ended', props = { 3, 42 } } }) end,
               "^[^:]+:%d+: invalid value for Property %(3%) at index 2 in will to 'connect'" )

error_matches( function () cli:connect({ will = { 'a/b', 'ended', props = { 8, 'bad#' } } }) end,
               "^[^:]+:%d+: invalid value for Property %(8%) at index 2 in will to 'connect'" )

error_matches( function () cli:connect({ will = { 'a/b', 'ended', props = { 9, 42 } } }) end,
               "^[^:]+:%d+: invalid value for Property %(9%) at index 2 in will to 'connect'" )

error_matches( function () cli:connect({ will = { 'a/b', 'ended', props = { 24, 'foo' } } }) end,
               "^[^:]+:%d+: invalid value for Property %(24%) at index 2 in will to 'connect'" )

error_matches( function () cli:connect({ will = { 'a/b', 'ended', props = { 38, 42 } } }) end,
               "^[^:]+:%d+: invalid value for Property %(38%) at index 2 in will to 'connect'" )

cli.connect_sent = true
truthy(cli:reconnect(sock))

res = cli:publish('a/b', 'FooBar')
equals( res[1], 0x30 )

res = cli:publish('a/b', 'FooBar', { qos = 1 })
equals( res[1], 0x32 )

res = cli:publish('a/b', 'FooBar', { qos = 1, retain = true })
equals( res[1], 0x33 )

res = cli:publish('a/b', 'FooBar', { qos = 2 })
equals( res[1], 0x34 )

res = cli:publish('a/b', 'FooBar', { qos = 2, retain = true })
equals( res[1], 0x35 )

error_matches( function () cli:publish(42, 'FooBar') end,
               "^[^:]+:%d+: bad argument #1 to 'publish' %(string expected, got number%)" )

error_matches( function () cli:publish('', 'FooBar') end,
               "^[^:]+:%d+: invalid Topic Name to 'publish'" )

if _VERSION == 'Lua 5.1' and not jit then
    todo('compat53 utf8', 1)
end
error_matches( function () cli:publish('\xFF', 'FooBar') end,
               "^[^:]+:%d+: invalid Topic Name to 'publish'" )

error_matches( function () cli:publish('a\0b', 'FooBar') end,
               "^[^:]+:%d+: invalid Topic Name to 'publish'" )

error_matches( function () cli:publish('a/#', 'FooBar') end,
               "^[^:]+:%d+: invalid Topic Name to 'publish'" )

error_matches( function () cli:publish('+/b', 'FooBar') end,
               "^[^:]+:%d+: invalid Topic Name to 'publish'" )

error_matches( function () cli:publish('$a/b', 'FooBar') end,
               "^[^:]+:%d+: invalid Topic Name to 'publish'" )

error_matches( function () cli:publish('a/b', 42) end,
               "^[^:]+:%d+: bad argument #2 to 'publish' %(string expected, got number%)" )

error_matches( function () cli:publish('a/b', 'FooBar', 42 ) end,
               "^[^:]+:%d+: bad argument #3 to 'publish' %(table expected, got number%)" )

error_matches( function () cli:publish('a/b', 'FooBar', { qos = 42 }) end,
               "^[^:]+:%d+: invalid QoS %(42%) in table to 'publish'" )

res = cli:publish('a/b', 'FooBar', {}, { 3, 'plain/text' })
equals( res[1], 0x30 )

error_matches( function () cli:publish('a/b', 'FooBar', {}, 42) end,
               "^[^:]+:%d+: bad argument #4 to 'publish' %(table expected, got number%)" )

error_matches( function () cli:publish('a/b', 'FooBar', {}, { 0, 'dummy' }) end,
               "^[^:]+:%d+: invalid Property %(0%) at index 1 in table to 'publish'" )

error_matches( function () cli:publish('a/b', 'FooBar', {}, { 11, 42 }) end,
               "^[^:]+:%d+: invalid Property %(11%) at index 1 in table to 'publish'" )

error_matches( function () cli:publish('a/b', 'FooBar', {}, { 3, 'plain/text', 3, 'plain/text' }) end,
               "^[^:]+:%d+: duplicated Property %(3%) at index 3 in table to 'publish'" )

error_matches( function () cli:publish('a/b', 'FooBar', {}, { 1, 'foo' }) end,
               "^[^:]+:%d+: invalid value for Property %(1%) at index 2 in table to 'publish'" )

error_matches( function () cli:publish('a/b', 'FooBar', {}, { 2, 'foo' }) end,
               "^[^:]+:%d+: invalid value for Property %(2%) at index 2 in table to 'publish'" )

error_matches( function () cli:publish('a/b', 'FooBar', {}, { 3, 42 }) end,
               "^[^:]+:%d+: invalid value for Property %(3%) at index 2 in table to 'publish'" )

error_matches( function () cli:publish('a/b', 'FooBar', {}, { 8, 'bad#' }) end,
               "^[^:]+:%d+: invalid value for Property %(8%) at index 2 in table to 'publish'" )

error_matches( function () cli:publish('a/b', 'FooBar', {}, { 9, 42 }) end,
               "^[^:]+:%d+: invalid value for Property %(9%) at index 2 in table to 'publish'" )

error_matches( function () cli:publish('a/b', 'FooBar', {}, { 35, 'foo' }) end,
               "^[^:]+:%d+: invalid value for Property %(35%) at index 2 in table to 'publish'" )

error_matches( function () cli:publish('a/b', 'FooBar', {}, { 38, 42 }) end,
               "^[^:]+:%d+: invalid value for Property %(38%) at index 2 in table to 'publish'" )

if _VERSION == 'Lua 5.1' and not jit then
    todo('compat53 utf8', 1)
end
error_matches( function () cli:publish('a/b', '\xFF', {}, { mqtt.PROP.PAYLOAD_FORMAT_INDICATOR, 1 }) end,
               "^[^:]+:%d+: invalid Payload Format to 'publish'" )

res = cli:subscribe({ 'a/b', 0x1, 'c/d', 0x2 })
equals( res[1], 0x82 )

error_matches( function () cli:subscribe(42) end,
               "^[^:]+:%d+: bad argument #1 to 'subscribe' %(table expected, got number%)" )

error_matches( function () cli:subscribe({}) end,
               "^[^:]+:%d+: empty table for 'subscribe'" )

error_matches( function () cli:subscribe({ 'a/b', 0x1, '', 0x2 }) end,
               "^[^:]+:%d+: invalid Topic Filter %(%) at index 3 in table to 'subscribe'" )

if _VERSION == 'Lua 5.1' and not jit then
    todo('compat53 utf8', 1)
end
error_matches( function () cli:subscribe({ 'a/b', 0x1, '\xFF', 0x2 }) end,
               "^[^:]+:%d+: invalid Topic Filter %(\xFF%) at index 3 in table to 'subscribe'" )

error_matches( function () cli:subscribe({ 'a/b', 0x1, 'c\0d', 0x2 }) end,
               "^[^:]+:%d+: invalid Topic Filter %(c\0d%) at index 3 in table to 'subscribe'" )

error_matches( function () cli:subscribe({ 'a/b', 0x1, 42, 0x2 }) end,
               "^[^:]+:%d+: invalid Topic Filter %(42%) at index 3 in table to 'subscribe'" )

error_matches( function () cli:subscribe({ 'a/b', 0x1, 'c/d', -2 }) end,
               "^[^:]+:%d+: invalid Options %(%-2%) at index 4 in table to 'subscribe'" )

error_matches( function () cli:subscribe({ 'a/b', 0x1, 'c/d', 66 }) end,
               "^[^:]+:%d+: invalid Options %(66%) at index 4 in table to 'subscribe'" )

error_matches( function () cli:subscribe({ 'a/b', 0x1, 'c/d', 0x03 }) end,
               "^[^:]+:%d+: invalid Requested QoS %(3%) at index 4 in table to 'subscribe'" )

error_matches( function () cli:subscribe({ 'a/b', 0x1, '$share/c/d', 0x04 }) end,
               "^[^:]+:%d+: No Local on a Shared Subscription at index 4 in table to 'subscribe'" )

error_matches( function () cli:subscribe({ 'a/b', 0x1, 'c/d', 0x30 }) end,
               "^[^:]+:%d+: invalid Retain Handling option %(3%) at index 4 in table to 'subscribe'" )

res = cli:subscribe({ 'a/b', 0x1, 'c/d', 0x2 }, { 11, 123456 })
equals( res[1], 0x82 )

error_matches( function () cli:subscribe({ 'a/b', 0x1, 'c/d', 0x2 }, 42) end,
               "^[^:]+:%d+: bad argument #2 to 'subscribe' %(table expected, got number%)" )

error_matches( function () cli:subscribe({ 'a/b', 0x1, 'c/d', 0x2 }, { 0, 'dummy' }) end,
               "^[^:]+:%d+: invalid Property %(0%) at index 1 in table to 'subscribe'" )

error_matches( function () cli:subscribe({ 'a/b', 0x1, 'c/d', 0x2 }, { 11, 123456, 11, 123459 }) end,
               "^[^:]+:%d+: duplicated Property %(11%) at index 3 in table to 'subscribe'" )

error_matches( function () cli:subscribe({ 'a/b', 0x1, 'c/d', 0x2 }, { 11, 'foo' }) end,
               "^[^:]+:%d+: invalid value for Property %(11%) at index 2 in table to 'subscribe'" )

error_matches( function () cli:subscribe({ 'a/b', 0x1, 'c/d', 0x2 }, { 11, 0 }) end,
               "^[^:]+:%d+: invalid value for Property %(11%) at index 2 in table to 'subscribe'" )

error_matches( function () cli:subscribe({ 'a/b', 0x1, 'c/d', 0x2 }, { 38, 42 }) end,
               "^[^:]+:%d+: invalid value for Property %(38%) at index 2 in table to 'subscribe'" )

res = cli:unsubscribe({ 'a/b', 'c/d' })
equals( res[1], 0xA2 )

error_matches( function () cli:unsubscribe(42) end,
               "^[^:]+:%d+: bad argument #1 to 'unsubscribe' %(table expected, got number%)" )

error_matches( function () cli:unsubscribe({}) end,
               "^[^:]+:%d+: empty table for 'unsubscribe'" )

error_matches( function () cli:unsubscribe({'a/b', 'c\0d'}) end,
               "^[^:]+:%d+: invalid Topic Filter %(c\0d%) at index 2 in table to 'unsubscribe'" )

error_matches( function () cli:unsubscribe({'a/b', 42}) end,
               "^[^:]+:%d+: invalid Topic Filter %(42%) at index 2 in table to 'unsubscribe'" )

res = cli:unsubscribe({ 'a/b', 'c/d' }, { 38, { foo = 'bar' } })
equals( res[1], 0xA2 )

error_matches( function () cli:unsubscribe({ 'a/b', 'c/d' }, 42) end,
               "^[^:]+:%d+: bad argument #2 to 'unsubscribe' %(table expected, got number%)" )

error_matches( function () cli:unsubscribe({ 'a/b', 'c/d' }, { 0, 'dummy' }) end,
               "^[^:]+:%d+: invalid Property %(0%) at index 1 in table to 'unsubscribe'" )

error_matches( function () cli:unsubscribe({ 'a/b', 'c/d' }, { 38, 42 }) end,
               "^[^:]+:%d+: invalid value for Property %(38%) at index 2 in table to 'unsubscribe'" )

res = cli:ping()
equals( res[1], 0xC0 )

res = cli:disconnect()
equals( res[1], 0xE0 )

res = cli:disconnect(4)
equals( res[1], 0xE0 )

error_matches( function () cli:disconnect(3.14) end,
               "^[^:]+:%d+: bad argument #1 to 'disconnect' %(integer expected, got number%)" )

error_matches( function () cli:disconnect(42) end,
               "^[^:]+:%d+: invalid Reason Code %(42%) to 'disconnect'" )

res = cli:disconnect(0, { 31, "Foo error" })
equals( res[1], 0xE0 )

error_matches( function () cli:disconnect(0, 42) end,
               "^[^:]+:%d+: bad argument #2 to 'disconnect' %(table expected, got number%)" )

error_matches( function () cli:disconnect(0, { 0, 'dummy' }) end,
               "^[^:]+:%d+: invalid Property %(0%) at index 1 in table to 'disconnect'" )

error_matches( function () cli:disconnect(0, { 31, 'bar', 31, 'baz' }) end,
               "^[^:]+:%d+: duplicated Property %(31%) at index 3 in table to 'disconnect'" )

error_matches( function () cli:disconnect(0, { 17, 'foo' }) end,
               "^[^:]+:%d+: invalid value for Property %(17%) at index 2 in table to 'disconnect'" )

error_matches( function () cli:disconnect(0, { 28, 42 }) end,
               "^[^:]+:%d+: invalid value for Property %(28%) at index 2 in table to 'disconnect'" )

error_matches( function () cli:disconnect(0, { 31, 42 }) end,
               "^[^:]+:%d+: invalid value for Property %(31%) at index 2 in table to 'disconnect'" )

error_matches( function () cli:disconnect(0, { 38, 42 }) end,
               "^[^:]+:%d+: invalid value for Property %(38%) at index 2 in table to 'disconnect'" )

res = cli:auth(24)
equals( res[1], 0xF0 )

error_matches( function () cli:auth(3.14) end,
               "^[^:]+:%d+: bad argument #1 to 'auth' %(integer expected, got number%)" )

error_matches( function () cli:auth(42) end,
               "^[^:]+:%d+: invalid Reason Code %(42%) to 'auth'" )

res = cli:auth(24, { 31, "Foo error" })
equals( res[1], 0xF0 )

error_matches( function () cli:auth(24, 42) end,
               "^[^:]+:%d+: bad argument #2 to 'auth' %(table expected, got number%)" )

error_matches( function () cli:auth(24, { 0, 'dummy' }) end,
               "^[^:]+:%d+: invalid Property %(0%) at index 1 in table to 'auth'" )

error_matches( function () cli:auth(24, { 31, 'bar', 31, 'baz' }) end,
               "^[^:]+:%d+: duplicated Property %(31%) at index 3 in table to 'auth'" )

error_matches( function () cli:auth(24, { 21, 42 }) end,
               "^[^:]+:%d+: invalid value for Property %(21%) at index 2 in table to 'auth'" )

error_matches( function () cli:auth(24, { 22, 42 }) end,
               "^[^:]+:%d+: invalid value for Property %(22%) at index 2 in table to 'auth'" )

error_matches( function () cli:auth(24, { 31, 42 }) end,
               "^[^:]+:%d+: invalid value for Property %(31%) at index 2 in table to 'auth'" )

error_matches( function () cli:auth(24, { 38, 42 }) end,
               "^[^:]+:%d+: invalid value for Property %(38%) at index 2 in table to 'auth'" )


cli = mqtt({
    socket = sock,
})

error_matches( function () cli:reconnect(sock) end,
               "^[^:]+:%d+: CONNECT not sent" )

error_matches( function () cli:publish('a/b', 'FooBar') end,
               "^[^:]+:%d+: CONNECT not sent" )

error_matches( function () cli:subscribe({ 'a/b', 0x1, 'c/d', 0x2 }) end,
               "^[^:]+:%d+: CONNECT not sent" )

error_matches( function () cli:unsubscribe({ 'a/b', 'c/d' }) end,
               "^[^:]+:%d+: CONNECT not sent" )

error_matches( function () cli:ping() end,
               "^[^:]+:%d+: CONNECT not sent" )

error_matches( function () cli:disconnect() end,
               "^[^:]+:%d+: CONNECT not sent" )

error_matches( function () cli:auth(24) end,
               "^[^:]+:%d+: CONNECT not sent" )

truthy( cli:connect() )

error_matches( function () cli:connect() end,
               "^[^:]+:%d+: CONNECT already sent" )


cli = mqtt({
    socket = sock,
})
cli._send_connect = function (_, _, _, client_id)
    return client_id
end
equals( cli:connect({ clean = true, id = 'foo' }), 'foo' )
equals( cli:connect({ clean = false, id = 'foo' }), 'foo' )
equals( cli:connect({ clean = true, id = '' }), '' )
equals( cli:connect({ clean = false, id = '' }), '' )
if _VERSION == 'Lua 5.1' and not jit then
    todo('format', 1)
end
matches( cli:connect({ clean = true }), '^LuaMQTT%x%x%x%x%x%x%x%x$', 'auto-generated' )
equals( cli:connect({ clean = false }), '' )

