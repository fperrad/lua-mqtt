#!/usr/bin/env lua

require 'Test.Assertion'

plan(34)

local mqtt = require 'mqtt5'

local buffer
local pos
local function set_buffer (s)
    buffer = s
    pos = 1
end
local sock = {
    close = function ()
        diag('socket:close')
    end,
    read  = function (_, n)
        local s = string.sub(buffer, pos, pos + n -1)
        pos = pos + n
        return s ~= '' and s or nil
    end,
    write = function (_, s)
       return { string.byte(s, 1, #s) }
    end,
}

local logger = {
    debug = function (_, ...) diag(...) end,
    error = function (_, ...) diag(...) end,
    fatal = function (_, ...) diag(...) end,
    info  = function (_, ...) diag(...) end,
    warn  = function (_, ...) diag(...) end,
}

local cli = mqtt({ socket = sock, logger = logger })
is_table( cli )

local res = cli:connect({ id = 'foo' })
equals( res[1], 0x10, "connect" )
set_buffer(string.char(0x20, 3, 0x00, 0x00, 0))
truthy( cli:read(), "connack")
res = cli:subscribe({ '#', 2 })
equals( res[1], 0x82, "subscribe QoS 2" )

set_buffer(string.char(
    0x30,
       9 + 6,
          0x00, 0x03, 97, 47, 98,       -- a/b
          3,
              35,
              0x00, 0x00,
          70, 111, 111, 66, 97, 114     -- FooBar
))
cli._mk_disconnect = function (reason_code)
    equals( reason_code, 148, "disconnect 148: topic alias invalid" )
    return ''
end
local ret, msg = cli:read()
is_nil( ret )
equals( msg, "topic alias invalid", "topic alias invalid" )

cli = mqtt({ socket = sock, logger = logger })
res = cli:connect({ id = 'foo' }, { mqtt.PROP.TOPIC_ALIAS_MAXIMUM, 0x8000 })
equals( res[1], 0x10, "connect with topic alias maximum" )
set_buffer(string.char(
    0x20,
    3,
        0x00,
        0x00,
        0
))
same( cli:read(), {
    type  = 'CONNACK',
    sp    = false,
    rc    = 0x00,
    props = {},
}, "connack")
res = cli:subscribe({ '#', 2 })
equals( res[1], 0x82, "subscribe QoS 2" )

set_buffer(string.char(
    0x30,
       9 + 6,
          0x00, 0x03, 97, 47, 98,       -- a/b
          3,
              35,
              0x90, 0x00,
          70, 111, 111, 66, 97, 114     -- FooBar
))
cli._mk_disconnect = function (reason_code)
    equals( reason_code, 148, "disconnect 148: topic alias invalid" )
    return ''
end
ret, msg = cli:read()
is_nil( ret )
equals( msg, "topic alias invalid" , "topic alias invalid" )

cli = mqtt({ socket = sock, logger = logger })
res = cli:connect({ id = 'foo' }, { mqtt.PROP.TOPIC_ALIAS_MAXIMUM, 0x8000 })
equals( res[1], 0x10, "connect with topic alias maximum" )
set_buffer(string.char(
    0x20,
    3,
        0x00,
        0x00,
        0
))
same( cli:read(), {
    type  = 'CONNACK',
    sp    = false,
    rc    = 0x00,
    props = {},
}, "connack")
res = cli:subscribe({ '#', 2 })
equals( res[1], 0x82, "subscribe QoS 2" )

set_buffer(string.char(
    0x30,
       9 + 6,
          0x00, 0x03, 97, 47, 98,       -- a/b
          3,
              35,
              0x00, 0x42,
          70, 111, 111, 66, 97, 114     -- FooBar
))
same( cli:read(), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 0,
    topic   = 'a/b',
    props   = { mqtt.PROP.TOPIC_ALIAS, 0x0042 },
    payload = 'FooBar',
} )

set_buffer(string.char(
    0x30,
       6 + 6,
          0x00, 0x00,                   --
          3,
              35,
              0x00, 0x42,
          70, 111, 111, 66, 97, 114     -- FooBar
))
same( cli:read(), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 0,
    topic   = 'a/b',
    props   = { mqtt.PROP.TOPIC_ALIAS, 0x0042 },
    payload = 'FooBar',
} )

set_buffer(string.char(
    0x30,
       9 + 6,
          0x00, 0x03, 99, 47, 100,      -- c/d
          3,
              35,
              0x00, 0x42,
          70, 111, 111, 66, 97, 114     -- FooBar
))
same( cli:read(), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 0,
    topic   = 'c/d',
    props   = { mqtt.PROP.TOPIC_ALIAS, 0x0042 },
    payload = 'FooBar',
} )

set_buffer(string.char(
    0x30,
       6 + 6,
          0x00, 0x00,                   --
          3,
              35,
              0x00, 0x42,
          70, 111, 111, 66, 97, 114     -- FooBar
))
same( cli:read(), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 0,
    topic   = 'c/d',
    props   = { mqtt.PROP.TOPIC_ALIAS, 0x0042 },
    payload = 'FooBar',
} )

set_buffer(string.char(
    0x30,
       6 + 6,
          0x00, 0x00,                   --
          3,
              35,
              0x00, 67,
          70, 111, 111, 66, 97, 114     -- FooBar
))
cli._mk_disconnect = function (reason_code)
    equals( reason_code, 130, "disconnect 130: protocol error" )
    return ''
end
ret, msg = cli:read()
is_nil( ret )
equals( msg, "unknown Topic Alias 67 in PUBLISH", "unknown topic alias" )


cli = mqtt({ socket = sock, logger = logger })
res = cli:connect({ id = 'foo' })
equals( res[1], 0x10, "connect" )

error_matches( function () cli:publish('a/b', 'FooBar', { qos = 0 }, { mqtt.PROP.TOPIC_ALIAS, 0 }) end,
               "^[^:]+:%d+: invalid Topic Alias %(0%) at index 2 in table to 'publish'" )

cli = mqtt({ socket = sock, logger = logger })
res = cli:connect({ id = 'foo' })
equals( res[1], 0x10, "connect" )
set_buffer(string.char(
    0x20,
    6,
        0x00,
        0x00,
        3,
            34,
            0x80, 0x00
))
same( cli:read(), {
    type  = 'CONNACK',
    sp    = false,
    rc    = 0x00,
    props = { mqtt.PROP.TOPIC_ALIAS_MAXIMUM, 0x8000 },
}, "connack with topic alias maximum = 0x8000")

error_matches( function () cli:publish('a/b', 'FooBar', { qos = 0 }, { mqtt.PROP.TOPIC_ALIAS, 42000 }) end,
               "^[^:]+:%d+: invalid Topic Alias %(42000%) at index 2 in table to 'publish'" )

same( cli:publish('a/b', 'FooBar', { qos = 0 }, { mqtt.PROP.TOPIC_ALIAS, 0x0042 }), {
    0x30,
       9 + 6,
          0x00, 0x03, 97, 47, 98,       -- a/b
          3,
              35,
              0x00, 0x42,
          70, 111, 111, 66, 97, 114     -- FooBar
} )

same( cli:publish('a/b', 'FooBar', { qos = 0 }, { mqtt.PROP.TOPIC_ALIAS, 0x0042 }), {
    0x30,
       6 + 6,
          0x00, 0x00,                   --
          3,
              35,
              0x00, 0x42,
          70, 111, 111, 66, 97, 114     -- FooBar
} )

same( cli:publish('c/d', 'FooBar', { qos = 0 }, { mqtt.PROP.TOPIC_ALIAS, 0x0042 }), {
    0x30,
       9 + 6,
          0x00, 0x03, 99, 47, 100,      -- c/d
          3,
              35,
              0x00, 0x42,
          70, 111, 111, 66, 97, 114     -- FooBar
} )

same( cli:publish('c/d', 'FooBar', { qos = 0 }, { mqtt.PROP.TOPIC_ALIAS, 0x0042 }), {
    0x30,
       6 + 6,
          0x00, 0x00,                   --
          3,
              35,
              0x00, 0x42,
          70, 111, 111, 66, 97, 114     -- FooBar
} )

error_matches( function () cli:publish('', 'FooBar', { qos = 0 }, { mqtt.PROP.TOPIC_ALIAS, 0x0042 }) end,
               "^[^:]+:%d+: invalid Topic Name to 'publish'" )

error_matches( function () cli:publish(nil, 'FooBar', { qos = 0 }, { mqtt.PROP.TOPIC_ALIAS, 0x0042 }) end,
               "^[^:]+:%d+: bad argument #1 to 'publish' %(string expected, got no value%)" )
