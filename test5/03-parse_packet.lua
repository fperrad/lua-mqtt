#!/usr/bin/env lua

require 'Test.Assertion'

plan(208)

local mqtt = require 'mqtt5'

local sock = {
    close = function () diag('socket:close') end,
    read  = true,
    write = function () diag('socket:write') end,
}
local cli = mqtt({
    socket = sock,
    logger = {
        debug = function (_, ...) diag(...) end,
        error = function (_, ...) diag(...) end,
        fatal = function (_, ...) diag(...) end,
        info  = function (_, ...) diag(...) end,
        warn  = function (_, ...) diag(...) end,
    },
})
is_table( cli )

same( cli:_parse_connack(string.char(
    0x01,
    0x00,
    0
)), {
    type  = 'CONNACK',
    sp    = true,
    rc    = 0x00,
    props = {},
})

same( cli:_parse_connack(string.char(
    0x00,
    0x83,
    12,
        31,
        0x00, 0x09, 70, 111, 111, 32, 101, 114, 114, 111, 114
)), {
    type  = 'CONNACK',
    sp    = false,
    rc    = 0x83,
    props = { 31, "Foo error" },
})

local ret, msg = cli:_parse_connack(string.char(
    0x02,
    0x00,
    0
))
is_nil( ret )
equals( msg, "invalid flags in CONNACK: 02", "invalid sp" )
cli.socket = sock

ret, msg = cli:_parse_connack(string.char(
    0x00,
    66,
    0
))
is_nil( ret )
equals( msg, "invalid reason in CONNACK: 66", "invalid reason" )
cli.socket = sock

ret, msg = cli:_parse_connack(string.char(
    0x01,
    0x80,
    0
))
is_nil( ret )
equals( msg, "invalid CONNACK (sp with rc)", "sp with rc" )
cli.socket = sock

ret, msg = cli:_parse_connack(string.char(
    0x00,
    0x83,
    0x80, 0x80
))
is_nil( ret )
equals( msg, "invalid data in CONNACK properties: invalid vbi (data too short)", "invalid vbi (length)" )
cli.socket = sock

ret, msg = cli:_parse_connack(string.char(
    0x00,
    0x83,
    2,
        0x80, 0x80
))
is_nil( ret )
equals( msg, "invalid data in CONNACK properties: invalid vbi (data too short)", "invalid vbi (id prop)" )
cli.socket = sock

ret, msg = cli:_parse_connack(string.char(
    0x00,
    0x83,
    5,
        0x80, 0x80, 0x80, 0x80, 0x00
))
is_nil( ret )
equals( msg, "invalid data in CONNACK properties: invalid vbi (too long)", "invalid vbi (id prop)" )
cli.socket = sock

ret, msg = cli:_parse_connack(string.char(
    0x00,
    0x83,
    2,
        99,
        0x00
))
is_nil( ret )
equals( msg, "invalid data in CONNACK property 99: unknown property", "unknown prop" )
cli.socket = sock

ret, msg = cli:_parse_connack(string.char(
    0x00,
    0x83,
    2,
        23,
        0x00
))
is_nil( ret )
equals( msg, "invalid Property 23 in CONNACK", "invalid prop" )
cli.socket = sock

ret, msg = cli:_parse_connack(string.char(
    0x00,
    0x83,
    24,
        31,
        0x00, 0x09, 70, 111, 111, 32, 101, 114, 114, 111, 114,
        31,
        0x00, 0x09, 70, 111, 111, 32, 101, 114, 114, 111, 114
))
is_nil( ret )
equals( msg, "duplicated Property 31 in CONNACK", "duplicated prop" )
cli.socket = sock

ret, msg = cli:_parse_connack(string.char(
    0x00,
    0x83,
    4,
        18,
        0x00, 0x01, 0xFF
))
is_nil( ret )
equals( msg, "invalid value for Property 18 in CONNACK", "invalid UTF8 string" )
cli.socket = sock

ret, msg = cli:_parse_connack(string.char(
    0x00,
    0x83,
    4,
        21,
        0x00, 0x01, 0xFF
))
is_nil( ret )
equals( msg, "invalid value for Property 21 in CONNACK", "invalid UTF8 string" )
cli.socket = sock

ret, msg = cli:_parse_connack(string.char(
    0x00,
    0x83,
    4,
        26,
        0x00, 0x01, 0xFF
))
is_nil( ret )
equals( msg, "invalid value for Property 26 in CONNACK", "invalid UTF8 string" )
cli.socket = sock

ret, msg = cli:_parse_connack(string.char(
    0x00,
    0x83,
    4,
        28,
        0x00, 0x01, 0xFF
))
is_nil( ret )
equals( msg, "invalid value for Property 28 in CONNACK", "invalid UTF8 string" )
cli.socket = sock

ret, msg = cli:_parse_connack(string.char(
    0x00,
    0x83,
    4,
        31,
        0x00, 0x01, 0xFF
))
is_nil( ret )
equals( msg, "invalid value for Property 31 in CONNACK", "invalid UTF8 string" )
cli.socket = sock

ret, msg = cli:_parse_connack(string.char(
    0x00,
    0x83,
    2,
        36,
        42
))
is_nil( ret )
equals( msg, "invalid value for Property 36 in CONNACK", "invalid flag" )
cli.socket = sock

ret, msg = cli:_parse_connack(string.char(
    0x00,
    0x83,
    2,
        37,
        42
))
is_nil( ret )
equals( msg, "invalid value for Property 37 in CONNACK", "invalid flag" )
cli.socket = sock

ret, msg = cli:_parse_connack(string.char(
    0x00,
    0x83,
    9,
        38,
        0x00, 0x03, 102, 111, 111, 0x00, 0x01, 0xFF
))
is_nil( ret )
equals( msg, "invalid value for Property 38 in CONNACK", "invalid UTF8 string" )
cli.socket = sock

ret, msg = cli:_parse_connack(string.char(
    0x00,
    0x83,
    2,
        40,
        42
))
is_nil( ret )
equals( msg, "invalid value for Property 40 in CONNACK", "invalid flag" )
cli.socket = sock

ret, msg = cli:_parse_connack(string.char(
    0x00,
    0x83,
    2,
        41,
        42
))
is_nil( ret )
equals( msg, "invalid value for Property 41 in CONNACK", "invalid flag" )
cli.socket = sock

ret, msg = cli:_parse_connack(string.char(
    0x00,
    0x83,
    2,
        42,
        42
))
is_nil( ret )
equals( msg, "invalid value for Property 42 in CONNACK", "invalid flag" )
cli.socket = sock

ret, msg = cli:_parse_connack(string.char(
    0x00,
    0x83,
    6,
        31,
        0x00, 0x30, 70, 111, 111
))
is_nil( ret )
if _VERSION == 'Lua 5.1' and jit then
    todo('compat53 error', 1)
end
equals( msg, "invalid data in CONNACK property 31: bad argument #2 to 'unpack' (data string too short)", "invalid message" )
cli.socket = sock

same( cli:_parse_publish(string.char(
    0x00, 0x03, 97, 47, 98,
    0,
    70, 111, 111, 66, 97, 114
), 0x0), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 0,
    topic   = 'a/b',
    props   = {},
    payload = 'FooBar',
})

same( cli:_parse_publish(string.char(
    0x00, 0x03, 97, 47, 98,
    0
), 0x0), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 0,
    topic   = 'a/b',
    props   = {},
    payload = '',
})

same( cli:_parse_publish(string.char(
    0x00, 0x03, 97, 47, 98,
    2,
        1,
        0x00,
    70, 111, 111, 66, 97, 114
), 0x0), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 0,
    topic   = 'a/b',
    props   = { 1, 0 },
    payload = 'FooBar',
})

same( cli:_parse_publish(string.char(
    0x00, 0x03, 97, 47, 98,
    5,
        2,
        0x00, 0x10, 0x20, 0x30,
    70, 111, 111, 66, 97, 114
), 0x0), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 0,
    topic   = 'a/b',
    props   = { 2, 0x00102030 },
    payload = 'FooBar',
})

same( cli:_parse_publish(string.char(
    0x00, 0x03, 97, 47, 98,
    6,
        3,
        0x00, 0x03, 102, 111, 111,
    70, 111, 111, 66, 97, 114
), 0x0), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 0,
    topic   = 'a/b',
    props   = { 3, 'foo' },
    payload = 'FooBar',
})

same( cli:_parse_publish(string.char(
    0x00, 0x03, 97, 47, 98,
    6,
        8,
        0x00, 0x03, 102, 111, 111,
    70, 111, 111, 66, 97, 114
), 0x0), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 0,
    topic   = 'a/b',
    props   = { 8, 'foo' },
    payload = 'FooBar',
})

same( cli:_parse_publish(string.char(
    0x00, 0x03, 97, 47, 98,
    6,
        9,
        0x00, 0x03, 102, 111, 111,
    70, 111, 111, 66, 97, 114
), 0x0), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 0,
    topic   = 'a/b',
    props   = { 9, 'foo' },
    payload = 'FooBar',
})

same( cli:_parse_publish(string.char(
    0x00, 0x03, 97, 47, 98,
    2,
        11,
        0x01,
    70, 111, 111, 66, 97, 114
), 0x0), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 0,
    topic   = 'a/b',
    props   = { 11, 1 },
    payload = 'FooBar',
})

same( cli:_parse_publish(string.char(
    0x00, 0x03, 97, 47, 98,
    3,
        11,
        0x80, 0x02,
    70, 111, 111, 66, 97, 114
), 0x0), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 0,
    topic   = 'a/b',
    props   = { 11, 0x100 },
    payload = 'FooBar',
})

same( cli:_parse_publish(string.char(
    0x00, 0x03, 97, 47, 98,
    4,
        11,
        0x80, 0x80, 0x04,
    70, 111, 111, 66, 97, 114
), 0x0), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 0,
    topic   = 'a/b',
    props   = { 11, 0x10000 },
    payload = 'FooBar',
})

same( cli:_parse_publish(string.char(
    0x00, 0x03, 97, 47, 98,
    3,
        35,
        0x12, 0x34,
    70, 111, 111, 66, 97, 114
), 0x0), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 0,
    topic   = 'a/b',
    props   = { 35, 0x1234 },
    payload = 'FooBar',
})

same( cli:_parse_publish(string.char(
    0x00, 0x03, 97, 47, 98,
    11,
        38,
        0x00, 0x03, 102, 111, 111, 0x00, 0x03, 98, 97, 114,
    70, 111, 111, 66, 97, 114
), 0x0), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 0,
    topic   = 'a/b',
    props   = { 38, { foo = 'bar' } },
    payload = 'FooBar',
})

same( cli:_parse_publish(string.char(
    0x00, 0x03, 97, 47, 98,
    0x12, 0x34,
    0,
    70, 111, 111, 66, 97, 114
), 0xB), {
    type    = 'PUBLISH',
    dup     = true,
    retain  = true,
    qos     = 1,
    topic   = 'a/b',
    id      = 0x1234,
    props   = {},
    payload = 'FooBar',
})

same( cli:_parse_publish(string.char(
    0x00, 0x03, 97, 47, 98,
    0x12, 0x34,
    11,
        38,
        0x00, 0x03, 102, 111, 111, 0x00, 0x03, 98, 97, 114,
    70, 111, 111, 66, 97, 114
), 0xB), {
    type    = 'PUBLISH',
    dup     = true,
    retain  = true,
    qos     = 1,
    topic   = 'a/b',
    id      = 0x1234,
    props   = { 38, { foo = 'bar' } },
    payload = 'FooBar',
})

ret, msg = cli:_parse_publish(string.char(
    0x00, 0x03, 97, 47, 98,
    0x12, 0x34,
    0,
    70, 111, 111, 66, 97, 114
), 0xF)
is_nil( ret )
equals( msg, "invalid QoS for PUBLISH", "invalid QoS" )
cli.socket = sock

ret, msg = cli:_parse_publish(string.char(
    0x00, 0x33, 97, 47, 98,
    0x12, 0x34,
    70, 111, 111, 66, 97, 114
), 0xB)
is_nil( ret )
if _VERSION == 'Lua 5.1' then
    todo('compat53 error', 1)
end
equals( msg, "invalid message for PUBLISH: bad argument #2 to 'string.unpack' (data string too short)", "data too short" )
cli.socket = sock

ret, msg = cli:_parse_publish(string.char(
    0x00, 0x03, 97, 47, 98,
    0x12
), 0xB)
is_nil( ret )
if _VERSION == 'Lua 5.1' then
    todo('compat53 error', 1)
end
equals( msg, "invalid message for PUBLISH: bad argument #2 to 'string.unpack' (data string too short)", "data too short" )
cli.socket = sock

ret, msg = cli:_parse_publish(string.char(
    0x00, 0x03, 97, 47, 98,
    2,
        23,
        0x00,
    70, 111, 111, 66, 97, 114
), 0x0)
is_nil( ret )
equals( msg, "invalid Property 23 in PUBLISH", "invalid prop" )
cli.socket = sock

ret,msg = cli:_parse_publish(string.char(
    0x00, 0x03, 97, 47, 98,
    24,
        3,
        0x00, 0x09, 70, 111, 111, 32, 101, 114, 114, 111, 114,
        3,
        0x00, 0x09, 70, 111, 111, 32, 101, 114, 114, 111, 114,
    70, 111, 111, 66, 97, 114
), 0x0)
is_nil( ret )
equals( msg, "duplicated Property 3 in PUBLISH", "duplicated prop" )
cli.socket = sock

ret, msg = cli:_parse_publish(string.char(
    0x00, 0x03, 97, 47, 98,
    2,
        1,
        42,
    70, 111, 111, 66, 97, 114
), 0x0)
is_nil( ret )
equals( msg, "invalid value for Property 1 in PUBLISH", "invalid flag" )
cli.socket = sock

ret, msg = cli:_parse_publish(string.char(
    0x00, 0x03, 97, 47, 98,
    4,
        3,
        0x00, 0x01, 0xFF,
    70, 111, 111, 66, 97, 114
), 0x0)
is_nil( ret )
equals( msg, "invalid value for Property 3 in PUBLISH", "invalid UTF8 string" )
cli.socket = sock

ret, msg = cli:_parse_publish(string.char(
    0x00, 0x03, 97, 47, 98,
    4,
        8,
        0x00, 0x01, 0xFF,
    70, 111, 111, 66, 97, 114
), 0x0)
is_nil( ret )
equals( msg, "invalid value for Property 8 in PUBLISH", "invalid UTF8 string" )
cli.socket = sock

ret, msg = cli:_parse_publish(string.char(
    0x00, 0x03, 97, 47, 98,
    6,
        11,
        0x80, 0x80, 0x80, 0x80, 0x80,
    70, 111, 111, 66, 97, 114
), 0x0)
is_nil( ret )
equals( msg, "invalid data in PUBLISH property 11: invalid vbi (too long)", "invalid vbi" )
cli.socket = sock

ret, msg = cli:_parse_publish(string.char(
    0x00, 0x03, 97, 47, 98,
    9,
        38,
        0x00, 0x03, 102, 111, 111, 0x00, 0x01, 0xFF,
    70, 111, 111, 66, 97, 114
), 0x0)
is_nil( ret )
equals( msg, "invalid value for Property 38 in PUBLISH", "invalid UTF8 string" )
cli.socket = sock

ret, msg = cli:_parse_publish(string.char(
    0x00, 0x03, 97, 47, 98,
    6,
        31,
        0x00, 0x30, 70, 111, 111,
    70, 111, 111, 66, 97, 114
), 0x00)
is_nil( ret )
if _VERSION == 'Lua 5.1' and jit then
    todo('compat53 error', 1)
end
equals( msg, "invalid data in PUBLISH property 31: bad argument #2 to 'unpack' (data string too short)", "invalid message" )
cli.socket = sock

same( cli:_parse_puback(string.char(
    0x12, 0x34
)), {
    type = 'PUBACK',
    id   = 0x1234,
    rc   = 0,
})

same( cli:_parse_puback(string.char(
    0x12, 0x34,
    0x10,
    0
)), {
    type  = 'PUBACK',
    id    = 0x1234,
    rc    = 0x10,
    props = {},
})

same( cli:_parse_puback(string.char(
    0x12, 0x34,
    0x83,
    12,
        31,
        0x00, 0x09, 70, 111, 111, 32, 101, 114, 114, 111, 114
)), {
    type  = 'PUBACK',
    id    = 0x1234,
    rc    = 0x83,
    props = { 31, "Foo error" },
})

same( cli:_parse_puback(string.char(
    0x12, 0x34,
    0x83,
    11,
        38,
        0x00, 0x03, 102, 111, 111, 0x00, 0x03, 98, 97, 114
)), {
    type  = 'PUBACK',
    id    = 0x1234,
    rc    = 0x83,
    props = { 38, { foo = 'bar' } },
})

ret, msg = cli:_parse_puback(string.char(
    0x12, 0x34,
    0x83,
    2,
        23,
        0x00
))
is_nil( ret )
equals( msg, "invalid Property 23 in PUBACK", "invalid prop" )
cli.socket = sock

ret, msg = cli:_parse_puback(string.char(
    0x12, 0x34,
    0x83,
    24,
        31,
        0x00, 0x09, 70, 111, 111, 32, 101, 114, 114, 111, 114,
        31,
        0x00, 0x09, 70, 111, 111, 32, 101, 114, 114, 111, 114
))
is_nil( ret )
equals( msg, "duplicated Property 31 in PUBACK", "duplicated prop" )
cli.socket = sock

ret, msg = cli:_parse_puback(string.char(
    0x12, 0x34,
    0x83,
    4,
        31,
        0x00, 0x01, 0xFF
))
is_nil( ret )
equals( msg, "invalid value for Property 31 in PUBACK", "invalid UTF8 string" )
cli.socket = sock

ret, msg = cli:_parse_puback(string.char(
    0x12, 0x34,
    0x83,
    9,
        38,
        0x00, 0x03, 102, 111, 111, 0x00, 0x01, 0xFF
))
is_nil( ret )
equals( msg, "invalid value for Property 38 in PUBACK", "invalid UTF8 string" )
cli.socket = sock

ret, msg = cli:_parse_puback(string.char(
    0x12, 0x34,
    0x83,
    6,
        31,
        0x00, 0x30, 70, 111, 111
))
is_nil( ret )
if _VERSION == 'Lua 5.1' and jit then
    todo('compat53 error', 1)
end
equals( msg, "invalid data in PUBACK property 31: bad argument #2 to 'unpack' (data string too short)", "invalid message" )
cli.socket = sock

same( cli:_parse_pubrec(string.char(
    0x12, 0x34
)), {
    type = 'PUBREC',
    id   = 0x1234,
    rc   = 0,
})

same( cli:_parse_pubrec(string.char(
    0x12, 0x34,
    0x10,
    0
)), {
    type  = 'PUBREC',
    id    = 0x1234,
    rc    = 0x10,
    props = {},
})

same( cli:_parse_pubrec(string.char(
    0x12, 0x34,
    0x83,
    12,
        31,
        0x00, 0x09, 70, 111, 111, 32, 101, 114, 114, 111, 114
)), {
    type  = 'PUBREC',
    id    = 0x1234,
    rc    = 0x83,
    props = { 31, "Foo error" },
})

same( cli:_parse_pubrec(string.char(
    0x12, 0x34,
    0x83,
    11,
        38,
        0x00, 0x03, 102, 111, 111, 0x00, 0x03, 98, 97, 114
)), {
    type  = 'PUBREC',
    id    = 0x1234,
    rc    = 0x83,
    props = { 38, { foo = 'bar' } },
})

ret, msg = cli:_parse_pubrec(string.char(
    0x12, 0x34,
    0x83,
    2,
        23,
        0x00
))
is_nil( ret )
equals( msg, "invalid Property 23 in PUBREC", "invalid prop" )
cli.socket = sock

ret, msg = cli:_parse_pubrec(string.char(
    0x12, 0x34,
    0x83,
    24,
        31,
        0x00, 0x09, 70, 111, 111, 32, 101, 114, 114, 111, 114,
        31,
        0x00, 0x09, 70, 111, 111, 32, 101, 114, 114, 111, 114
))
is_nil( ret )
equals( msg, "duplicated Property 31 in PUBREC", "duplicated prop" )
cli.socket = sock

ret, msg = cli:_parse_pubrec(string.char(
    0x12, 0x34,
    0x83,
    4,
        31,
        0x00, 0x01, 0xFF
))
is_nil( ret )
equals( msg, "invalid value for Property 31 in PUBREC", "invalid UTF8 string" )
cli.socket = sock

ret, msg = cli:_parse_pubrec(string.char(
    0x12, 0x34,
    0x83,
    9,
        38,
        0x00, 0x03, 102, 111, 111, 0x00, 0x01, 0xFF
))
is_nil( ret )
equals( msg, "invalid value for Property 38 in PUBREC", "invalid UTF8 string" )
cli.socket = sock

ret, msg = cli:_parse_pubrec(string.char(
    0x12, 0x34,
    0x83,
    6,
        31,
        0x00, 0x30, 70, 111, 111
))
is_nil( ret )
if _VERSION == 'Lua 5.1' and jit then
    todo('compat53 error', 1)
end
equals( msg, "invalid data in PUBREC property 31: bad argument #2 to 'unpack' (data string too short)", "invalid message" )
cli.socket = sock

same( cli:_parse_pubrel(string.char(
    0x12, 0x34
)), {
    type = 'PUBREL',
    id   = 0x1234,
    rc   = 0,
})

same( cli:_parse_pubrel(string.char(
    0x12, 0x34,
    0x92,
    0
)), {
    type  = 'PUBREL',
    id    = 0x1234,
    rc    = 0x92,
    props = {},
})

same( cli:_parse_pubrel(string.char(
    0x12, 0x34,
    0x92,
    12,
        31,
        0x00, 0x09, 70, 111, 111, 32, 101, 114, 114, 111, 114
)), {
    type  = 'PUBREL',
    id    = 0x1234,
    rc    = 0x92,
    props = { 31, "Foo error" },
})

same( cli:_parse_pubrel(string.char(
    0x12, 0x34,
    0x92,
    11,
        38,
        0x00, 0x03, 102, 111, 111, 0x00, 0x03, 98, 97, 114
)), {
    type  = 'PUBREL',
    id    = 0x1234,
    rc    = 0x92,
    props = { 38, { foo = 'bar' } },
})

ret, msg = cli:_parse_pubrel(string.char(
    0x12, 0x34,
    0x92,
    2,
        23,
        0x00
))
is_nil( ret )
equals( msg, "invalid Property 23 in PUBREL", "invalid prop" )
cli.socket = sock

ret, msg = cli:_parse_pubrel(string.char(
    0x12, 0x34,
    0x92,
    24,
        31,
        0x00, 0x09, 70, 111, 111, 32, 101, 114, 114, 111, 114,
        31,
        0x00, 0x09, 70, 111, 111, 32, 101, 114, 114, 111, 114
))
is_nil( ret )
equals( msg, "duplicated Property 31 in PUBREL", "duplicated prop" )
cli.socket = sock

ret, msg = cli:_parse_pubrel(string.char(
    0x12, 0x34,
    0x92,
    4,
        31,
        0x00, 0x01, 0xFF
))
is_nil( ret )
equals( msg, "invalid value for Property 31 in PUBREL", "invalid UTF8 string" )
cli.socket = sock

ret, msg = cli:_parse_pubrel(string.char(
    0x12, 0x34,
    0x92,
    9,
        38,
        0x00, 0x03, 102, 111, 111, 0x00, 0x01, 0xFF
))
is_nil( ret )
equals( msg, "invalid value for Property 38 in PUBREL", "invalid UTF8 string" )
cli.socket = sock

ret, msg = cli:_parse_pubrel(string.char(
    0x12, 0x34,
    0x92,
    6,
        31,
        0x00, 0x30, 70, 111, 111
))
is_nil( ret )
if _VERSION == 'Lua 5.1' and jit then
    todo('compat53 error', 1)
end
equals( msg, "invalid data in PUBREL property 31: bad argument #2 to 'unpack' (data string too short)", "invalid message" )
cli.socket = sock

same( cli:_parse_pubcomp(string.char(
    0x12, 0x34
)), {
    type = 'PUBCOMP',
    id   = 0x1234,
    rc   = 0,
})

same( cli:_parse_pubcomp(string.char(
    0x12, 0x34,
    0x92,
    0
)), {
    type  = 'PUBCOMP',
    id    = 0x1234,
    rc    = 0x92,
    props = {},
})

same( cli:_parse_pubcomp(string.char(
    0x12, 0x34,
    0x92,
    12,
        31,
        0x00, 0x09, 70, 111, 111, 32, 101, 114, 114, 111, 114
)), {
    type  = 'PUBCOMP',
    id    = 0x1234,
    rc    = 0x92,
    props = { 31, "Foo error" },
})

same( cli:_parse_pubcomp(string.char(
    0x12, 0x34,
    0x92,
    11,
        38,
        0x00, 0x03, 102, 111, 111, 0x00, 0x03, 98, 97, 114
)), {
    type  = 'PUBCOMP',
    id    = 0x1234,
    rc    = 0x92,
    props = { 38, { foo = 'bar' } },
})

ret, msg = cli:_parse_pubcomp(string.char(
    0x12, 0x34,
    0x92,
    2,
        23,
        0x00
))
is_nil( ret )
equals( msg, "invalid Property 23 in PUBCOMP", "invalid prop")
cli.socket = sock

ret, msg = cli:_parse_pubcomp(string.char(
    0x12, 0x34,
    0x92,
    24,
        31,
        0x00, 0x09, 70, 111, 111, 32, 101, 114, 114, 111, 114,
        31,
        0x00, 0x09, 70, 111, 111, 32, 101, 114, 114, 111, 114
))
is_nil( ret )
equals( msg, "duplicated Property 31 in PUBCOMP", "duplicated prop" )
cli.socket = sock

ret, msg = cli:_parse_pubcomp(string.char(
    0x12, 0x34,
    0x92,
    4,
        31,
        0x00, 0x01, 0xFF
))
is_nil( ret )
equals( msg, "invalid value for Property 31 in PUBCOMP", "invalid UTF8 string" )
cli.socket = sock

ret, msg = cli:_parse_pubcomp(string.char(
    0x12, 0x34,
    0x92,
    9,
        38,
        0x00, 0x03, 102, 111, 111, 0x00, 0x01, 0xFF
))
is_nil( ret )
equals( msg, "invalid value for Property 38 in PUBCOMP", "invalid UTF8 string" )
cli.socket = sock

ret, msg = cli:_parse_pubcomp(string.char(
    0x12, 0x34,
    0x92,
    6,
        31,
        0x00, 0x30, 70, 111, 111
))
is_nil( ret )
if _VERSION == 'Lua 5.1' and jit then
    todo('compat53 error', 1)
end
equals( msg, "invalid data in PUBCOMP property 31: bad argument #2 to 'unpack' (data string too short)", "invalid message" )
cli.socket = sock

same( cli:_parse_suback(string.char(
    0x12, 0x34,
    0,
    0x00, 0x00
)), {
    type    = 'SUBACK',
    id      = 0x1234,
    props   = {},
    payload = { 0x00, 0x00 },
})

ret, msg = cli:_parse_suback(string.char(
    0x12, 0x34,
    0,
    0x00, 0x05
))
is_nil( ret )
equals( msg, "invalid reason in SUBACK: 5", "bad reason" )
cli.socket = sock

same( cli:_parse_suback(string.char(
    0x12, 0x34,
    12,
        31,
        0x00, 0x09, 70, 111, 111, 32, 101, 114, 114, 111, 114,
    0x00, 0x00
)), {
    type    = 'SUBACK',
    id      = 0x1234,
    props   = { 31, "Foo error" },
    payload = { 0x00, 0x00 },
})

same( cli:_parse_suback(string.char(
    0x12, 0x34,
    11,
        38,
        0x00, 0x03, 102, 111, 111, 0x00, 0x03, 98, 97, 114,
    0x00, 0x00
)), {
    type    = 'SUBACK',
    id      = 0x1234,
    props   = { 38, { foo = 'bar' } },
    payload = { 0x00, 0x00 },
})

ret, msg = cli:_parse_suback(string.char(
    0x12, 0x34,
    2,
        23,
        0x00
))
is_nil( ret )
equals( msg, "invalid Property 23 in SUBACK", "invalid prop" )
cli.socket = sock

ret, msg = cli:_parse_suback(string.char(
    0x12, 0x34,
    24,
        31,
        0x00, 0x09, 70, 111, 111, 32, 101, 114, 114, 111, 114,
        31,
        0x00, 0x09, 70, 111, 111, 32, 101, 114, 114, 111, 114
))
is_nil( ret )
equals( msg, "duplicated Property 31 in SUBACK", "duplicated prop" )
cli.socket = sock

ret, msg = cli:_parse_suback(string.char(
    0x12, 0x34,
    4,
        31,
        0x00, 0x01, 0xFF
))
is_nil( ret )
equals( msg, "invalid value for Property 31 in SUBACK", "invalid UTF8 string" )
cli.socket = sock

ret, msg = cli:_parse_suback(string.char(
    0x12, 0x34,
    9,
        38,
        0x00, 0x03, 102, 111, 111, 0x00, 0x01, 0xFF
))
is_nil( ret )
equals( msg, "invalid value for Property 38 in SUBACK", "invalid UTF8 string" )
cli.socket = sock

ret, msg = cli:_parse_suback(string.char(
    0x12, 0x34,
    6,
        31,
        0x00, 0x30, 70, 111, 111
))
is_nil( ret )
if _VERSION == 'Lua 5.1' and jit then
    todo('compat53 error', 1)
end
equals( msg, "invalid data in SUBACK property 31: bad argument #2 to 'unpack' (data string too short)", "invalid message" )
cli.socket = sock

same( cli:_parse_unsuback(string.char(
    0x12, 0x34,
    0,
    0x00, 0x00
)), {
    type    = 'UNSUBACK',
    id      = 0x1234,
    props   = {},
    payload = { 0x00, 0x00 },
})

ret, msg = cli:_parse_unsuback(string.char(
    0x12, 0x34,
    0,
    0x00, 0x05
))
is_nil( ret )
equals( msg, "invalid reason in UNSUBACK: 5", "bad reason" )
cli.socket = sock

same( cli:_parse_unsuback(string.char(
    0x12, 0x34,
    12,
        31,
        0x00, 0x09, 70, 111, 111, 32, 101, 114, 114, 111, 114,
    0x00, 0x00
)), {
    type    = 'UNSUBACK',
    id      = 0x1234,
    props   = { 31, "Foo error" },
    payload = { 0x00, 0x00 },
})

same( cli:_parse_unsuback(string.char(
    0x12, 0x34,
    11,
        38,
        0x00, 0x03, 102, 111, 111, 0x00, 0x03, 98, 97, 114,
    0x00, 0x00
)), {
    type    = 'UNSUBACK',
    id      = 0x1234,
    props   = { 38, { foo = 'bar' } },
    payload = { 0x00, 0x00 },
})

ret, msg = cli:_parse_unsuback(string.char(
    0x12, 0x34,
    2,
        23,
        0x00
))
is_nil( ret )
equals( msg, "invalid Property 23 in UNSUBACK", "invalid prop" )
cli.socket = sock

ret, msg = cli:_parse_unsuback(string.char(
    0x12, 0x34,
    24,
        31,
        0x00, 0x09, 70, 111, 111, 32, 101, 114, 114, 111, 114,
        31,
        0x00, 0x09, 70, 111, 111, 32, 101, 114, 114, 111, 114
))
is_nil( ret )
equals( msg, "duplicated Property 31 in UNSUBACK", "duplicated prop" )
cli.socket = sock

ret, msg = cli:_parse_unsuback(string.char(
    0x12, 0x34,
    4,
        31,
        0x00, 0x01, 0xFF
))
is_nil( ret )
equals( msg, "invalid value for Property 31 in UNSUBACK", "invalid UTF8 string" )
cli.socket = sock

ret, msg = cli:_parse_unsuback(string.char(
    0x12, 0x34,
    9,
        38,
        0x00, 0x03, 102, 111, 111, 0x00, 0x01, 0xFF
))
is_nil( ret )
equals( msg, "invalid value for Property 38 in UNSUBACK", "invalid UTF8 string" )
cli.socket = sock

ret, msg = cli:_parse_unsuback(string.char(
    0x12, 0x34,
    6,
        31,
        0x00, 0x30, 70, 111, 111
))
is_nil( ret )
if _VERSION == 'Lua 5.1' and jit then
    todo('compat53 error', 1)
end
equals( msg, "invalid data in UNSUBACK property 31: bad argument #2 to 'unpack' (data string too short)", "invalid message" )
cli.socket = sock

same( cli:_parse_pingresp(), {
    type = 'PINGRESP',
})

same( cli:_parse_disconnect(), {
    type  = 'DISCONNECT',
    rc    = 0,
})

same( cli:_parse_disconnect(string.char(
    0x83,
    0
)), {
    type  = 'DISCONNECT',
    rc    = 0x83,
    props = {},
})

ret, msg = cli:_parse_disconnect(string.char(
    0x01,
    0
))
is_nil( ret )
equals( msg, "invalid reason in DISCONNECT: 1", "bad reason" )
cli.socket = sock

same( cli:_parse_disconnect(string.char(
    0x83,
    5,
        17,
        0x00, 0x10, 0x20, 0x30
)), {
    type  = 'DISCONNECT',
    rc    = 0x83,
    props = { 17, 0x00102030 },
})

same( cli:_parse_disconnect(string.char(
    0x83,
    6,
        28,
        0x00, 0x03, 70, 111, 111
)), {
    type  = 'DISCONNECT',
    rc    = 0x83,
    props = { 28, "Foo" },
})

same( cli:_parse_disconnect(string.char(
    0x83,
    12,
        31,
        0x00, 0x09, 70, 111, 111, 32, 101, 114, 114, 111, 114
)), {
    type  = 'DISCONNECT',
    rc    = 0x83,
    props = { 31, "Foo error" },
})

same( cli:_parse_disconnect(string.char(
    0x83,
    11,
        38,
        0x00, 0x03, 102, 111, 111, 0x00, 0x03, 98, 97, 114
)), {
    type  = 'DISCONNECT',
    rc    = 0x83,
    props = { 38, { foo = 'bar' } },
})

ret, msg = cli:_parse_disconnect(string.char(
    0x83,
    2,
        23,
        0x00
))
is_nil( ret )
equals( msg, "invalid Property 23 in DISCONNECT", "invalid prop" )
cli.socket = sock

ret, msg = cli:_parse_disconnect(string.char(
    0x83,
    24,
        31,
        0x00, 0x09, 70, 111, 111, 32, 101, 114, 114, 111, 114,
        31,
        0x00, 0x09, 70, 111, 111, 32, 101, 114, 114, 111, 114
))
is_nil( ret )
equals( msg,"duplicated Property 31 in DISCONNECT", "duplicated prop" )
cli.socket = sock

ret, msg = cli:_parse_disconnect(string.char(
    0x83,
    4,
        28,
        0x00, 0x01, 0xFF
))
is_nil( ret )
equals( msg, "invalid value for Property 28 in DISCONNECT", "invalid UTF8 string" )
cli.socket = sock

ret, msg = cli:_parse_disconnect(string.char(
    0x83,
    4,
        31,
        0x00, 0x01, 0xFF
))
is_nil( ret )
equals( msg, "invalid value for Property 31 in DISCONNECT", "invalid UTF8 string" )
cli.socket = sock

ret, msg = cli:_parse_disconnect(string.char(
    0x83,
    9,
        38,
        0x00, 0x03, 102, 111, 111, 0x00, 0x01, 0xFF
))
is_nil( ret )
equals( msg, "invalid value for Property 38 in DISCONNECT", "invalid UTF8 string" )
cli.socket = sock

ret, msg = cli:_parse_disconnect(string.char(
    0x83,
    6,
        31,
        0x00, 0x30, 70, 111, 111
))
is_nil( ret )
if _VERSION == 'Lua 5.1' and jit then
    todo('compat53 error', 1)
end
equals( msg, "invalid data in DISCONNECT property 31: bad argument #2 to 'unpack' (data string too short)", "invalid message" )
cli.socket = sock

same( cli:_parse_auth(), {
    type  = 'AUTH',
    rc    = 0,
})

same( cli:_parse_auth(string.char(
    0x18,
    0
)), {
    type  = 'AUTH',
    rc    = 0x18,
    props = {},
})

ret, msg = cli:_parse_auth(string.char(
    0x01,
    0
))
is_nil( ret )
equals( msg, "invalid reason in AUTH: 1", "invalid reason")
cli.socket = sock

same( cli:_parse_auth(string.char(
    0x18,
    6,
        21,
        0x00, 0x03, 70, 111, 111
)), {
    type  = 'AUTH',
    rc    = 0x18,
    props = { 21, "Foo" },
})

same( cli:_parse_auth(string.char(
    0x18,
    6,
        22,
        0x00, 0x03, 70, 111, 111
)), {
    type  = 'AUTH',
    rc    = 0x18,
    props = { 22, "Foo" },
})

same( cli:_parse_auth(string.char(
    0x18,
    12,
        31,
        0x00, 0x09, 70, 111, 111, 32, 101, 114, 114, 111, 114
)), {
    type  = 'AUTH',
    rc    = 0x18,
    props = { 31, "Foo error" },
})

same( cli:_parse_auth(string.char(
    0x18,
    11,
        38,
        0x00, 0x03, 102, 111, 111, 0x00, 0x03, 98, 97, 114
)), {
    type  = 'AUTH',
    rc    = 0x18,
    props = { 38, { foo = 'bar' } },
})

ret, msg = cli:_parse_auth(string.char(
    0x18,
    2,
        23,
        0x00
))
is_nil( ret )
equals( msg,"invalid Property 23 in AUTH", "invalid prop")
cli.socket = sock

ret, msg = cli:_parse_auth(string.char(
    0x18,
    24,
        31,
        0x00, 0x09, 70, 111, 111, 32, 101, 114, 114, 111, 114,
        31,
        0x00, 0x09, 70, 111, 111, 32, 101, 114, 114, 111, 114
))
is_nil( ret )
equals( msg, "duplicated Property 31 in AUTH", "duplicated prop" )
cli.socket = sock

ret, msg = cli:_parse_auth(string.char(
    0x18,
    4,
        21,
        0x00, 0x01, 0xFF
))
is_nil( ret )
equals( msg, "invalid value for Property 21 in AUTH", "invalid UTF8 string" )
cli.socket = sock

ret, msg = cli:_parse_auth(string.char(
    0x18,
    4,
        31,
        0x00, 0x01, 0xFF
))
is_nil( ret )
equals( msg, "invalid value for Property 31 in AUTH", "invalid UTF8 string" )
cli.socket = sock

ret, msg = cli:_parse_auth(string.char(
    0x18,
    9,
        38,
        0x00, 0x03, 102, 111, 111, 0x00, 0x01, 0xFF
))
is_nil( ret )
equals( msg, "invalid value for Property 38 in AUTH", "invalid UTF8 string" )
cli.socket = sock

ret, msg = cli:_parse_auth(string.char(
    0x18,
    6,
        31,
        0x00, 0x30, 70, 111, 111
))
is_nil( ret )
if _VERSION == 'Lua 5.1' and jit then
    todo('compat53 error', 1)
end
equals( msg, "invalid data in AUTH property 31: bad argument #2 to 'unpack' (data string too short)", "invalid message" )
cli.socket = sock
