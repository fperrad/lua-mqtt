#!/usr/bin/env lua

require 'Test.Assertion'

plan(90)

local mqtt = require 'mqtt5'

local buffer
local pos
local function set_buffer (s)
    buffer = s
    pos = 1
end
local sock = {
    close = function ()
        diag('socket:close')
    end,
    read  = function (_, n)
        local s = string.sub(buffer, pos, pos + n -1)
        pos = pos + n
        if s == '' then
            return nil, 'EMPTY'
        else
            return s
        end
    end,
    write = function ()
        diag('socket:write')
    end,
}

local logger = {
    debug = function (_, ...) diag(...) end,
    error = function (_, ...) diag(...) end,
    fatal = function (_, ...) diag(...) end,
    info  = function (_, ...) diag(...) end,
    warn  = function (_, ...) diag(...) end,
}

local cli = mqtt({
    socket = sock,
    logger = logger,
})
is_table( cli )
local reason = 129
cli._mk_disconnect = function (reason_code)
    equals( reason_code, reason, "disconnect " .. tostring(reason) )
    return ''
end
cli:connect()
cli:subscribe({ '#', 2 })

set_buffer('')
local ret, msg = cli:read()
is_nil( ret )
equals( msg, "EMPTY", "no input" )
cli.socket = sock

set_buffer(string.char(
    0x60
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "invalid flags: 60", "invalid flag" )
cli.socket = sock

set_buffer(string.char(
    0x62
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "EMPTY", "missing data" )
cli.socket = sock

set_buffer(string.char(
    0xD0,
    0x80, 0x80, 0x80, 0x80, 0x80
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "invalid vbi: too long", "invalid vbi" )
cli.socket = sock

set_buffer(string.char(
    0xD0,
    1
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "invalid length 1 for: D0", "invalid length" )
cli.socket = sock

set_buffer(string.char(
    0x30,
    1
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "invalid length 1 for: 30" , "invalid length" )
cli.socket = sock

set_buffer(string.char(
    0x20,
    3,
        0x01
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "missing data", "missing data" )
cli.socket = sock

set_buffer(string.char(
    0x30,
    0x80 + 65, 2,
        0x01
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "missing data", "missing data" )
cli.socket = sock

set_buffer(string.char(
    0x20,
    3,
        0x00,
        0x00,
        0
))
same( cli:read(), {
    type  = 'CONNACK',
    sp    = false,
    rc    = 0x00,
    props = {},
}, "connack")

set_buffer(string.char(
    0x30,
    12,
        0x00, 0x03, 97, 47, 98,
        0,
        70, 111, 111, 66, 97, 114
))
same( cli:read(), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 0,
    topic   = 'a/b',
    props   = {},
    payload = 'FooBar',
}, "message 0" )

set_buffer(string.char(
    0x3B,
    14,
        0x00, 0x03, 97, 47, 98,
        0x12, 0x34,
        0,
        70, 111, 111, 66, 97, 114
))
same( cli:read(), {
    type    = 'PUBLISH',
    dup     = true,
    retain  = true,
    qos     = 1,
    topic   = 'a/b',
    id      = 0x1234,
    props   = {},
    payload = 'FooBar',
}, "message 1" )

set_buffer(string.char(
    0x34,
    14,
        0x00, 0x03, 97, 47, 98,
        0x12, 0x34,
        0,
        70, 111, 111, 66, 97, 114
))
same( cli:read(), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 2,
    topic   = 'a/b',
    id      = 0x1234,
    props   = {},
    payload = 'FooBar',
}, "message 2" )

set_buffer(string.char(
    0x32,
       14,
          0x00, 0x03, 97, 43, 98,       -- a+b
          0x12, 0x34,
          0,
          70, 111, 111, 66, 97, 114
))
same( cli:read(), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 1,
    topic   = 'a+b',
    id      = 0x1234,
    props   = {},
    payload = 'FooBar',
}, "topic name invalid" )

set_buffer(string.char(
    0x32,
       10 + 1,
          0x00, 0x03, 97, 47, 98,       -- a/b
          0x12, 0x34,
          2,
              1,
              1,
          0xFF
))
if _VERSION == 'Lua 5.1' and not jit then
    todo('compat53 utf8', 1)
end
same( cli:read(), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 1,
    topic   = 'a/b',
    id      = 0x1234,
    props   = { mqtt.PROP.PAYLOAD_FORMAT_INDICATOR, 1 },
    payload = '\xFF',
}, "payload format invalid" )

set_buffer(string.char(
    0x40,
    4,
        0x12, 0x34,
        0,
        0
))
cli.session.publish[0x1234] = {}
same( cli:read(), {
    type  = 'PUBACK',
    id    = 0x1234,
    rc    = 0,
    props = {},
}, "puback")

set_buffer(string.char(
    0x40,
    4,
        0x12, 0x34,
        0,
        0
))
reason = 130
ret, msg = cli:read()
is_nil( ret )
equals( msg, "mismatch for PUBLISH/PUBACK", "mismatch publish/puback")
cli.socket = sock

set_buffer(string.char(
    0x50,
    4,
        0x12, 0x34,
        0,
        0
))
cli.session.publish[0x1234] = {}
same( cli:read(), {
    type  = 'PUBREC',
    id    = 0x1234,
    rc    = 0,
    props = {},
}, "pubrec")

set_buffer(string.char(
    0x50,
    4,
        0x12, 0x34,
        0,
        0
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "mismatch for PUBLISH/PUBREC", "mismatch publish/pubrec" )
cli.socket = sock

set_buffer(string.char(
    0x62,
    4,
        0x12, 0x34,
        0,
        0
))
same( cli:read(), {
    type  = 'PUBREL',
    id    = 0x1234,
    rc    = 0,
    props = {},
}, "pubrel")

set_buffer(string.char(
    0x62,
    4,
        0x12, 0x34,
        0,
        0
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "mismatch for PUBREC/PUBREL", "mismatch pubrec/pubrel" )
cli.socket = sock

set_buffer(string.char(
    0x70,
    4,
        0x12, 0x34,
        0,
        0
))
same( cli:read(), {
    type  = 'PUBCOMP',
    id    = 0x1234,
    rc    = 0,
    props = {},
}, "pubcomp")

set_buffer(string.char(
    0x70,
    4,
        0x12, 0x34,
        0,
        0
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "mismatch for PUBREL/PUBCOMP", "mismatch mubrel/pubcomp" )
cli.socket = sock

set_buffer(string.char(
    0x90,
    5,
        0x12, 0x34,
        0,
        0x00, 0x00
))
cli.session.subscribe[0x1234] = { { 'a/b', 1, 'c/d', 2 }, {} }
same( cli:read(), {
    type    = 'SUBACK',
    id      = 0x1234,
    props   = {},
    payload = { 0x00, 0x00 },
})

set_buffer(string.char(
    0x90,
    5,
        0x12, 0x34,
        0,
        0x00, 0x00
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "mismatch for SUBSCRIBE/SUBACK", "mismatch subscribe/suback" )
cli.socket = sock

set_buffer(string.char(
    0xB0,
    5,
        0x12, 0x34,
        0,
        0x00, 0x00
))
cli.session.unsubscribe[0x1234] = { { 'a/b', 'c/d' }, {} }
same( cli:read(), {
    type    = 'UNSUBACK',
    id      = 0x1234,
    props   = {},
    payload = { 0x00, 0x00 },
})

set_buffer(string.char(
    0xB0,
    5,
        0x12, 0x34,
        0,
        0x00, 0x00
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "mismatch for UNSUBSCRIBE/UNSUBACK", "mismatch unsubscribe/unsuback" )
cli.socket = sock

cli.pingreq_sent = true
set_buffer(string.char(
    0xD0,
    0
))
same( cli:read(), {
    type = 'PINGRESP',
}, "pingresp")

set_buffer(string.char(
    0xD0,
    0
))
reason = 129

set_buffer(string.char(
    0xE0,
    2,
        0x83,
        0
))
same( cli:read(), {
    type  = 'DISCONNECT',
    rc    = 0x83,
    props = {},
}, "disconnect")

cli.connack_received = false
set_buffer(string.char(
    0xF0,
    2,
        0x18,
        0
))
same( cli:read(), {
    type  = 'AUTH',
    rc    = 0x18,
    props = {},
}, "auth")

set_buffer(string.char(
    0x04,
    2,
        0,
        0
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "invalid packet control: 04", "unknown" )
cli.socket = sock

set_buffer(string.char(
    0x04,
    99
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "EMPTY", "unknown" )
cli.socket = sock


cli = mqtt({
    socket = sock,
    logger = logger,
})
cli._mk_disconnect = function (reason_code)
    equals( reason_code, 130, "disconnect 130" )
    return ''
end

set_buffer(string.char(
    0x30,
    12,
        0x00, 0x03, 97, 47, 98,
        0,
        70, 111, 111, 66, 97, 114
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "CONNACK not received", "publish before connack" )
cli.socket = sock

set_buffer(string.char(
    0x40,
    4,
        0x12, 0x34,
        0,
        0
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "CONNACK not received", "puback before connack" )
cli.socket = sock

set_buffer(string.char(
    0x50,
    4,
        0x12, 0x34,
        0,
        0
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "CONNACK not received", "pubrec before connack" )
cli.socket = sock

set_buffer(string.char(
    0x62,
    4,
        0x12, 0x34,
        0,
        0
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "CONNACK not received", "pubrel before connack" )
cli.socket = sock

set_buffer(string.char(
    0x70,
    4,
        0x12, 0x34,
        0,
        0
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "CONNACK not received", "pubcomp before connack" )
cli.socket = sock

set_buffer(string.char(
    0x90,
    5,
        0x12, 0x34,
        0,
        0x00, 0x00
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "CONNACK not received", "suback before connack" )
cli.socket = sock

set_buffer(string.char(
    0xB0,
    5,
        0x12, 0x34,
        0,
        0x00, 0x00
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "CONNACK not received", "unsuback before connack" )
cli.socket = sock

cli.pingreq_sent = true
set_buffer(string.char(
    0xD0,
    0
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "CONNACK not received", "pingresp before connack" )
cli.socket = sock

set_buffer(string.char(
    0x20,
    3,
        0x01,
        0x00,
        0
))
truthy( cli:read(), "connack")

set_buffer(string.char(
    0x20,
    3,
        0x01,
        0x00,
        0
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "CONNACK already received", "connack after connack" )
cli.socket = sock

set_buffer(string.char(
    0xF0,
    2,
        0x18,
        0
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "CONNACK already received", "auth after connack" )
cli.socket = sock

