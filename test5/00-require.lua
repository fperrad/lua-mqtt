#!/usr/bin/env lua

require 'Test.Assertion'

plan(20)

if not require_ok 'mqtt5' then
    BAIL_OUT "no lib"
end

local m = require 'mqtt5'
is_table( m, 'module' )
equals( m, package.loaded['mqtt5'], 'package.loaded' )

is_function( m.match, 'function match' )

local sock = { close = true, read = true, write = true }
local o = m({ socket = sock })
is_table( o, 'instance' )
is_function( o.connect, 'meth connect' )
is_function( o.reconnect, 'meth reconnect' )
is_function( o.publish, 'meth publish' )
is_function( o.subscribe, 'meth subscribe' )
is_function( o.unsubscribe, 'meth unsubscribe' )
is_function( o.ping, 'meth ping' )
is_function( o.disconnect, 'meth disconnect' )
is_function( o.auth, 'meth auth' )
is_function( o.read, 'meth read' )

is_table( m.PROP )

equals( m._NAME, 'mqtt5', "_NAME" )
matches( m._COPYRIGHT, 'Perrad', "_COPYRIGHT" )
matches( m._DESCRIPTION, 'client .- 5', "_DESCRIPTION" )
is_string( m._VERSION, "_VERSION" )
matches( m._VERSION, '^%d%.%d%.%d$' )
