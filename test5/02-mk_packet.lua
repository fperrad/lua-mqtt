#!/usr/bin/env lua

require 'Test.Assertion'

plan(52)

local mqtt = require 'mqtt5'

local sock = { close = true, read = true, write = true }
local cli = mqtt({ socket = sock })
is_table( cli )

equals( cli._mk_connect(0x00, 0x1234, 'CLIENT'), string.char(
    0x10,
       11 + 8,
          0x00, 0x04, 77, 81, 84, 84,   -- MQTT
          5,
          0x00,                         -- connect flags
          0x12, 0x34,                   -- keep alive
          0,
          -- payload
          0x00, 0x06, 67, 76, 73, 69, 78, 84                    -- CLIENT
) )

equals( cli._mk_connect(0xC6, 0x1234, '', {}, 'top/ic', 'Message', 'user', 'passwd'), string.char(
    0x10,
       11 + 34,
          0x00, 0x04, 77, 81, 84, 84,   -- MQTT
          5,
          0xC6,                         -- connect flags
          0x12, 0x34,                   -- keep alive
          0,
          -- payload
          0x00, 0x00,                                           -- ''
          0,
          0x00, 0x06, 116, 111, 112, 47, 105, 99,               -- top/ic
          0x00, 0x07, 77, 101, 115, 115, 97, 103, 101,          -- Message
          0x00, 0x04, 117, 115, 101, 114,                       -- user
          0x00, 0x06, 112, 97, 115, 115, 119, 100               -- passwd
) )

equals( cli._mk_connect(0xC6, 0x1234, '', { 38, { foo = 'bar' } }, 'top/ic', 'Message', 'user', 'passwd'), string.char(
    0x10,
       11 + 45,
          0x00, 0x04, 77, 81, 84, 84,   -- MQTT
          5,
          0xC6,                         -- connect flags
          0x12, 0x34,                   -- keep alive
          0,
          -- payload
          0x00, 0x00,                                           -- ''
          11,
              38,
              0x00, 0x03, 102, 111, 111, 0x00, 0x03, 98, 97, 114,
          0x00, 0x06, 116, 111, 112, 47, 105, 99,               -- top/ic
          0x00, 0x07, 77, 101, 115, 115, 97, 103, 101,          -- Message
          0x00, 0x04, 117, 115, 101, 114,                       -- user
          0x00, 0x06, 112, 97, 115, 115, 119, 100               -- passwd
) )

equals( cli._mk_connect(0xC6, 0x1234, '', {}, 'top/ic', 'Message', 'user', 'passwd', { 38, { foo = 'bar' } }), string.char(
    0x10,
       22 + 34,
          0x00, 0x04, 77, 81, 84, 84,   -- MQTT
          5,
          0xC6,                         -- connect flags
          0x12, 0x34,                   -- keep alive
          11,
              38,
              0x00, 0x03, 102, 111, 111, 0x00, 0x03, 98, 97, 114,
          -- payload
          0x00, 0x00,                                           -- ''
          0,
          0x00, 0x06, 116, 111, 112, 47, 105, 99,               -- top/ic
          0x00, 0x07, 77, 101, 115, 115, 97, 103, 101,          -- Message
          0x00, 0x04, 117, 115, 101, 114,                       -- user
          0x00, 0x06, 112, 97, 115, 115, 119, 100               -- passwd
) )

equals( cli._mk_publish(0x0, nil, 'a/b', 'FooBar'), string.char(
    0x30,
       6 + 6,
          0x00, 0x03, 97, 47, 98,       -- a/b
          0,
          70, 111, 111, 66, 97, 114     -- FooBar
) )

equals( cli._mk_publish(0x0, nil, 'a/b', 'FooBar', { 1, 0 }), string.char(
    0x30,
       8 + 6,
          0x00, 0x03, 97, 47, 98,       -- a/b
          2,
              1,
              0x00,
          70, 111, 111, 66, 97, 114     -- FooBar
) )

equals( cli._mk_publish(0x0, nil, 'a/b', 'FooBar', { 2, 0x00102030 }), string.char(
    0x30,
       11 + 6,
          0x00, 0x03, 97, 47, 98,       -- a/b
          5,
              2,
              0x00, 0x10, 0x20, 0x30,
          70, 111, 111, 66, 97, 114     -- FooBar
) )

equals( cli._mk_publish(0x0, nil, 'a/b', 'FooBar', { 3, 'Foo' }), string.char(
    0x30,
       12 + 6,
          0x00, 0x03, 97, 47, 98,       -- a/b
          6,
              3,
              0x00, 0x03, 70, 111, 111,
          70, 111, 111, 66, 97, 114     -- FooBar
) )

equals( cli._mk_publish(0x0, nil, 'a/b', 'FooBar', { 8, 'Foo' }), string.char(
    0x30,
       12 + 6,
          0x00, 0x03, 97, 47, 98,       -- a/b
          6,
              8,
              0x00, 0x03, 70, 111, 111,
          70, 111, 111, 66, 97, 114     -- FooBar
) )

equals( cli._mk_publish(0x0, nil, 'a/b', 'FooBar', { 9, 'Foo' }), string.char(
    0x30,
       12 + 6,
          0x00, 0x03, 97, 47, 98,       -- a/b
          6,
              9,
              0x00, 0x03, 70, 111, 111,
          70, 111, 111, 66, 97, 114     -- FooBar
) )

equals( cli._mk_publish(0x0, nil, 'a/b', 'FooBar', { 11, 0 }), string.char(
    0x30,
       8 + 6,
          0x00, 0x03, 97, 47, 98,       -- a/b
          2,
              11,
              0x00,
          70, 111, 111, 66, 97, 114     -- FooBar
) )

equals( cli._mk_publish(0x0, nil, 'a/b', 'FooBar', { 11, 0x100 }), string.char(
    0x30,
       9 + 6,
          0x00, 0x03, 97, 47, 98,       -- a/b
          3,
              11,
              0x80, 0x02,
          70, 111, 111, 66, 97, 114     -- FooBar
) )

equals( cli._mk_publish(0x0, nil, 'a/b', 'FooBar', { 11, 0x10000 }), string.char(
    0x30,
       10 + 6,
          0x00, 0x03, 97, 47, 98,       -- a/b
          4,
              11,
              0x80, 0x80, 0x04,
          70, 111, 111, 66, 97, 114     -- FooBar
) )

equals( cli._mk_publish(0x0, nil, 'a/b', 'FooBar', { 35, 0x1234 }), string.char(
    0x30,
       9 + 6,
          0x00, 0x03, 97, 47, 98,       -- a/b
          3,
              35,
              0x12, 0x34,
          70, 111, 111, 66, 97, 114     -- FooBar
) )

equals( cli._mk_publish(0x0, nil, 'a/b', 'FooBar', { 38, { foo = 'bar' } }), string.char(
    0x30,
       17 + 6,
          0x00, 0x03, 97, 47, 98,       -- a/b
          11,
              38,
              0x00, 0x03, 102, 111, 111, 0x00, 0x03, 98, 97, 114,
          70, 111, 111, 66, 97, 114     -- FooBar
) )

local payload = string.rep('A', 0x100)
equals( cli._mk_publish(0x0, nil, 'a/b', payload ), string.char(
    0x30,
       0x80 + 6, 2,
          0x00, 0x03, 97, 47, 98,       -- a/b
          0
) .. payload )

payload = string.rep('A', 0x10000)
equals( cli._mk_publish(0x0, nil, 'a/b', payload ), string.char(
    0x30,
       0x80 + 6, 0x80 + 0, 4,
          0x00, 0x03, 97, 47, 98,       -- a/b
          0
) .. payload )

equals( cli._mk_publish(0x2, 0x1234, 'a/b', 'FooBar'), string.char(
    0x32,
       8 + 6,
          0x00, 0x03, 97, 47, 98,       -- a/b
          0x12, 0x34,
          0,
          70, 111, 111, 66, 97, 114     -- FooBar
) )

equals( cli._mk_publish(0x2, 0x1234, 'a/b', 'FooBar', { 38, { foo = 'bar' } }), string.char(
    0x32,
       19 + 6,
          0x00, 0x03, 97, 47, 98,       -- a/b
          0x12, 0x34,
          11,
              38,
              0x00, 0x03, 102, 111, 111, 0x00, 0x03, 98, 97, 114,
          70, 111, 111, 66, 97, 114     -- FooBar
) )

equals( cli._mk_puback(0x1234, 0x00), string.char(
    0x40,
       2,
          0x12, 0x34
) )

equals( cli._mk_puback(0x1234, 0x83), string.char(
    0x40,
       4,
          0x12, 0x34,
          0x83,
             0
) )

equals( cli._mk_puback(0x1234, 0x83, { 31, "Foo error" }), string.char(
    0x40,
      16,
          0x12, 0x34,
          0x83,
            12,
                31,
                0x00, 0x09, 70, 111, 111, 32, 101, 114, 114, 111, 114
) )

equals( cli._mk_pubrec(0x1234, 0x00), string.char(
    0x50,
       2,
          0x12, 0x34
) )

equals( cli._mk_pubrec(0x1234, 0x83), string.char(
    0x50,
       4,
          0x12, 0x34,
          0x83,
             0
) )

equals( cli._mk_pubrec(0x1234, 0x83, { 31, "Foo error" }), string.char(
    0x50,
      16,
          0x12, 0x34,
          0x83,
            12,
                31,
                0x00, 0x09, 70, 111, 111, 32, 101, 114, 114, 111, 114
) )

equals( cli._mk_pubrel(0x1234, 0x00), string.char(
    0x62,
       2,
          0x12, 0x34
) )

equals( cli._mk_pubrel(0x1234, 0x83), string.char(
    0x62,
       4,
          0x12, 0x34,
          0x83,
             0
) )

equals( cli._mk_pubrel(0x1234, 0x83, { 31, "Foo error" }), string.char(
    0x62,
      16,
          0x12, 0x34,
          0x83,
            12,
                31,
                0x00, 0x09, 70, 111, 111, 32, 101, 114, 114, 111, 114
) )

equals( cli._mk_pubcomp(0x1234, 0x00), string.char(
    0x70,
       2,
          0x12, 0x34
) )

equals( cli._mk_pubcomp(0x1234, 0x83), string.char(
    0x70,
       4,
          0x12, 0x34,
          0x83,
             0
) )

equals( cli._mk_pubcomp(0x1234, 0x83, { 31, "Foo error" }), string.char(
    0x70,
      16,
          0x12, 0x34,
          0x83,
            12,
                31,
                0x00, 0x09, 70, 111, 111, 32, 101, 114, 114, 111, 114
) )

equals( cli._mk_subscribe(0x1234, { 'a/b', 0x1, 'c/d', 0x2 }), string.char(
    0x82,
       3 + 12,
          0x12, 0x34,
          0,
          0x00, 0x03, 97, 47, 98,       -- a/b
          0x1,
          0x00, 0x03, 99, 47, 100,      -- c/d
          0x2
) )

equals( cli._mk_subscribe(0x1234, { 'a/b', 0x1, 'c/d', 0x2 }, { 11, 0 }), string.char(
    0x82,
       5 + 12,
          0x12, 0x34,
            2,
                11,
                0x00,
          0x00, 0x03, 97, 47, 98,       -- a/b
          0x1,
          0x00, 0x03, 99, 47, 100,      -- c/d
          0x2
) )

equals( cli._mk_subscribe(0x1234, { 'a/b', 0x1, 'c/d', 0x2 }, { 11, 0x100 }), string.char(
    0x82,
       6 + 12,
          0x12, 0x34,
            3,
                11,
                0x80, 0x02,
          0x00, 0x03, 97, 47, 98,       -- a/b
          0x1,
          0x00, 0x03, 99, 47, 100,      -- c/d
          0x2
) )

equals( cli._mk_subscribe(0x1234, { 'a/b', 0x1, 'c/d', 0x2 }, { 11, 0x10000 }), string.char(
    0x82,
       7 + 12,
          0x12, 0x34,
            4,
                11,
                0x80, 0x80, 0x04,
          0x00, 0x03, 97, 47, 98,       -- a/b
          0x1,
          0x00, 0x03, 99, 47, 100,      -- c/d
          0x2
) )

equals( cli._mk_subscribe(0x1234, { 'a/b', 0x1, 'c/d', 0x2 }, { 38, { foo = 'bar' } }), string.char(
    0x82,
       14 + 12,
          0x12, 0x34,
            11,
                38,
                0x00, 0x03, 102, 111, 111, 0x00, 0x03, 98, 97, 114,
          0x00, 0x03, 97, 47, 98,       -- a/b
          0x1,
          0x00, 0x03, 99, 47, 100,      -- c/d
          0x2
) )

equals( cli._mk_unsubscribe(0x1234, { 'a/b', 'c/d' }), string.char(
    0xA2,
       3 + 10,
          0x12, 0x34,
          0,
          0x00, 0x03, 97, 47, 98,       -- a/b
          0x00, 0x03, 99, 47, 100       -- c/d
) )

equals( cli._mk_unsubscribe(0x1234, { 'a/b', 'c/d' }, { 38, { foo = 'bar' } }), string.char(
    0xA2,
       14 + 10,
          0x12, 0x34,
            11,
                38,
                0x00, 0x03, 102, 111, 111, 0x00, 0x03, 98, 97, 114,
          0x00, 0x03, 97, 47, 98,       -- a/b
          0x00, 0x03, 99, 47, 100       -- c/d
) )

equals( cli._mk_pingreq(), string.char(
    0xC0,
       0
) )

equals( cli._mk_disconnect(0x00), string.char(
    0xE0,
       0
) )

equals( cli._mk_disconnect(0x83), string.char(
    0xE0,
       2,
          0x83,
          0
) )

equals( cli._mk_disconnect(0x83, { 17, 0x00102030 }), string.char(
    0xE0,
      7,
          0x83,
            5,
                17,
                0x00, 0x10, 0x20, 0x30
) )

equals( cli._mk_disconnect(0x83, { 28, "Foo" }), string.char(
    0xE0,
      8,
          0x83,
            6,
                28,
                0x00, 0x03, 70, 111, 111
) )

equals( cli._mk_disconnect(0x83, { 31, "Foo error" }), string.char(
    0xE0,
      14,
          0x83,
            12,
                31,
                0x00, 0x09, 70, 111, 111, 32, 101, 114, 114, 111, 114
) )

equals( cli._mk_disconnect(0x83, { 38, { foo = 'bar' } }), string.char(
    0xE0,
      13,
          0x83,
            11,
                38,
                0x00, 0x03, 102, 111, 111, 0x00, 0x03, 98, 97, 114
) )

equals( cli._mk_auth(0x00), string.char(
    0xF0,
       0
) )

equals( cli._mk_auth(0x19), string.char(
    0xF0,
       2,
          0x19,
          0
) )

equals( cli._mk_auth(0x19, { 21, "Foo" }), string.char(
    0xF0,
      8,
          0x19,
            6,
                21,
                0x00, 0x03, 70, 111, 111
) )

equals( cli._mk_auth(0x19, { 22, "Foo" }), string.char(
    0xF0,
      8,
          0x19,
            6,
                22,
                0x00, 0x03, 70, 111, 111
) )

equals( cli._mk_auth(0x19, { 31, "Foo error" }), string.char(
    0xF0,
      14,
          0x19,
            12,
                31,
                0x00, 0x09, 70, 111, 111, 32, 101, 114, 114, 111, 114
) )

equals( cli._mk_auth(0x19, { 38, { foo = 'bar' } }), string.char(
    0xF0,
      13,
          0x19,
            11,
                38,
                0x00, 0x03, 102, 111, 111, 0x00, 0x03, 98, 97, 114
) )
