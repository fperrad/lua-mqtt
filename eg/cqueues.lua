#!/usr/bin/env lua

local mqtt = require'mqtt5'
local cqueues = require'cqueues'        -- https://25thandclement.com/~william/projects/cqueues.html
local socket = require'cqueues.socket'

local function new_socket ()
    local sock = socket.connect({
        host      = 'test.mosquitto.org',
        port      = mqtt.PORT_TLS,
        family    = socket.AF_INET,     -- IPv4
        reuseaddr = true,
    })
    sock:setmode('b-', 'bn')            -- binary mode, no output buffering
    sock:connect(5)
    sock:starttls()
    return sock
end

local client
client = mqtt({
    socket     = assert(new_socket()),
    logger     = require'logging.console'({}),
    on_connect = function (rc)
        if rc == 0 then
            client:subscribe({
                '$SYS/broker/version', 0,
                '$SYS/broker/publish/#', 0,
            })
        end
    end,
    on_message = function (topic, payload)
        print(topic, payload)
    end,
    on_error   = function ()
        for _ = 1, 10 do
            cqueues.sleep(2)
            local _, sock = pcall(new_socket)
            if socket.type(sock) == 'socket' then
                client:reconnect(sock)
                return true
            end
        end
        return false
    end,
})

client:connect({
    id       = 'LuaMQTT',
    username = 'guest',
    password = 'guest',
}, {
    mqtt.PROP.SESSION_EXPIRY_INTERVAL, 0xFFFFFFFF,
    mqtt.PROP.MAXIMUM_PACKET_SIZE, 0x3F,
})
local connack = client:read()
assert(connack.rc == 0)

local loop = cqueues.new()

loop:wrap(function()
    while client:read() do
    end
end)

loop:wrap(function()
    while true do
        cqueues.sleep(15)
        client:ping()
    end
end)

assert(loop:loop())

