#!/usr/bin/env lua

local mqtt = require'mqtt311'
local websocket = require'http.websocket'       -- https://daurnimator.github.io/lua-http/

local conn = assert(websocket.new_from_uri('http://test.mosquitto.org:8080/', { 'mqtt' }))
conn:connect(5)

local socket
socket = {
    data  = '',
    pos   = 1,
    close = function ()
        conn:close()
    end,
    read  = function (_, n)
        if socket.pos > #socket.data then
            socket.data = conn:receive(5)
            socket.pos = 1
        end
        local s = string.sub(socket.data, socket.pos, socket.pos + n -1)
        socket.pos = socket.pos + n
        return s ~= '' and s or nil
    end,
    write = function (_, data)
        conn:send(data, 'binary')
    end,
}

local client = mqtt({
    socket = socket,
    logger = require'logging.console'({}),
})

client:connect({ clean = true })
local connack = client:read()
if connack and connack.rc == 0 then
    client:publish('foo/bar', 'Hello world!', { qos = 1 })
    local puback = client:read()
    client:disconnect()
end
conn:close()

