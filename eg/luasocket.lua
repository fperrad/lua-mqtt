#!/usr/bin/env lua

local mqtt = require'mqtt311'
local socket = require'socket'  -- https://lunarmodules.github.io/luasocket/

local conn = assert(socket.connect('test.mosquitto.org', mqtt.PORT))

local client = mqtt({
    socket = {
        close = function ()
            conn:shutdown()
        end,
        read  = function (_, n)
            return conn:receive(n)
        end,
        write = function (_, data)
            conn:send(data)
        end,
    },
    logger = require'logging.console'({}),
})

client:connect({ clean = true, id = '' })
local connack = client:read()
if connack and connack.rc == 0 then
    client:publish('foo/bar', 'Hello world!', { qos = 1 })
    local puback = client:read()
    client:disconnect()
end
conn:shutdown()

