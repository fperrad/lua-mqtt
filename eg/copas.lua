#!/usr/bin/env lua

local mqtt = require'mqtt311'
local socket = require'socket'  -- https://lunarmodules.github.io/luasocket/
local copas = require'copas'    -- https://lunarmodules.github.io/copas/

local conn = assert(socket.connect('test.mosquitto.org', mqtt.PORT))
conn:settimeout(0)              -- non-blocking

local client
client = mqtt({
    socket     = {
        close = function ()
            conn:shutdown()
        end,
        read  = function (_, n)
            return copas.receive(conn, n)
        end,
        write = function (_, data)
            copas.send(conn, data)
        end,
    },
    logger     = require'logging.console'({}),
    on_connect = function (rc)
        if rc == 0 then
            client:subscribe({
                '$SYS/broker/version', 0,
                '$SYS/broker/publish/#', 0,
            })
        end
    end,
    on_message = function (topic, payload)
        print(topic, payload)
    end,
})

copas.addthread(function()
    client:connect({ clean = true })
    local connack = client:read()
    assert(connack.rc == 0)
    while client:read() do
    end
end)

copas.addthread(function()
    while true do
        copas.sleep(15)
        client:ping()
    end
end)

copas.loop()

