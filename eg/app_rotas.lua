-- file: app_rotas.lua

local rotas = require'Rotas'            -- https://fperrad.frama.io/lua-Rotas/

rotas.http_methods = { 'MQTT' }         -- replace all HTTP verbs

local app = rotas()

local function filter (patt)            -- a matcher for Rotas
    local match = require'mqtt5'.match
    return function (s)
        return match(s, patt)
    end
end

app.MQTT[filter'$SYS/broker/publish/#'] = function (topic, payload)
    print(topic, payload)
end

app.MQTT[filter'$SYS/broker/version'] = function (_, payload)
    print('VERSION', payload)
end

return app

