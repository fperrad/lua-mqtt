#!/usr/bin/env lua

local mqtt = require'mqtt311'
local socket = require'socket'  -- https://lunarmodules.github.io/luasocket/
local ssl = require'ssl'        -- https://github.com/brunoos/luasec/

local conn = assert(socket.connect('test.mosquitto.org', mqtt.PORT_TLS))
conn = ssl.wrap(conn, {
    mode     = 'client',
    protocol = 'any',
    options  = { 'all' },
})
conn:dohandshake()

local client = mqtt({
    socket = {
        close = function ()
            conn:close()
        end,
        read  = function (_, n)
            return conn:receive(n)
        end,
        write = function (_, data)
            conn:send(data)
        end,
    },
    logger = require'logging.console'({}),
})

client:connect({ clean = true })
local connack = client:read()
if connack and connack.rc == 0 then
    client:publish('foo/bar', 'Hello world!', { qos = 1 })
    local puback = client:read()
    client:disconnect()
end
conn:close()

