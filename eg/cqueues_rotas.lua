#!/usr/bin/env lua

local mqtt = require'mqtt5'
local cqueues = require'cqueues'        -- https://25thandclement.com/~william/projects/cqueues.html
local socket = require'cqueues.socket'

local logger = require'logging.console'({})

local conn = assert(socket.connect({
        host      = 'test.mosquitto.org',
        port      = mqtt.PORT_TLS,
        family    = socket.AF_INET,     -- IPv4
        reuseaddr = true,
}))
conn:setmode('b-', 'bn')                -- binary mode, no output buffering
conn:connect(5)
conn:starttls()

local client
client = mqtt({
    socket     = conn,
    logger     = logger,
    on_connect = function (rc)
        if rc == 0 then
            client:subscribe({
                '$SYS/broker/version', 0,
                '$SYS/broker/publish/#', 0,
            })
        end
    end,
    on_message = function (topic, payload, props)
        local app = require'app_rotas'
        local fn = app('MQTT', topic)
        if not fn then
            logger:warn(topic .. ' --> no MQTT handler')
        else
            local status, msg = pcall(fn, topic, payload, props)
            if not status then
                logger:error(topic .. ' --> throw error: ' .. msg)
            end
        end
    end,
})

client:connect({ clean = true })
local connack = client:read()
assert(connack.rc == 0)

local loop = cqueues.new()

loop:wrap(function()
    while client:read() do
    end
end)

assert(loop:loop())

