codes = true
max_line_length = 160
read_globals = {
    -- Test.More
    'plan',
    'done_testing',
    'skip_all',
    'BAIL_OUT',
    'subtest',
    'diag',
    'note',
    'skip',
    'todo_skip',
    'skip_rest',
    'todo',
    -- Test.Assertion
    'equals',
    'is_false',
    'is_function',
    'is_nil',
    'is_string',
    'is_table',
    'is_true',
    'same',
    'matches',
    'require_ok',
    'error_matches',
    'fails',
    'passes',
    'truthy',
}
