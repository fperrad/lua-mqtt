
lua-mqtt
========

Introduction
------------

**lua-mqtt** is a client library for [MQTT](https://mqtt.org/) 3.1.1 & 5.

It's developed for Lua 5.3 & 5.4 (and transpilable by [Teal](https://github.com/teal-language/tl)
to Lua 5.1 & 5.2).

lua-mqtt is in beta stage.

Links
-----

The homepage is at <https://fperrad.frama.io/lua-mqtt>,
and the sources are hosted at <https://framagit.org/fperrad/lua-mqtt>.

Copyright and License
---------------------

Copyright (c) 2022-2024 Francois Perrad

This library is licensed under the terms of the MIT/X11 license, like Lua itself.

