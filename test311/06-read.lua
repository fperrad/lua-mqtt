#!/usr/bin/env lua

require 'Test.Assertion'

plan(69)

local mqtt = require 'mqtt311'

local buffer
local pos
local function set_buffer (s)
    buffer = s
    pos = 1
end
local sock = {
    close = function ()
        diag('socket:close')
    end,
    read  = function (_, n)
        local s = string.sub(buffer, pos, pos + n -1)
        pos = pos + n
        if s == '' then
            return nil, 'EMPTY'
        else
            return s
        end
    end,
    write = function ()
        diag('socket:write')
        return true
    end,
}

local logger = {
    debug = function (_, ...) diag(...) end,
    error = function (_, ...) diag(...) end,
    fatal = function (_, ...) diag(...) end,
    info  = function (_, ...) diag(...) end,
    warn  = function (_, ...) diag(...) end,
}

local cli = mqtt({
    socket = sock,
    logger = logger,
})
is_table( cli )

set_buffer('')
local ret, msg = cli:read()
is_nil( ret )
equals( msg, "EMPTY", "no input" )
cli.socket = sock

set_buffer(string.char(
    0x60
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "invalid flags: 60", "invalid flag" )
cli.socket = sock

set_buffer(string.char(
    0x62
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "EMPTY", "missing data" )
cli.socket = sock

set_buffer(string.char(
    0xD0,
    0x80, 0x80, 0x80, 0x80, 0x80
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "invalid vbi: too long", "invalid vbi" )
cli.socket = sock

set_buffer(string.char(
    0xD0,
    1
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "invalid length 1 for: D0", "invalid length" )
cli.socket = sock

set_buffer(string.char(
    0x30,
    1
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "invalid length 1 for: 30", "invalid length" )
cli.socket = sock

set_buffer(string.char(
    0x20,
    2,
        0x01
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "missing data", "missing data" )
cli.socket = sock

set_buffer(string.char(
    0x30,
    0x80 + 65, 2,
        0x01
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "missing data", "missing data" )
cli.socket = sock

set_buffer(string.char(
    0x20,
    2,
        0x00,
        0x00
))
same( cli:read(), {
    type = 'CONNACK',
    sp   = false,
    rc   = 0x00,
}, "connack")

set_buffer(string.char(
    0x30,
    11,
        0x00, 0x03, 97, 47, 98,
        70, 111, 111, 66, 97, 114
))
same( cli:read(), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 0,
    topic   = 'a/b',
    payload = 'FooBar',
}, "message 0" )

set_buffer(string.char(
    0x3B,
    13,
        0x00, 0x03, 97, 47, 98,
        0x12, 0x34,
        70, 111, 111, 66, 97, 114
))
same( cli:read(), {
    type    = 'PUBLISH',
    dup     = true,
    retain  = true,
    qos     = 1,
    topic   = 'a/b',
    id      = 0x1234,
    payload = 'FooBar',
}, "message 1" )

set_buffer(string.char(
    0x34,
    13,
        0x00, 0x03, 97, 47, 98,
        0x12, 0x34,
        70, 111, 111, 66, 97, 114
))
same( cli:read(), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 2,
    topic   = 'a/b',
    id      = 0x1234,
    payload = 'FooBar',
}, "message 2" )

set_buffer(string.char(
    0x40,
    2,
        0x12, 0x34
))
cli.session.publish[0x1234] = {}
same( cli:read(), {
    type = 'PUBACK',
    id   = 0x1234,
}, "puback")

set_buffer(string.char(
    0x40,
    2,
        0x12, 0x34
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "mismatch for PUBLISH/PUBACK", "mismatch publish/puback" )
cli.socket = sock

set_buffer(string.char(
    0x50,
    2,
        0x12, 0x34
))
cli.session.publish[0x1234] = {}
same( cli:read(), {
    type = 'PUBREC',
    id   = 0x1234,
}, "pubrec")

set_buffer(string.char(
    0x50,
    2,
        0x12, 0x34
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "mismatch for PUBLISH/PUBREC", "mismatch publish/pubrec" )
cli.socket = sock

set_buffer(string.char(
    0x62,
    2,
        0x12, 0x34
))
same( cli:read(), {
    type = 'PUBREL',
    id   = 0x1234,
}, "pubrel")

set_buffer(string.char(
    0x62,
    2,
        0x12, 0x34
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "mismatch for PUBREC/PUBREL", "mismatch pubrec/pubrel" )
cli.socket = sock

set_buffer(string.char(
    0x70,
    2,
        0x12, 0x34
))
same( cli:read(), {
    type = 'PUBCOMP',
    id   = 0x1234,
}, "pubcomp")

set_buffer(string.char(
    0x70,
    2,
        0x12, 0x34
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "mismatch for PUBREL/PUBCOMP", "mismatch pubrel/pubcomp" )
cli.socket = sock

set_buffer(string.char(
    0x90,
    4,
        0x12, 0x34,
        0x00, 0x00
))
cli.session.subscribe[0x1234] = { 'a/b', 1, 'c/d', 2 }
same( cli:read(), {
    type    = 'SUBACK',
    id      = 0x1234,
    payload = { 0x00, 0x00 },
}, "suback")

set_buffer(string.char(
    0x90,
    4,
        0x12, 0x34,
        0x00, 0x00
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "mismatch for SUBSCRIBE/SUBACK", "mismatch subscribe/suback" )
cli.socket = sock

set_buffer(string.char(
    0xB0,
    2,
        0x12, 0x34
))
cli.session.unsubscribe[0x1234] = { 'a/b', 'c/d' }
same( cli:read(), {
    type = 'UNSUBACK',
    id   = 0x1234,
}, "unsuback")

set_buffer(string.char(
    0xB0,
    2,
        0x12, 0x34
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "mismatch for UNSUBSCRIBE/UNSUBACK", "mismatch unsubscribe/unsuback" )
cli.socket = sock

cli.pingreq_sent = true
set_buffer(string.char(
    0xD0,
    0
))
same( cli:read(), {
    type = 'PINGRESP',
}, "pingresp")

set_buffer(string.char(
    0xD0,
    0
))

set_buffer(string.char(
    0x04,
    2,
        0,
        0
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "invalid packet control: 04", "unknown" )
cli.socket = sock

set_buffer(string.char(
    0x04,
    99
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "EMPTY", "unknown" )
cli.socket = sock


cli = mqtt({
    socket          = sock,
    logger          = logger,
    max_packet_size = 255,
})
set_buffer(string.char(0x20, 2, 0x00, 0x00))
truthy( cli:read(), "connack")

local payload = string.rep('x', 255 - 5)
set_buffer(string.char(
    0x30,
    0xFF, 0x01,
        0x00, 0x03, 97, 47, 98
) .. payload)
same( cli:read(), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 0,
    topic   = 'a/b',
    payload = payload,
}, "message not too large" )

payload = string.rep('x', 256 - 5)
set_buffer(string.char(
    0x30,
    0x80, 0x02,
        0x00, 0x03, 97, 47, 98
) .. payload)
ret, msg = cli:read()
is_nil( ret )
equals( msg, "packet too large: 256", "packet too large" )


cli = mqtt({
    socket          = sock,
    logger          = logger,
})

set_buffer(string.char(
    0x30,
    11,
        0x00, 0x03, 97, 47, 98,
        70, 111, 111, 66, 97, 114
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "CONNACK not received", "publish before connack" )
cli.socket = sock

set_buffer(string.char(
    0x40,
    2,
        0x12, 0x34
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "CONNACK not received", "puback before connack" )
cli.socket = sock

set_buffer(string.char(
    0x50,
    2,
        0x12, 0x34
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "CONNACK not received", "pubrec before connack" )
cli.socket = sock

set_buffer(string.char(
    0x62,
    2,
        0x12, 0x34
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "CONNACK not received", "pubrel before connack" )
cli.socket = sock

set_buffer(string.char(
    0x70,
    2,
        0x12, 0x34
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "CONNACK not received", "pubcomp before connack" )
cli.socket = sock

set_buffer(string.char(
    0x90,
    4,
        0x12, 0x34,
        0x00, 0x00
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "CONNACK not received", "suback before connack" )
cli.socket = sock

set_buffer(string.char(
    0xB0,
    2,
        0x12, 0x34
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "CONNACK not received", "unsuback before connack" )
cli.socket = sock

cli.pingreq_sent = true
set_buffer(string.char(
    0xD0,
    0
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "CONNACK not received", "pingresp before connack" )
cli.socket = sock

set_buffer(string.char(
    0x20,
    2,
        0x01,
        0x00
))
truthy( cli:read(), "connack")

set_buffer(string.char(
    0x20,
    2,
        0x01,
        0x00
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "CONNACK already received", "connack after connack" )
cli.socket = sock


cli:connect({ clean = true })
set_buffer(string.char(
    0x20,
    2,
        0x01,
        0x00
))
ret, msg = cli:read()
is_nil( ret )
equals( msg, "invalid CONNACK (sp but clean)", "connack with sp but clean requested" )
cli.socket = sock

