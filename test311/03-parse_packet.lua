#!/usr/bin/env lua

require 'Test.Assertion'

plan(26)

local mqtt = require 'mqtt311'

local sock = {
    close = function () diag('socket:close') end,
    read  = true,
    write = true,
}
local cli = mqtt({
    socket = sock,
    logger = {
        debug = function (_, ...) diag(...) end,
        error = function (_, ...) diag(...) end,
        fatal = function (_, ...) diag(...) end,
        info  = function (_, ...) diag(...) end,
        warn  = function (_, ...) diag(...) end,
    },
})
is_table( cli )

same( cli:_parse_connack(string.char(
    0x01,
    0x00
)), {
    type = 'CONNACK',
    sp   = true,
    rc   = 0x00,
})

local ret, msg = cli:_parse_connack(string.char(
    0x02,
    0x00
))
is_nil( ret )
equals( msg, "invalid flags in CONNACK: 02", "bad flags" )
cli.socket = sock

ret, msg = cli:_parse_connack(string.char(
    0x00,
    66
))
is_nil( ret )
equals( msg, "invalid code in CONNACK: 66", "bad code")
cli.socket = sock

ret, msg = cli:_parse_connack(string.char(
    0x01,
    0x05
))
is_nil( ret )
equals( msg, "invalid CONNACK (sp with rc)", "sp with rc")
cli.socket = sock

same( cli:_parse_publish(string.char(
    0x00, 0x03, 97, 47, 98,
    70, 111, 111, 66, 97, 114
), 0x0), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 0,
    topic   = 'a/b',
    payload = 'FooBar',
})

same( cli:_parse_publish(string.char(
    0x00, 0x03, 97, 47, 98
), 0x0), {
    type    = 'PUBLISH',
    dup     = false,
    retain  = false,
    qos     = 0,
    topic   = 'a/b',
    payload = '',
})

same( cli:_parse_publish(string.char(
    0x00, 0x03, 97, 47, 98,
    0x12, 0x34,
    70, 111, 111, 66, 97, 114
), 0xB), {
    type    = 'PUBLISH',
    dup     = true,
    retain  = true,
    qos     = 1,
    topic   = 'a/b',
    id      = 0x1234,
    payload = 'FooBar',
})

ret, msg = cli:_parse_publish(string.char(
    0x00, 0x03, 97, 47, 98,
    0x12, 0x34,
    70, 111, 111, 66, 97, 114
), 0xF)
is_nil( ret )
equals( msg, "invalid QoS for PUBLISH", "invalid QoS")
cli.socket = sock

ret, msg = cli:_parse_publish(string.char(
    0x00, 0x33, 97, 47, 98,
    0x12, 0x34,
    70, 111, 111, 66, 97, 114
), 0xB)
is_nil( ret )
if _VERSION == 'Lua 5.1' then
    todo('compat53 error', 1)
end
equals( msg, "invalid message for PUBLISH: bad argument #2 to 'string.unpack' (data string too short)", "data too short")
cli.socket = sock

ret, msg = cli:_parse_publish(string.char(
    0x00, 0x03, 97, 47, 98,
    0x12
), 0xB)
is_nil( ret )
if _VERSION == 'Lua 5.1' then
    todo('compat53 error', 1)
end
equals( msg, "invalid message for PUBLISH: bad argument #2 to 'string.unpack' (data string too short)", "data too short")
cli.socket = sock

same( cli:_parse_puback(string.char(
    0x12, 0x34
)), {
    type = 'PUBACK',
    id   = 0x1234,
})

same( cli:_parse_pubrec(string.char(
    0x12, 0x34
)), {
    type = 'PUBREC',
    id   = 0x1234,
})

same( cli:_parse_pubrel(string.char(
    0x12, 0x34
)), {
    type = 'PUBREL',
    id   = 0x1234,
})

same( cli:_parse_pubcomp(string.char(
    0x12, 0x34
)), {
    type = 'PUBCOMP',
    id   = 0x1234,
})

same( cli:_parse_suback(string.char(
    0x12, 0x34,
    0x00, 0x00
)), {
    type    = 'SUBACK',
    id      = 0x1234,
    payload = { 0x00, 0x00 },
})

ret, msg = cli:_parse_suback(string.char(
    0x12, 0x34,
    0x00, 0x05
))
is_nil( ret )
equals( msg, "invalid code in SUBACK: 5", "bad code")
cli.socket = sock

same( cli:_parse_unsuback(string.char(
    0x12, 0x34
)), {
    type = 'UNSUBACK',
    id   = 0x1234,
})

same( cli:_parse_pingresp(), {
    type = 'PINGRESP',
})

