#!/usr/bin/env lua

require 'Test.Assertion'

plan(28)

local mqtt = require 'mqtt311'

is_true( mqtt.match('sport/tennis/player1', 'sport/tennis/player1/#') )
is_true( mqtt.match('sport/tennis/player1/ranking', 'sport/tennis/player1/#') )
is_true( mqtt.match('sport/tennis/player1/score/wimbledon', 'sport/tennis/player1/#') )

is_true( mqtt.match('sport', 'sport/#') )
is_true( mqtt.match('sport/tennis/player1', '#') )
is_true( mqtt.match('sport/tennis/player1', 'sport/tennis/#') )

error_matches( function () mqtt.match('sport/tennis/player1', 'sport/tennis#') end,
               "^[^:]+:%d+: invalid Topic Filter to 'match'" )

error_matches( function () mqtt.match('sport/tennis/player1/ranking', 'sport/tennis/#/ranking') end,
               "^[^:]+:%d+: invalid Topic Filter to 'match'" )

is_true( mqtt.match('sport/tennis/player1', 'sport/tennis/+') )
is_true( mqtt.match('sport/tennis/player2', 'sport/tennis/+') )
is_false( mqtt.match('sport/tennis/player1/ranking', 'sport/tennis/+') )
is_false( mqtt.match('sport', 'sport/+') )
is_true( mqtt.match('sport/', 'sport/+') )
is_true( mqtt.match('sport/tennis/player1/ranking', 'sport/+/#') )

is_true( mqtt.match('sport', '+') )
is_true( mqtt.match('sport/tennis/player1/ranking', '+/tennis/#') )

error_matches( function () mqtt.match('sport/tennis/player1', 'sport+') end,
               "^[^:]+:%d+: invalid Topic Filter to 'match'" )

error_matches( function () mqtt.match('sport/tennis/player1', '+sport') end,
               "^[^:]+:%d+: invalid Topic Filter to 'match'" )

is_true( mqtt.match('/finance', '+/+') )
is_true( mqtt.match('/finance', '/+') )
is_false( mqtt.match('/finance', '+') )

is_false( mqtt.match('$SYS', '#') )
is_false( mqtt.match('$SYS/monitor/Clients', '+/monitor/Clients') )
is_true( mqtt.match('$SYS/monitor/Clients', '$SYS/#') )
is_true( mqtt.match('$SYS/monitor/Clients', '$SYS/monitor/+') )

error_matches( function () mqtt.match('foo\0bar', '#') end,
               "^[^:]+:%d+: invalid Topic Name to 'match'" )

error_matches( function () mqtt.match(42, '#') end,
               "^[^:]+:%d+: bad argument #1 to 'match' %(string expected, got number%)" )

error_matches( function () mqtt.match('foobar', 42) end,
               "^[^:]+:%d+: bad argument #2 to 'match' %(string expected, got number%)" )

