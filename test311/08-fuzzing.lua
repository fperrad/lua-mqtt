#! /usr/bin/lua

require 'Test.Assertion'

local mqtt = require 'mqtt311'
local unpack = table.unpack or unpack

local buffer
local pos
local function set_buffer (s)
    buffer = s
    pos = 1
end
local sock = {
    close = function () end,
    read  = function (_, n)
        local s = string.sub(buffer, pos, pos + n -1)
        pos = pos + n
        return s ~= '' and s or nil
    end,
    write = function () end,
}
local cli = mqtt({
    socket = sock,
--[[
    logger = {
        debug = function () end,
        error = function (_, ...) diag(...) end,
        fatal = function () end,
        info  = function () end,
        warn  = function () end,
    },
--]]
})

local nb = tonumber(os.getenv('FUZZ_NB')) or 1000
local len = tonumber(os.getenv('FUZZ_LEN')) or 128

if nb <= 0 then
    skip_all('coverage')
else
    plan 'no_plan'
end

for _ = 1, nb do
    local t = {}
    for i = 1, len do
        t[i] = math.random(0, 255)
    end
    local data = string.char(unpack(t))
    cli.socket = sock
    cli.connack_received = math.random() < 0.5
    cli.pingreq_sent = true
    set_buffer(data)
    local r, msg = pcall(cli.read, cli)         -- cli:read()
    if r == true then
        passes()
    else
        diag(table.concat(t, ' '))
        diag(msg)
        fails()
    end
end

done_testing()
