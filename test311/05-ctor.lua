#!/usr/bin/env lua

require 'Test.Assertion'

plan(9)

local mqtt = require 'mqtt311'

local sock = {
    close = true,
    read  = true,
    write = true,
}
local logger = {
    debug = true,
    error = true,
    fatal = true,
    info  = true,
    warn  = true,
}

local cli = mqtt({
    socket = sock,
})
is_table( cli )

cli = mqtt({
    socket     = sock,
    logger     = logger,
    on_message = function () end,
    on_error   = function () end,
})
is_table( cli )

if _VERSION == 'Lua 5.1' and not jit then
    todo('compat53 error', 1)
end
error_matches( function () mqtt(42) end,
               "^[^:]+:%d+: bad argument #1 to 'new' %(table expected, got number%)" )

error_matches( function () mqtt({
                   socket = 42,
               }) end,
               "^[^:]+:%d+: invalid socket %(42%) in table to 'new'" )

error_matches( function () mqtt({
                   socket = sock,
                   logger = 42,
               }) end,
               "^[^:]+:%d+: invalid logger %(42%) in table to 'new'" )

error_matches( function () mqtt({
                   socket     = sock,
                   on_connect = 42,
               }) end,
               "^[^:]+:%d+: invalid on_connect %(42%) in table to 'new'" )

error_matches( function () mqtt({
                   socket     = sock,
                   on_message = 42,
               }) end,
               "^[^:]+:%d+: invalid on_message %(42%) in table to 'new'" )

error_matches( function () mqtt({
                   socket   = sock,
                   on_error = 42,
               }) end,
               "^[^:]+:%d+: invalid on_error %(42%) in table to 'new'" )

error_matches( function () mqtt({
                   socket          = sock,
                   max_packet_size = -1,
               }) end,
               "^[^:]+:%d+: invalid max_packet_size %(%-1%) in table to 'new'" )
