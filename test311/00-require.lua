#!/usr/bin/env lua

require 'Test.Assertion'

plan(18)

if not require_ok 'mqtt311' then
    BAIL_OUT "no lib"
end

local m = require 'mqtt311'
is_table( m, 'module' )
equals( m, package.loaded['mqtt311'], 'package.loaded' )

is_function( m.match, 'function match' )

local sock = { close = true, read = true, write = true }
local o = m({ socket = sock })
is_table( o, 'instance' )
is_function( o.connect, 'meth connect' )
is_function( o.reconnect, 'meth reconnect' )
is_function( o.publish, 'meth publish' )
is_function( o.subscribe, 'meth subscribe' )
is_function( o.unsubscribe, 'meth unsubscribe' )
is_function( o.ping, 'meth ping' )
is_function( o.disconnect, 'meth disconnect' )
is_function( o.read, 'meth read' )

equals( m._NAME, 'mqtt311', "_NAME" )
matches( m._COPYRIGHT, 'Perrad', "_COPYRIGHT" )
matches( m._DESCRIPTION, 'client .- 3%.1%.1', "_DESCRIPTION" )
is_string( m._VERSION, "_VERSION" )
matches( m._VERSION, '^%d%.%d%.%d$' )
