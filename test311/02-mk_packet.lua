#!/usr/bin/env lua

require 'Test.Assertion'

plan(15)

local mqtt = require 'mqtt311'

local sock = { close = true, read = true, write = true }
local cli = mqtt({ socket = sock })
is_table( cli )

equals( cli._mk_connect(0x00, 0x1234, 'CLIENT'), string.char(
    0x10,
       10 + 8,
          0x00, 0x04, 77, 81, 84, 84,   -- MQTT
          4,
          0x00,                         -- connect flags
          0x12, 0x34,                   -- keep alive
          -- payload
          0x00, 0x06, 67, 76, 73, 69, 78, 84                    -- CLIENT
) )

equals( cli._mk_connect(0xC6, 0x1234, '', 'top/ic', 'Message', 'user', 'passwd'), string.char(
    0x10,
       10 + 33,
          0x00, 0x04, 77, 81, 84, 84,   -- MQTT
          4,
          0xC6,                         -- connect flags
          0x12, 0x34,                   -- keep alive
          -- payload
          0x00, 0x00,                                           -- ''
          0x00, 0x06, 116, 111, 112, 47, 105, 99,               -- top/ic
          0x00, 0x07, 77, 101, 115, 115, 97, 103, 101,          -- Message
          0x00, 0x04, 117, 115, 101, 114,                       -- user
          0x00, 0x06, 112, 97, 115, 115, 119, 100               -- passwd
) )

equals( cli._mk_publish(0x0, nil, 'a/b', 'FooBar'), string.char(
    0x30,
       5 + 6,
          0x00, 0x03, 97, 47, 98,       -- a/b
          70, 111, 111, 66, 97, 114     -- FooBar
) )

local payload = string.rep('A', 0x100)
equals( cli._mk_publish(0x0, nil, 'a/b', payload ), string.char(
    0x30,
       0x80 + 5, 2,
          0x00, 0x03, 97, 47, 98        -- a/b
) .. payload )

payload = string.rep('A', 0x10000)
equals( cli._mk_publish(0x0, nil, 'a/b', payload ), string.char(
    0x30,
       0x80 + 5, 0x80 + 0, 4,
          0x00, 0x03, 97, 47, 98        -- a/b
) .. payload )

equals( cli._mk_publish(0x2, 0x1234, 'a/b', 'FooBar'), string.char(
    0x32,
       7 + 6,
          0x00, 0x03, 97, 47, 98,       -- a/b
          0x12, 0x34,
          70, 111, 111, 66, 97, 114     -- FooBar
) )

equals( cli._mk_puback(0x1234), string.char(
    0x40,
       2,
          0x12, 0x34
) )

equals( cli._mk_pubrec(0x1234), string.char(
    0x50,
       2,
          0x12, 0x34
) )

equals( cli._mk_pubrel(0x1234), string.char(
    0x62,
       2,
          0x12, 0x34
) )

equals( cli._mk_pubcomp(0x1234), string.char(
    0x70,
       2,
          0x12, 0x34
) )

equals( cli._mk_subscribe(0x1234, { 'a/b', 0x1, 'c/d', 0x2 }), string.char(
    0x82,
       2 + 12,
          0x12, 0x34,
          0x00, 0x03, 97, 47, 98,       -- a/b
          0x1,
          0x00, 0x03, 99, 47, 100,      -- c/d
          0x2
) )

equals( cli._mk_unsubscribe(0x1234, { 'a/b', 'c/d' }), string.char(
    0xA2,
       2 + 10,
          0x12, 0x34,
          0x00, 0x03, 97, 47, 98,       -- a/b
          0x00, 0x03, 99, 47, 100       -- c/d
) )

equals( cli._mk_pingreq(), string.char(
    0xC0,
       0
) )

equals( cli._mk_disconnect(), string.char(
    0xE0,
       0
) )
