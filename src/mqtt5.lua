
--
-- lua-mqtt : <https://fperrad.frama.io/lua-mqtt/>
--

if _VERSION < 'Lua 5.3' then
    require'compat53'
    if _VERSION == 'Lua 5.1' then
        require'bit'
    end
end

local error = error
local next = next
local pcall = pcall
local setmetatable = setmetatable
local tostring = tostring
local type = type
local random = math.random
local math_type = math.type
local byte = string.byte
local char = string.char
local format = string.format
local len = string.len
local pack = string.pack
local sub = string.sub
local unpack = string.unpack
local concat = table.concat
local insert = table.insert
local remove = table.remove
local utf8_codes = utf8.codes
local utf8_len = utf8.len

if _VERSION <= 'Lua 5.3' then
    math.randomseed(os.time())
end

local _ENV = _ENV
if _VERSION >= 'Lua 5.3' then
    _ENV = nil
end

local mt = {}

local function argerror(caller, narg, extramsg, level)
    error("bad argument #" .. tostring(narg) .. " to '"
          .. caller .. "' (" .. extramsg .. ")", level + 2)
end

local function typeerror (caller, narg, arg, tname, level)
    level = level or 1
    local got = (arg == nil) and 'no value' or type(arg)
    argerror(caller, narg, tname .. " expected, got " .. got, level + 1)
end

local function valid_socket (o)
    local status, result = pcall(function ()
        return o and o.close and o.read and o.write
    end)
    return status and result
end

local function valid_logger (o)
    local status, result = pcall(function ()
        return o and o.error and o.debug
    end)
    return status and result
end

local function valid_0_1 (v)
    return v == 0 or v == 1
end

local function valid_2byte (v)
    return math_type(v) == 'integer' and v >= 0 and v <= 0xFFFF
end

local function valid_vbi (v)
    return math_type(v) == 'integer' and v >= 0 and v <= 0xFFFFFFF
end

local function valid_4byte (v)
    return math_type(v) == 'integer' and v >= 0 and v <= 0xFFFFFFFF
end

local function valid_string (v)
    return type(v) == 'string' and #v <= 0xFFFF and utf8_len(v)
end

local function valid_binary (v)
    return type(v) == 'string' and #v <= 0xFFFF
end

local function valid_topic_name (s, lax)
    if #s == 0 or #s > 0xFFFF then
        return false
    end
    if not utf8_len(s) then
        return false
    end
    local first = true
    for _, c in utf8_codes(s) do
        if c >= 0x0000 and c <= 0x001F then
            return false
        end
        if c >= 0x007F and c <= 0x009F then
            return false
        end
        if c >= 0xD800 and c <= 0xDFFF then
            return false
        end
        if c == 35 or c == 43 then              -- # +
            return false
        end
        if first then
            if c == 36 and not lax then         -- $
                return false
            end
            first = false
        end
    end
    return true
end

local function valid_topic_filter (s)
    if #s == 0 or #s > 0xFFFF then
        return false
    end
    if not utf8_len(s) then
        return false
    end
    local start = true
    local seen
    for p, c in utf8_codes(s) do
        if c >= 0x0000 and c <= 0x001F then
            return false
        end
        if c >= 0x007F and c <= 0x009F then
            return false
        end
        if c >= 0xD800 and c <= 0xDFFF then
            return false
        end
        if (c == 35 or c == 43) and not start then
            return false
        end
        if c == 35 and p ~= #s then             -- #
            return false
        end
        if c ~= 47 and seen then
            return false
        end
        start = c == 47                         -- /
        seen = c == 43                          -- +
    end
    return true
end

local valid_prop = {
    [1]  = valid_0_1,           -- Payload Format Indicator
    [2]  = valid_4byte,         -- Message Expiry Interval
    [3]  = valid_string,        -- Content Type
    [8]  = function (v)         -- Response Topic
        if type(v) == 'string' then
            return valid_topic_name(v)
        end
    end,
    [9]  = valid_binary,        -- Correlation Data
    [11] = function (v)         -- Subscription Identifier
        return valid_vbi(v) and v ~= 0
    end,
    [17] = valid_4byte,         -- Session Expiry Interval
    [18] = valid_string,        -- Assigned Client Identifier
    [19] = valid_2byte,         -- Server Keep Alive
    [21] = valid_string,        -- Authentication Method
    [22] = valid_binary,        -- Authentication Data
    [23] = valid_0_1,           -- Request Problem Information
    [24] = valid_4byte,         -- Will Delay Interval
    [25] = valid_0_1,           -- Request Response Information
    [26] = valid_string,        -- Response Information
    [28] = valid_string,        -- Server Reference
    [31] = valid_string,        -- Reason String
    [33] = function (v)         -- Receive Maximum
        return valid_2byte(v) and v ~= 0
    end,
    [34] = valid_2byte,         -- Topic Alias Maximum
    [35] = valid_2byte,         -- Topic Alias
    [36] = valid_0_1,           -- Maximum QoS
    [37] = valid_0_1,           -- Retain Available
    [38] = function (t)         -- User Property
        if type(t) == 'table' then
            local k, v = next(t)
            return valid_string(k) and valid_string(v)
        end
    end,
    [39] = function (v)         -- Maximum Packet Size
        return valid_4byte(v) and v ~= 0
    end,
    [40] = valid_0_1,           -- Wildcard Subscription Available
    [41] = valid_0_1,           -- Subscription Identifier Available
    [42] = valid_0_1,           -- Shared Subscription Available
}

local function valid_props (props, known_prop)
    local exist = {}
    for j = 1, #props, 2 do
        local id, val = props[j], props[j+1]
        if not known_prop[id] then
            return nil, 'invalid Property', id, j
        end
        if known_prop[id] == 1 then
            if exist[id] then
                return nil, 'duplicated Property', id, j
            else
                exist[id] = true
            end
        end
        if not valid_prop[id](val) then
            return nil, 'invalid value for Property', id, j + 1
        end
    end
    return true
end

local function encode_vbi (n)
    if n < 0x80 then
        return char(n)
    else
        local t = {}
        repeat
            local b = n % 0x80
            n = n // 0x80
            if n > 0 then
                b = b + 0x80
            end
            t[#t+1] = char(b)
        until n == 0
        return concat(t)
    end
end

local function encode_byte (v)
    return pack('>I1', v)
end

local function encode_2byte (v)
    return pack('>I2', v)
end

local function encode_4byte (v)
    return pack('>I4', v)
end

local function encode_string (v)
    return pack('>s2', v)
end

local function encode_pair (v)
    return pack('>s2s2', next(v))
end

local function encode_binary (v)
    return pack('>s2', v)
end

local encode_prop = {
    [1]  = encode_byte,         -- Payload Format Indicator
    [2]  = encode_4byte,        -- Message Expiry Interval
    [3]  = encode_string,       -- Content Type
    [8]  = encode_string,       -- Response Topic
    [9]  = encode_binary,       -- Correlation Data
    [11] = encode_vbi,          -- Subscription Identifier
    [17] = encode_4byte,        -- Session Expiry Interval
    [18] = encode_string,       -- Assigned Client Identifier
    [19] = encode_2byte,        -- Server Keep Alive
    [21] = encode_string,       -- Authentication Method
    [22] = encode_binary,       -- Authentication Data
    [23] = encode_byte,         -- Request Problem Information
    [24] = encode_4byte,        -- Will Delay Interval
    [25] = encode_byte,         -- Request Response Information
    [26] = encode_string,       -- Response Information
    [28] = encode_string,       -- Server Reference
    [31] = encode_string,       -- Reason String
    [33] = encode_2byte,        -- Receive Maximum
    [34] = encode_2byte,        -- Topic Alias Maximum
    [35] = encode_2byte,        -- Topic Alias
    [36] = encode_byte,         -- Maximum QoS
    [37] = encode_byte,         -- Retain Available
    [38] = encode_pair,         -- User Property
    [39] = encode_4byte,        -- Maximum Packet Size
    [40] = encode_byte,         -- Wildcard Subscription Available
    [41] = encode_byte,         -- Subscription Identifier Available
    [42] = encode_byte,         -- Shared Subscription Available
}

local function encode_props (props)
    if not props then
        return '\0'
    end
    local t = {}
    for i = 1, #props, 2 do
        local id = props[i]
        local val = props[i+1]
        local fn = encode_prop[id]
        t[#t+1] = encode_vbi(id) .. fn(val)
    end
    local s = concat(t)
    return encode_vbi(len(s)) .. s
end

local function decode_vbi (s, pos)
    local v = 0
    local mult = 1
    repeat
        local b = byte(sub(s, pos, pos))
        if not b then
            return nil, "invalid vbi (data too short)"
        end
        pos = pos + 1
        v = v + (b & 0x7F) * mult
        mult = mult * 0x80
        if mult > 0x200000 then
            return nil, "invalid vbi (too long)"
        end
    until (b & 0x80) == 0
    return v, pos
end

local function decode_byte (s, pos)
    return unpack('>I1', s, pos)
end

local function decode_2byte (s, pos)
    return unpack('>I2', s, pos)
end

local function decode_4byte (s, pos)
    return unpack('>I4', s, pos)
end

local function decode_string (s, pos)
    return unpack('>s2', s, pos)
end

local function decode_pair (s, pos)
    local k, v
    k, v, pos = unpack('>s2s2', s, pos)
    return { [k] = v }, pos
end

local function decode_binary (s, pos)
    return unpack('>s2', s, pos)
end

local decode_prop = {
    [1]  = decode_byte,         -- Payload Format Indicator
    [2]  = decode_4byte,        -- Message Expiry Interval
    [3]  = decode_string,       -- Content Type
    [8]  = decode_string,       -- Response Topic
    [9]  = decode_binary,       -- Correlation Data
    [11] = decode_vbi,          -- Subscription Identifier
    [17] = decode_4byte,        -- Session Expiry Interval
    [18] = decode_string,       -- Assigned Client Identifier
    [19] = decode_2byte,        -- Server Keep Alive
    [21] = decode_string,       -- Authentication Method
    [22] = decode_binary,       -- Authentication Data
    [23] = decode_byte,         -- Request Problem Information
    [24] = decode_4byte,        -- Will Delay Interval
    [25] = decode_byte,         -- Request Response Information
    [26] = decode_string,       -- Response Information
    [28] = decode_string,       -- Server Reference
    [31] = decode_string,       -- Reason String
    [33] = decode_2byte,        -- Receive Maximum
    [34] = decode_2byte,        -- Topic Alias Maximum
    [35] = decode_2byte,        -- Topic Alias
    [36] = decode_byte,         -- Maximum QoS
    [37] = decode_byte,         -- Retain Available
    [38] = decode_pair,         -- User Property
    [39] = decode_4byte,        -- Maximum Packet Size
    [40] = decode_byte,         -- Wildcard Subscription Available
    [41] = decode_byte,         -- Subscription Identifier Available
    [42] = decode_byte,         -- Shared Subscription Available
}

local function decode_props (s, pos)
    local n
    n, pos = decode_vbi(s, pos)
    if not n then
        return nil, "properties: " .. pos
    end
    local rpos = pos + n
    local t = {}
    if n ~= 0 then
        while pos < rpos do
            local id, p, status
            id, pos = decode_vbi(s, pos)
            if not id then
                return nil, "properties: " .. pos
            end
            local fn = decode_prop[id]
            if not fn then
                return nil, "property " .. tostring(id) .. ": unknown property"
            end
            status, p, pos = pcall(fn, s, pos)
            if not status then
                return nil, "property " .. tostring(id) .. ": " .. (p:gsub('[^:]+:%d+: ', ''))
            end
            if not p then
                return nil, "property " .. tostring(id) .. ": " .. pos
            end
            t[#t+1] = id
            t[#t+1] = p
        end
    end
    return t, rpos
end

function mt:_log_props (props)
    for i = 1, #props, 2 do
        local id = props[i]
        local p = props[i+1]
        if type(p) == 'table' then
            local k, v = next(p)
            self.logger:debug('MQTT  Prop ' .. tostring(id) .. ' ' .. tostring(k) .. ' = ' .. tostring(v))
        else
            self.logger:debug('MQTT  Prop ' .. tostring(id) .. ' ' .. tostring(p))
        end
    end
end

local function cook (fixed_header, variable_header, payload, max)
    variable_header = variable_header or ''
    payload = payload or ''
    local rem_len = len(variable_header) + len(payload)
    if max and rem_len > max then
        error("Packet too large", 3)
    end
    return char(fixed_header) .. encode_vbi(rem_len) .. variable_header .. payload
end

local function compute_id (id)
    if id then
        id = id + 1
        if id <= 0xFFFF then
            return id
        end
    end
    return random(0x0100, 0xFFFF)
end

local function next_id (queue, id)
    while true do
        id = compute_id(id)
        local found
        for i = 1, #queue do
            if queue[i] == id then
                found = true
                break
            end
        end
        if not found then
            return id
        end
        id = nil
    end
end

local function remove_id (queue, id)
    local found
    repeat
        found = false
        local nb = #queue
        local last = 0
        for i = 1, #queue do
            last = i
            if queue[i] == id then
                remove(queue, i)
                found = true
                break
            end
        end
    until found or nb == last
end

function mt._mk_connect (flags, keep_alive, client_id, will_props, will_topic, will_message, username, password, props)
    local payload = pack('>s2', client_id)
    if will_topic and will_message then
        payload = payload .. encode_props(will_props)
        payload = payload .. pack('>s2s2', will_topic, will_message)
    end
    if username then
        payload = payload .. pack('>s2', username)
    end
    if password then
        payload = payload .. pack('>s2', password)
    end
    return cook(0x10, pack('>s2I1I1I2', 'MQTT', 5, flags, keep_alive) .. encode_props(props), payload)
end

function mt:_send_connect (flags, keep_alive, client_id, will_props, will_topic, will_message, username, password, props)
    if self.logger then
        self.logger:debug('MQTT> CONNECT ' .. tostring(keep_alive) .. ' ' .. client_id)
        if will_topic and will_message then
            self.logger:debug('MQTT> Will ' .. will_topic)
        end
        self:_log_props(props)
    end
    self.connect_sent = true
    self.connack_received = false
    return self.socket:write(self._mk_connect(flags, keep_alive, client_id, will_props, will_topic, will_message, username, password, props))
end

-- connack

function mt._mk_publish (flags, id, topic, payload, props, max)
    return cook(0x30 | flags, (id and pack('>s2I2', topic, id) or pack('>s2', topic)) .. encode_props(props), payload, max)
end

function mt:_send_publish (flags, id, topic, payload, props, max)
    local packet = self._mk_publish(flags, id, topic, payload, props, max)
    if self.logger then
        if id then
            self.logger:debug('MQTT> PUBLISH ' .. tostring(id) .. ' ' .. topic)
        else
            self.logger:debug('MQTT> PUBLISH ' .. topic)
        end
        self:_log_props(props)
    end
    return self.socket:write(packet)
end

function mt._mk_puback (id, reason_code, props)
    local prop = encode_props(props)
    if prop == '\0' and reason_code == 0 then
        return cook(0x40, pack('>I2', id))
    else
        return cook(0x40, pack('>I2I1', id, reason_code) .. prop)
    end
end

function mt:_send_puback (id, reason_code, props)
    if self.logger then
        self.logger:debug('MQTT> PUBACK ' .. tostring(id) .. ' ' .. tostring(reason_code))
    end
    return self.socket:write(self._mk_puback(id, reason_code, props))
end

function mt._mk_pubrec (id, reason_code, props)
    local prop = encode_props(props)
    if prop == '\0' and reason_code == 0 then
        return cook(0x50, pack('>I2', id))
    else
        return cook(0x50, pack('>I2I1', id, reason_code) .. prop)
    end
end

function mt:_send_pubrec (id, reason_code, props)
    if self.logger then
        self.logger:debug('MQTT> PUBREC ' .. tostring(id) .. ' ' .. tostring(reason_code))
    end
    return self.socket:write(self._mk_pubrec(id, reason_code, props))
end

function mt._mk_pubrel (id, reason_code, props)
    local prop = encode_props(props)
    if prop == '\0' and reason_code == 0 then
        return cook(0x62, pack('>I2', id))
    else
        return cook(0x62, pack('>I2I1', id, reason_code) .. prop)
    end
end

function mt:_send_pubrel (id, reason_code, props)
    if self.logger then
        self.logger:debug('MQTT> PUBREL ' .. tostring(id) .. ' ' .. tostring(reason_code))
    end
    return self.socket:write(self._mk_pubrel(id, reason_code, props))
end

function mt._mk_pubcomp (id, reason_code, props)
    local prop = encode_props(props)
    if prop == '\0' and reason_code == 0 then
        return cook(0x70, pack('>I2', id))
    else
        return cook(0x70, pack('>I2I1', id, reason_code) .. prop)
    end
end

function mt:_send_pubcomp (id, reason_code, props)
    if self.logger then
        self.logger:debug('MQTT> PUBCOMP ' .. tostring(id) .. ' ' .. tostring(reason_code))
    end
    return self.socket:write(self._mk_pubcomp(id, reason_code, props))
end

function mt._mk_subscribe (id, list, props, max)
    local t = {}
    for i = 1, #list, 2 do
        t[#t+1] = pack('>s2I1', list[i], list[i+1])
    end
    return cook(0x82, pack('>I2', id) .. encode_props(props), concat(t), max)
end

function mt:_send_subscribe (id, list, props, max)
    local packet = self._mk_subscribe(id, list, props, max)
    if self.logger then
        local t = {}
        for i = 1, #list do
            t[#t+1] = tostring(list[i])
        end
        self.logger:debug('MQTT> SUBSCRIBE ' .. tostring(id) .. ' ' .. concat(t, ' '))
        self:_log_props(props)
    end
    return self.socket:write(packet)
end

-- suback

function mt._mk_unsubscribe (id, list, props, max)
    local t = {}
    for i = 1, #list do
        t[#t+1] = pack('>s2', list[i])
    end
    return cook(0xA2, pack('>I2', id) .. encode_props(props), concat(t), max)
end

function mt:_send_unsubscribe (id, list, props, max)
    local packet = self._mk_unsubscribe(id, list, props, max)
    if self.logger then
        self.logger:debug('MQTT> UNSUBSCRIBE ' .. tostring(id) .. ' ' .. concat(list, ' '))
        self:_log_props(props)
    end
    return self.socket:write(packet)
end

-- unsuback

function mt._mk_pingreq ()
    return cook(0xC0)
end

function mt:_send_pingreq ()
    if self.logger then
        self.logger:debug('MQTT> PINGREQ')
    end
    return self.socket:write(self._mk_pingreq())
end

-- pingresp

function mt._mk_disconnect (reason_code, props, max)
    local prop = encode_props(props)
    if prop == '\0' and reason_code == 0 then
        return cook(0xE0)
    else
        return cook(0xE0, pack('>I1', reason_code) .. prop, nil, max)
    end
end

function mt:_send_disconnect (reason_code, props, max)
    local packet = self._mk_disconnect(reason_code, props, max)
    if self.logger then
        self.logger:debug('MQTT> DISCONNECT ' .. tostring(reason_code))
        self:_log_props(props)
    end
    return self.socket:write(packet)
end

function mt._mk_auth (reason_code, props)
    local prop = encode_props(props)
    if prop == '\0' and reason_code == 0 then
        return cook(0xF0)
    else
        return cook(0xF0, pack('>I1', reason_code) .. prop)
    end
end

function mt:_send_auth (reason_code, props)
    if self.logger then
        self.logger:debug('MQTT> AUTH ' .. tostring(reason_code))
        self:_log_props(props)
    end
    return self.socket:write(self._mk_auth(reason_code, props))
end

local function new_session ()
    return {
        queue       = {},
        rqueue      = {},
        ptype       = {},
        publish     = {},
        pubrec      = {},
        pubrel      = {},
        subscribe   = {},
        subid       = {},
        unsubscribe = {},
        client      = {
            session_expiry_interval            = 0,             -- 17
            receive_maximum                    = 0xFFFF,        -- 33
            topic_alias_maximum                = 0,             -- 34
            maximum_qos                        = -1,
            maximum_packet_size                = 0xFFFFFFF,     -- 39
        },
        server      = {
            session_expiry_interval            = 0,             -- 17
            receive_maximum                    = 0xFFFF,        -- 33
            topic_alias_maximum                = 0,             -- 34
            maximum_qos                        = 2,             -- 36
            retain_available                   = true,          -- 37
            maximum_packet_size                = 0xFFFFFFF,     -- 39
            wildcard_subscription_available    = true,          -- 40
            subscription_identifiers_available = true,          -- 41
            shared_subscription_available      = true,          -- 42
        },
    }
end

local valid_qos = {
    [0] = true,
    [1] = true,
    [2] = true,
}

local valid_prop_connect = {
    [17] = 1,           -- Session Expiry Interval
    [21] = 1,           -- Authentication Method
    [22] = 1,           -- Authentication Data
    [23] = 1,           -- Request Problem Information
    [25] = 1,           -- Request Response Information
    [33] = 1,           -- Receive Maximum
    [34] = 1,           -- Topic Alias Maximum
    [38] = true,        -- User Property
    [39] = 1,           -- Maximum Packet Size
}

local valid_prop_will = {
    [1]  = 1,           -- Payload Format Indicator
    [2]  = 1,           -- Message Expiry Interval
    [3]  = 1,           -- Content Type
    [8]  = 1,           -- Response Topic
    [9]  = 1,           -- Correlation Data
    [24] = 1,           -- Will Delay Interval
    [38] = true,        -- User Property
}

function mt:connect (options, props)
    if self.connect_sent then
        error("CONNECT already sent")
    end
    options = options or {}
    if type(options) ~= 'table' then
        typeerror('connect', 1, options, 'table')
    end
    props = props or  {}
    if type(props) ~= 'table' then
        typeerror('connect', 2, props, 'table')
    end
    local keep_alive = options.keep_alive or 0
    if not valid_2byte(keep_alive) then
        error("invalid Keep Alive (" .. tostring(keep_alive) .. ") in table to 'connect'")
    end
    local username = options.username
    if username and type(username) ~= 'string' then
        error("invalid User Name (" .. tostring(username) .. ") in table to 'connect'")
    end
    local password = options.password
    if password and type(password) ~= 'string' then
        error("invalid Password (" .. tostring(password) .. ") in table to 'connect'")
    end
    if password and not username then
        error("Password without User Name in table to 'connect'")
    end
    local will = options.will or {}
    if type(will) ~= 'table' then
        error("invalid Will (" .. tostring(will) .. ") in table to 'connect'")
    end
    local flags = 0

    local client_id = options.id
    if client_id and type(client_id) ~= 'string' then
        error("invalid Client Identifier (" .. tostring(client_id) .. ") in table to 'connect'")
    end
    if options.clean then
        client_id = client_id or format('LuaMQTT%08x', random(0, 0x7FFFFFFF))
    else
        client_id = client_id or ''
    end
    if options.clean or client_id == '' then
        flags = flags | 0x02            -- Clean Start
    end

    local will_topic = will[1]
    local will_message = will[2]
    local will_qos = will.qos or 0
    local will_props = will.props or {}
    if will_topic and will_message then
        will.props = will_props
        if type(will_props) ~= 'table' then
            error("invalid Will Props (" .. tostring(will_props) .. ") in table to 'connect'")
        end
        if type(will_topic) ~= 'string' or not valid_topic_name(will_topic) then
            error("invalid Will Topic (" .. tostring(will_topic) .. ") in table to 'connect'")
        end
        if type(will_message) ~= 'string' then
            error("invalid Will Message (" .. tostring(will_message) .. ") in table to 'connect'")
        end
        if not valid_qos[will_qos] then
            error("invalid Will QoS (" .. tostring(will_qos) .. ") in table to 'connect'")
        end
        local res, msg, prop, idx = valid_props(will_props, valid_prop_will)
        if not res then
            error(msg .. " (" .. tostring(prop) .. ") at index " .. tostring(idx) .. " in will to 'connect'")
        end

        flags = flags | 0x04            -- Will flag
        flags = flags | (will_qos << 3) -- Will QoS
        if will.retain then
            flags = flags | 0x20        -- Will Retain
        end
    end
    if username then
        flags = flags | 0x80            -- User Name Flag
        if password then
            flags = flags | 0x40        -- Password Flag
        end
    end
    local session = self.session
    local res, msg, prop, idx = valid_props(props, valid_prop_connect)
    if not res then
        error(msg .. " (" .. tostring(prop) .. ") at index " .. tostring(idx) .. " in table to 'connect'")
    end
    for j = 1, #props, 2 do
        local id, val = props[j], props[j+1]
        if     id == 17 then
            session.client.session_expiry_interval = val
            session.server.session_expiry_interval = val
        elseif id == 33 then
            session.client.receive_maximum = val
        elseif id == 34 then
            session.client.topic_alias_maximum = val
        elseif id == 39 then
            session.client.maximum_packet_size = val
        end
    end

    session.alias = {}
    res, msg = self:_send_connect(flags, keep_alive, client_id, will_props, will_topic, will_message, username, password, props)
    if res then
        self.clean = options.clean
        self.keep_alive = keep_alive
        self.id = client_id
        self.will = will
        self.password = password
        self.username = username
        self.props = props
    end
    return res, msg
end

function mt:reconnect (socket)
    if not self.connect_sent then
        error("CONNECT not sent")
    end
    if not socket or not valid_socket(socket) then
        typeerror('reconnect', 1, socket, 'socket')
    end
    self.socket = socket
    local will = self.will
    local username = self.username
    local password = self.password
    local flags = 0
    if self.clean or self.id == '' then
        flags = flags | 0x02            -- Clean Start
        self.session = new_session()
    end
    local will_topic = will[1]
    local will_message = will[2]
    local will_qos = will.qos or 0
    local will_props = will.props
    if will_topic and will_message then
        flags = flags | 0x04            -- Will flag
        flags = flags | (will_qos << 3) -- Will QoS
        if will.retain then
            flags = flags | 0x20        -- Will Retain
        end
    end
    if username then
        flags = flags | 0x80            -- User Name Flag
        if password then
            flags = flags | 0x40        -- Password Flag
        end
    end

    self.session.alias = {}
    return self:_send_connect(flags, self.keep_alive, self.id, will_props, will_topic, will_message, username, password, self.props)
end

local valid_prop_publish = {
    [1]  = 1,           -- Payload Format Indicator
    [2]  = 1,           -- Message Expiry Interval
    [3]  = 1,           -- Content Type
    [8]  = 1,           -- Response Topic
    [9]  = 1,           -- Correlation Data
    [35] = 1,           -- Topic Alias
    [38] = true,        -- User Property
}

function mt:publish (topic, payload, options, props)
    if not self.connect_sent then
        error("CONNECT not sent")
    end
    local session = self.session
    if type(topic) ~= 'string' then
        typeerror('publish', 1, topic, 'string')
    end
    if not valid_topic_name(topic) then
        error("invalid Topic Name to 'publish'")
    end
    if type(payload) ~= 'string' then
        typeerror('publish', 2, payload, 'string')
    end
    options = options or {}
    if type(options) ~= 'table' then
        typeerror('publish', 3, options, 'table')
    end
    props = props or  {}
    if type(props) ~= 'table' then
        typeerror('publish', 4, props, 'table')
    end
    local qos = options.qos or 0
    if not valid_qos[qos] then
        error("invalid QoS (" .. tostring(qos) .. ") in table to 'publish'")
    end
    if qos > session.server.maximum_qos then
        error("QoS not supported (" .. tostring(qos) .. ")")
    end
    local retain = options.retain
    if retain and not session.server.retain_available then
        error("Retain not supported")
    end
    local res, msg, prop, idx = valid_props(props, valid_prop_publish)
    if not res then
        error(msg .. " (" .. tostring(prop) .. ") at index " .. tostring(idx) .. " in table to 'publish'")
    end
    for j = 1, #props, 2 do
        local id, val = props[j], props[j+1]
        if     id == 1 then
            if val == 1 and not utf8_len(payload) then
                error("invalid Payload Format to 'publish'")
            end
        elseif id == 35 then
            if (val == 0 or val > session.server.topic_alias_maximum) then
                error("invalid Topic Alias (" .. tostring(val) .. ") at index " .. tostring(j+1) .. " in table to 'publish'")
            end
            if session.alias[val] == topic then
                topic = ''
            else
                session.alias[val] = topic
            end
        end
    end

    local flags = (qos << 1) | (retain and 0x1 or 0x0)
    local id
    if qos > 0 then
        if #session.queue >= session.server.receive_maximum then
            error("Receive Maximum exceeded")
        end
        id = next_id(session.queue, session.pid)
        session.pid = id
        insert(session.queue, id)
        session.ptype[id] = 'publish'
        session.publish[id] = { topic, payload, options, props }
    end
    return self:_send_publish(flags, id, topic, payload, props, session.server.maximum_packet_size)
end

local valid_prop_subscribe = {
    [11] = 1,           -- Subscription Identifier
    [38] = true,        -- User Property
}

function mt:subscribe (list, props)
    if not self.connect_sent then
        error("CONNECT not sent")
    end
    if type(list) ~= 'table' then
        typeerror('subscribe', 1, list, 'table')
    end
    props = props or  {}
    if type(props) ~= 'table' then
        typeerror('subscribe', 2, props, 'table')
    end
    if #list == 0 then
        error("empty table for 'subscribe'")
    end
    local session = self.session
    local max_qos = -1
    for i = 1, #list, 2 do
        local filter, options = list[i], list[i+1]
        if type(filter) ~= 'string' or not valid_topic_filter(filter) then
            error("invalid Topic Filter (" .. tostring(filter) .. ") at index " .. tostring(i) .. " in table to 'subscribe'")
        end
        if not session.server.wildcard_subscription_available and filter:match('[#+]') then
            error("Wildcard subscriptions not supported (" .. filter .. ")")
        end
        if not session.server.shared_subscription_available and filter:match('^%$share/') then
            error("Shared subscriptions not supported (" .. filter .. ")")
        end
        if math_type(options) ~= 'integer' or options < 0 or options > 0x3F then
            error("invalid Options (" .. tostring(options) .. ") at index " .. tostring(i+1) .. " in table to 'subscribe'")
        end
        local qos = options & 0x03
        if qos == 3 then
            error("invalid Requested QoS (" .. tostring(qos) .. ") at index " .. tostring(i+1) .. " in table to 'subscribe'")
        end
        if qos > max_qos then
            max_qos = qos
        end
        if (options & 0x04) ~= 0 and filter:match('^%$share/') then
            error("No Local on a Shared Subscription at index " .. tostring(i+1) .. " in table to 'subscribe'")
        end
        local handling = options >> 4
        if handling == 3 then
            error("invalid Retain Handling option (" .. tostring(handling) .. ") at index " .. tostring(i+1) .. " in table to 'subscribe'")
        end
    end
    local res, msg, prop, idx = valid_props(props, valid_prop_subscribe)
    if not res then
        error(msg .. " (" .. tostring(prop) .. ") at index " .. tostring(idx) .. " in table to 'subscribe'")
    end
    for j = 1, #props, 2 do
        local id, val = props[j], props[j+1]
        if id == 11 then
            if not session.server.subscription_identifiers_available then
                error("Subscription identifiers not supported")
            end
            session.subid[val] = max_qos
        end
    end
    if max_qos > session.client.maximum_qos then
        session.client.maximum_qos = max_qos
    end

    local id = next_id(session.queue, session.pid)
    session.pid = id
    insert(session.queue, id)
    session.ptype[id] = 'subscribe'
    session.subscribe[id] = { list, props }
    return self:_send_subscribe(id, list, props, session.server.maximum_packet_size)
end

local valid_prop_unsubscribe = {
    [38] = true,        -- User Property
}

function mt:unsubscribe (list, props)
    if not self.connect_sent then
        error("CONNECT not sent")
    end
    if type(list) ~= 'table' then
        typeerror('unsubscribe', 1, list, 'table')
    end
    props = props or  {}
    if type(props) ~= 'table' then
        typeerror('unsubscribe', 2, props, 'table')
    end
    if #list == 0 then
        error("empty table for 'unsubscribe'")
    end
    for i = 1, #list do
        local filter = list[i]
        if type(filter) ~= 'string' or not valid_topic_filter(filter) then
            error("invalid Topic Filter (" .. tostring(filter) .. ") at index " .. tostring(i) .. " in table to 'unsubscribe'")
        end
    end
    local res, msg, prop, idx = valid_props(props, valid_prop_unsubscribe)
    if not res then
        error(msg .. " (" .. tostring(prop) .. ") at index " .. tostring(idx) .. " in table to 'unsubscribe'")
    end

    local session = self.session
    local id = next_id(session.queue, session.pid)
    session.pid = id
    insert(session.queue, id)
    session.ptype[id] = 'unsubscribe'
    session.unsubscribe[id] = { list, props }
    return self:_send_unsubscribe(id, list, props, session.server.maximum_packet_size)
end

function mt:ping ()
    if not self.connect_sent then
        error("CONNECT not sent")
    end
    return self:_send_pingreq()
end

local valid_prop_disconnect = {
    [17] = 1,           -- Session Expiry Interval
    [28] = 1,           -- Server Reference
    [31] = 1,           -- Reason String
    [38] = true,        -- User Property
}

local valid_reason_disconnect = {
    [0]   = true,       -- Normal disconnection
    [4]   = true,       -- Disconnect with Will Message
    [128] = true,       -- Unspecified error
    [129] = true,       -- Malformed Packet
    [130] = true,       -- Protocol Error
    [131] = true,       -- Implementation specific error
    [144] = true,       -- Topic Name invalid
    [147] = true,       -- Receive Maximum exceeded
    [148] = true,       -- Topic Alias invalid
    [149] = true,       -- Packet too large
    [150] = true,       -- Message rate too high
    [151] = true,       -- Quota exceeded
    [152] = true,       -- Administrative action
    [153] = true,       -- Payload format invalid
}

function mt:disconnect (reason_code, props)
    if not self.connect_sent then
        error("CONNECT not sent")
    end
    reason_code = reason_code or 0
    if math_type(reason_code) ~= 'integer'  then
        typeerror('disconnect', 1, reason_code, 'integer')
    end
    if not valid_reason_disconnect[reason_code] then
        error("invalid Reason Code (" .. tostring(reason_code) .. ") to 'disconnect'")
    end
    props = props or {}
    if type(props) ~= 'table' then
        typeerror('disconnect', 2, props, 'table')
    end
    local session = self.session
    local res, msg, prop, idx = valid_props(props, valid_prop_disconnect)
    if not res then
        error(msg .. " (" .. tostring(prop) .. ") at index " .. tostring(idx) .. " in table to 'disconnect'")
    end

    return self:_send_disconnect(reason_code, props, session.server.maximum_packet_size)
end

local valid_prop_auth = {
    [21] = 1,           -- Authentication Method
    [22] = 1,           -- Authentication Data
    [31] = 1,           -- Reason String
    [38] = true,        -- User Property
}

local valid_reason_auth = {
    [24] = true,        -- Continue authentication
    [25] = true,        -- Re-authenticate
}

function mt:auth (reason_code, props)
    if not self.connect_sent then
        error("CONNECT not sent")
    end
    reason_code = reason_code or 0
    if math_type(reason_code) ~= 'integer'  then
        typeerror('auth', 1, reason_code, 'integer')
    end
    if not valid_reason_auth[reason_code] then
        error("invalid Reason Code (" .. tostring(reason_code) .. ") to 'auth'")
    end
    props = props or  {}
    if type(props) ~= 'table' then
        typeerror('auth', 2, props, 'table')
    end
    local res, msg, prop, idx = valid_props(props, valid_prop_auth)
    if not res then
        error(msg .. " (" .. tostring(prop) .. ") at index " .. tostring(idx) .. " in table to 'auth'")
    end

    return self:_send_auth(reason_code, props)
end

function mt:_connect (reason_code, session_present, props)
    if self.on_connect then
        local status, msg = pcall(self.on_connect, reason_code, session_present, props)
        if not status and self.logger then
            self.logger:error('MQTT on_connect throws --> ' .. tostring(msg))
        end
    else
        if self.logger then
            self.logger:debug('MQTT no on_connect callback')
        end
    end
end

function mt:_message (topic, payload, props)
    if self.on_message then
        local status, msg = pcall(self.on_message, topic, payload, props)
        if not status and self.logger then
            self.logger:error('MQTT on_message throws --> ' .. tostring(msg))
        end
    else
        if self.logger then
            self.logger:debug('MQTT no on_message callback')
        end
    end
end

function mt:_error (msg)
    if self.logger then
        self.logger:error('MQTT LOST_CONNECTION: ' .. tostring(msg))
    end
    if self.on_error then
        local status, result, message = pcall(self.on_error)
        if status then
            if result then
                return self:_fetch()
            else
                return nil, message
            end
        else
            if self.logger then
                self.logger:error('MQTT on_error throws --> ' .. tostring(result))
            end
            return nil, result
        end
    else
        if self.logger then
            self.logger:error('MQTT no on_error callback')
        end
        return nil, msg
    end
end

function mt:_disconnect (reason_code, props)
    if self.on_disconnect then
        local status, msg = pcall(self.on_disconnect, reason_code, props)
        if not status and self.logger then
            self.logger:error('MQTT on_disconnect throws --> ' .. tostring(msg))
        end
    else
        if self.logger then
            self.logger:debug('MQTT no on_disconnect callback')
        end
    end
end

function mt:_hangup (reason_code, msg)
    self:_send_disconnect(reason_code, {})
    if self.logger then
        self.logger:error('MQTT HANGUP: ' .. msg)
    end
    local sock = self.socket
    sock:close()
    self.socket = nil
    return nil, msg
end

local valid_reason_connack = {
    [0]   = true,       -- Success
    [128] = true,       -- Unspecified error
    [129] = true,       -- Malformed Packet
    [130] = true,       -- Protocol Error
    [131] = true,       -- Implementation specific error
    [132] = true,       -- Unsupported Protocol Version
    [133] = true,       -- Client Identifier not valid
    [134] = true,       -- Bad User Name or Password
    [135] = true,       -- Not authorized
    [136] = true,       -- Server unavailable
    [137] = true,       -- Server busy
    [138] = true,       -- Banned
    [140] = true,       -- Bad authentication method
    [144] = true,       -- Topic Name invalid
    [149] = true,       -- Packet too large
    [151] = true,       -- Quota exceeded
    [153] = true,       -- Payload format invalid
    [154] = true,       -- Retain not supported
    [155] = true,       -- QoS not supported
    [156] = true,       -- Use another server
    [157] = true,       -- Server moved
    [159] = true,       -- Connection rate exceeded
}

local valid_prop_connack = {
    [17] = 1,           -- Session Expiry Interval
    [18] = 1,           -- Assigned Client Identifier
    [19] = 1,           -- Server Keep Alive
    [21] = 1,           -- Authentication Method
    [22] = 1,           -- Authentication Data
    [26] = 1,           -- Response Information
    [28] = 1,           -- Server Reference
    [31] = 1,           -- Reason String
    [33] = 1,           -- Receive Maximum
    [34] = 1,           -- Topic Alias Maximum
    [36] = 1,           -- Maximum QoS
    [37] = 1,           -- Retain Available
    [38] = true,        -- User Property
    [39] = 1,           -- Maximum Packet Size
    [40] = 1,           -- Wildcard Subscription Available
    [41] = 1,           -- Subscription Identifier Available
    [42] = 1,           -- Shared Subscription Available
}

function mt:_parse_connack (s)
    local flags, rc = unpack('>I1I1', s)
    if (flags & 0xFE) ~= 0 then
        return self:_hangup(129, format('invalid flags in CONNACK: %02X', flags))       -- Malformed Packet
    end
    local sp = flags ~= 0
    if not valid_reason_connack[rc] then
        return self:_hangup(130, 'invalid reason in CONNACK: ' .. tostring(rc))         -- Protocol Error
    end
    if sp and rc ~= 0 then
        return self:_hangup(130, 'invalid CONNACK (sp with rc)')                        -- Protocol Error
    end
    if self.logger then
        self.logger:debug('MQTT< CONNACK sp=' .. tostring(flags) .. ' rc=' .. tostring(rc))
    end
    local props, pos = decode_props(s, 3)
    if not props then
        return self:_hangup(129, 'invalid data in CONNACK ' .. tostring(pos))           -- Malformed Packet
    end
    local res, msg, prop = valid_props(props, valid_prop_connack)
    if not res then
        return self:_hangup(130, msg .. ' ' .. tostring(prop) .. ' in CONNACK')         -- Protocol Error
    end
    if self.logger then
        self:_log_props(props)
    end
    return {
        type  = 'CONNACK',
        sp    = sp,
        rc    = rc,
        props = props,
    }
end

local valid_prop_rpublish = {
    [1]  = 1,           -- Payload Format Indicator
    [2]  = 1,           -- Message Expiry Interval
    [3]  = 1,           -- Content Type
    [8]  = 1,           -- Response Topic
    [9]  = 1,           -- Correlation Data
    [11] = true,        -- Subscription Identifier
    [35] = 1,           -- Topic Alias
    [38] = true,        -- User Property
}

function mt:_parse_publish (s, flags)
    local dup = (flags & 0x8) ~= 0
    local retain = (flags & 0x1) ~= 0
    local qos = (flags & 0x6) >> 1
    if qos == 0x3 then
        return self:_hangup(129, 'invalid QoS for PUBLISH')             -- Malformed Packet
    end
    local status, topic, id, props
    local pos = 1
    if qos == 0 then
        status, topic, pos = pcall(unpack, '>s2', s, pos)
    else
        status, topic, id, pos = pcall(unpack, '>s2I2', s, pos)
    end
    if not status then
        return self:_hangup(129, 'invalid message for PUBLISH: ' .. topic)      -- Malformed Packet
    end
    if self.logger then
        if id then
            self.logger:debug('MQTT< PUBLISH ' .. tostring(id) .. ' ' .. topic)
        else
            self.logger:debug('MQTT< PUBLISH ' .. topic)
        end
    end
    props, pos = decode_props(s, pos)
    if not props then
        return self:_hangup(129, 'invalid data in PUBLISH ' .. tostring(pos))           -- Malformed Packet
    end
    local res, msg, prop = valid_props(props, valid_prop_rpublish)
    if not res then
        return self:_hangup(130, msg .. ' ' .. tostring(prop) .. ' in PUBLISH')         -- Protocol Error
    end
    if self.logger then
        self:_log_props(props)
    end
    return {
        type    = 'PUBLISH',
        dup     = dup,
        retain  = retain,
        qos     = qos,
        topic   = topic,
        id      = id,
        props   = props,
        payload = sub(s, pos),
    }
end

local valid_prop_ack = {
    [31] = 1,           -- Reason String
    [38] = true,        -- User Property
}

function mt:_parse_puback (s)
    local id, rc, props
    if #s == 2 then
        id = unpack('>I2', s)
        rc = 0
    else
        id, rc = unpack('>I2I1', s)
    end
    if self.logger then
        self.logger:debug('MQTT< PUBACK ' .. tostring(id) .. ' ' .. tostring(rc))
    end
    if #s > 3 then
        local pos
        props, pos = decode_props(s, 4)
        if not props then
            return self:_hangup(129, 'invalid data in PUBACK ' .. tostring(pos))        -- Malformed Packet
        end
        local res, msg, prop = valid_props(props, valid_prop_ack)
        if not res then
            return self:_hangup(130, msg .. ' ' .. tostring(prop) .. ' in PUBACK')      -- Protocol Error
        end
        if self.logger then
            self:_log_props(props)
        end
    end
    return {
        type  = 'PUBACK',
        id    = id,
        rc    = rc,
        props = props,
    }
end

function mt:_parse_pubrec (s)
    local id, rc, props
    if #s == 2 then
        id = unpack('>I2', s)
        rc = 0
    else
        id, rc = unpack('>I2I1', s)
    end
    if self.logger then
        self.logger:debug('MQTT< PUBREC ' .. tostring(id) .. ' ' .. tostring(rc))
    end
    if #s > 3 then
        local pos
        props, pos = decode_props(s, 4)
        if not props then
            return self:_hangup(129, 'invalid data in PUBREC ' .. tostring(pos))        -- Malformed Packet
        end
        local res, msg, prop = valid_props(props, valid_prop_ack)
        if not res then
            return self:_hangup(130, msg .. ' ' .. tostring(prop) .. ' in PUBREC')      -- Protocol Error
        end
        if self.logger then
            self:_log_props(props)
        end
    end
    return {
        type  = 'PUBREC',
        id    = id,
        rc    = rc,
        props = props,
    }
end

function mt:_parse_pubrel (s)
    local id, rc, props
    if #s == 2 then
        id = unpack('>I2', s)
        rc = 0
    else
        id, rc = unpack('>I2I1', s)
    end
    if self.logger then
        self.logger:debug('MQTT< PUBREL ' .. tostring(id) .. ' ' .. tostring(rc))
    end
    if #s > 3 then
        local pos
        props, pos = decode_props(s, 4)
        if not props then
            return self:_hangup(129, 'invalid data in PUBREL ' .. tostring(pos))        -- Malformed Packet
        end
        local res, msg, prop = valid_props(props, valid_prop_ack)
        if not res then
            return self:_hangup(130, msg .. ' ' .. tostring(prop) .. ' in PUBREL')      -- Protocol Error
        end
        if self.logger then
            self:_log_props(props)
        end
    end
    return {
        type  = 'PUBREL',
        id    = id,
        rc    = rc,
        props = props,
    }
end

function mt:_parse_pubcomp (s)
    local id, rc, props
    if #s == 2 then
        id = unpack('>I2', s)
        rc = 0
    else
        id, rc = unpack('>I2I1', s)
    end
    if self.logger then
        self.logger:debug('MQTT< PUBCOMP ' .. tostring(id) .. ' ' .. tostring(rc))
    end
    if #s > 3 then
        local pos
        props, pos = decode_props(s, 4)
        if not props then
            return self:_hangup(129, 'invalid data in PUBCOMP ' .. tostring(pos))       -- Malformed Packet
        end
        local res, msg, prop = valid_props(props, valid_prop_ack)
        if not res then
            return self:_hangup(130, msg .. ' ' .. tostring(prop) .. ' in PUBCOMP')     -- Protocol Error
        end
        if self.logger then
            self:_log_props(props)
        end
    end
    return {
        type  = 'PUBCOMP',
        id    = id,
        rc    = rc,
        props = props,
    }
end

local valid_reason_suback = {
    [0]   = true,       -- Granted QoS 0
    [1]   = true,       -- Granted QoS 1
    [2]   = true,       -- Granted QoS 2
    [128] = true,       -- Unspecified error
    [131] = true,       -- Implementation specific error
    [135] = true,       -- Not authorized
    [143] = true,       -- Topic Filter invalid
    [145] = true,       -- Packet Identifier in use
    [151] = true,       -- Quota exceeded
    [158] = true,       -- Shared Subscriptions not supported
    [161] = true,       -- Subscription Identifiers not supported
    [162] = true,       -- Wildcard Subscriptions not supported
}

function mt:_parse_suback (s)
    local id = unpack('>I2', s)
    local props, pos = decode_props(s, 3)
    if not props then
        return self:_hangup(129, 'invalid data in SUBACK ' .. tostring(pos))            -- Malformed Packet
    end
    local resp = { byte(s, pos, #s) }
    for i = 1, #resp do
        local v = resp[i]
        if not valid_reason_suback[v] then
            return self:_hangup(130, 'invalid reason in SUBACK: ' .. tostring(v))       -- Protocol Error
        end
    end
    if self.logger then
        local t = {}
        for i = 1, #resp do
            t[#t+1] = tostring(resp[i])
        end
        self.logger:debug('MQTT< SUBACK ' .. tostring(id) .. ' ' .. concat(t, ' '))
    end
    local res, msg, prop = valid_props(props, valid_prop_ack)
    if not res then
        return self:_hangup(130, msg .. ' ' .. tostring(prop) .. ' in SUBACK')          -- Protocol Error
    end
    if self.logger then
        self:_log_props(props)
    end
    return {
        type    = 'SUBACK',
        id      = id,
        props   = props,
        payload = resp,
    }
end

local valid_reason_unsuback = {
    [0]   = true,       -- Success
    [17]  = true,       -- No subscription existed
    [128] = true,       -- Unspecified error
    [131] = true,       -- Implementation specific error
    [135] = true,       -- Not authorized
    [143] = true,       -- Topic Filter invalid
    [145] = true,       -- Packet Identifier in use
}

function mt:_parse_unsuback (s)
    local id = unpack('>I2', s)
    local props, pos = decode_props(s, 3)
    if not props then
        return self:_hangup(129, 'invalid data in UNSUBACK ' .. tostring(pos))          -- Malformed Packet
    end
    local resp = { byte(s, pos, #s) }
    for i = 1, #resp do
        local v = resp[i]
        if not valid_reason_unsuback[v] then
            return self:_hangup(130, 'invalid reason in UNSUBACK: ' .. tostring(v))     -- Protocol Error
        end
    end
    if self.logger then
        local t = {}
        for i = 1, #resp do
            t[#t+1] = tostring(resp[i])
        end
        self.logger:debug('MQTT< UNSUBACK ' .. tostring(id) .. ' ' .. concat(t, ' '))
    end
    local res, msg, prop = valid_props(props, valid_prop_ack)
    if not res then
        return self:_hangup(130, msg .. ' ' .. tostring(prop) .. ' in UNSUBACK')        -- Protocol Error
    end
    if self.logger then
        self:_log_props(props)
    end
    return {
        type    = 'UNSUBACK',
        id      = id,
        props   = props,
        payload = resp,
    }
end

function mt:_parse_pingresp ()
    if self.logger then
        self.logger:debug('MQTT< PINGRESP')
    end
    return {
        type = 'PINGRESP',
    }
end

local valid_reason_rdisconnect = {
    [0]   = true,       -- Normal disconnection
    [128] = true,       -- Unspecified error
    [129] = true,       -- Malformed Packet
    [130] = true,       -- Protocol Error
    [131] = true,       -- Implementation specific error
    [135] = true,       -- Not authorized
    [137] = true,       -- Server busy
    [139] = true,       -- Server shutting down
    [141] = true,       -- Keep Alive timeout
    [142] = true,       -- Session taken over
    [143] = true,       -- Topic Filter invalid
    [144] = true,       -- Topic Name invalid
    [147] = true,       -- Receive Maximum exceeded
    [148] = true,       -- Topic Alias invalid
    [149] = true,       -- Packet too large
    [150] = true,       -- Message rate too high
    [151] = true,       -- Quota exceeded
    [152] = true,       -- Administrative action
    [153] = true,       -- Payload format invalid
    [154] = true,       -- Retain not supported
    [155] = true,       -- QoS not supported
    [156] = true,       -- Use another server
    [157] = true,       -- Server moved
    [158] = true,       -- Shared Subscriptions not supported
    [159] = true,       -- Connection rate exceeded
    [160] = true,       -- Maximum connect time
    [161] = true,       -- Subscription Identifiers not supported
    [162] = true,       -- Wildcard Subscriptions not supported
}

function mt:_parse_disconnect (s)
    local rc = s and unpack('>I1', s) or 0
    if not valid_reason_rdisconnect[rc] then
        return self:_hangup(130, 'invalid reason in DISCONNECT: ' .. tostring(rc))      -- Protocol Error
    end
    if self.logger then
        self.logger:debug('MQTT< DISCONNECT ' .. tostring(rc))
    end
    local props
    if s and #s > 1 then
        local pos
        props, pos = decode_props(s, 2)
        if not props then
            return self:_hangup(129, 'invalid data in DISCONNECT ' .. tostring(pos))    -- Malformed Packet
        end
        local res, msg, prop = valid_props(props, valid_prop_disconnect)
        if not res then
            return self:_hangup(130, msg .. ' ' .. tostring(prop) .. ' in DISCONNECT')  -- Protocol Error
        end
        if self.logger then
            self:_log_props(props)
        end
    end
    return {
        type  = 'DISCONNECT',
        rc    = rc,
        props = props,
    }
end

local valid_reason_rauth = {
    [0]  = true,        -- Success
    [24] = true,        -- Continue authentication
}

function mt:_parse_auth (s)
    local rc = s and unpack('>I1', s) or 0
    if not valid_reason_rauth[rc] then
        return self:_hangup(130, 'invalid reason in AUTH: ' .. tostring(rc))            -- Protocol Error
    end
    if self.logger then
        self.logger:debug('MQTT< AUTH ' .. tostring(rc))
    end
    local props
    if s and #s > 1 then
        local pos
        props, pos = decode_props(s, 2)
        if not props then
            return self:_hangup(129, 'invalid data in AUTH ' .. tostring(pos))          -- Malformed Packet
        end
        local res, msg, prop = valid_props(props, valid_prop_auth)
        if not res then
            return self:_hangup(130, msg .. ' ' .. tostring(prop) .. ' in AUTH')        -- Protocol Error
        end
        if self.logger then
            self:_log_props(props)
        end
    end
    return {
        type  = 'AUTH',
        rc    = rc,
        props = props,
    }
end

local expected_flags = {
    [0x2] = 0x0,        -- connack
    [0x4] = 0x0,        -- puback
    [0x5] = 0x0,        -- pubrec
    [0x6] = 0x2,        -- pubrel
    [0x7] = 0x0,        -- pubcomp
    [0x9] = 0x0,        -- suback
    [0xB] = 0x0,        -- unsuback
    [0xD] = 0x0,        -- pingresp
    [0xE] = 0x0,        -- disconnect
    [0xF] = 0x0,        -- auth
}

local expected_length = {
    [0xD] = 0,          -- pingresp
}

local min_length = {
    [0x2] = 3,          -- connack
    [0x3] = 4,          -- publish
    [0x4] = 2,          -- puback
    [0x5] = 2,          -- pubrec
    [0x6] = 2,          -- pubrel
    [0x7] = 2,          -- pubcomp
    [0x9] = 4,          -- suback
    [0xB] = 4,          -- unsuback
}

function mt:_fetch ()
    local ch, msg = self.socket:read(1)
    if not ch then
        return self:_error(msg)
    end
    local b = byte(ch)
    local ptype = b >> 4
    local flags = b & 0x0F
    local expected_flag = expected_flags[ptype]
    if expected_flag and expected_flag ~= flags then
        return self:_hangup(129, format('invalid flags: %02X', b))      -- Malformed Packet
    end

    local rem_len = 0
    local mult = 1
    local nb = 0
    repeat
        ch, msg = self.socket:read(1)
        if not ch then
            return self:_error(msg)
        end
        local v = byte(ch)
        rem_len = rem_len + (v & 0x7F) * mult
        mult = mult * 0x80
        nb = nb + 1
        if nb >= 4 then
            return self:_hangup(129, 'invalid vbi: too long')           -- Malformed Packet
        end
    until (v & 0x80) == 0

    local expected_len = expected_length[ptype]
    if expected_len and expected_len ~= rem_len then
        return self:_hangup(129, format('invalid length %d for: %02X', rem_len, b))     -- Malformed Packet
    end
    local min_len = min_length[ptype]
    if min_len and rem_len < min_len then
        return self:_hangup(129, format('invalid length %d for: %02X', rem_len, b))     -- Malformed Packet
    end

    if rem_len > self.session.client.maximum_packet_size then
        return self:_hangup(149, 'packet too large: ' .. tostring(rem_len))             -- Packet too large
    end

    local s
    if rem_len > 0 then
        s, msg = self.socket:read(rem_len)
        if not s or #s ~= rem_len then
            return self:_error(msg or "missing data")
        end
    end
    return ptype, flags, s
end

function mt:read ()
    local ptype, flags, s = self:_fetch()
    if not ptype then
        return nil, flags
    end
    local ret, msg
    if     ptype == 0x2 then
        if self.connack_received then
            return self:_hangup(130, 'CONNACK already received')                -- Protocol Error
        end
        ret, msg = self:_parse_connack(s)
        if ret then
            if ret.rc == 0 then
                self.connack_received = true
                self.session_present = ret.sp
                if self.clean and self.session_present then
                    return self:_hangup(130, 'invalid CONNACK (sp but clean)')      -- Protocol Error
                end
                local session = self.session
                local props = ret.props
                for j = 1, #props, 2 do
                    local i, v = props[j], props[j+1]
                    if     i == 17 then
                        session.server.session_expiry_interval = v
                    elseif i == 18 then
                        if self.id == '' then
                            self.id = v
                        else
                            return self:_hangup(130, 'client id in CONNACK')        -- Protocol Error
                        end
                    elseif i == 33 then
                        session.server.receive_maximum = v
                    elseif i == 34 then
                        session.server.topic_alias_maximum = v
                    elseif i == 36 then
                        session.server.maximum_qos = v
                    elseif i == 37 then
                        session.server.retain_available = (v == 1)
                    elseif i == 39 then
                        session.server.maximum_packet_size = v
                    elseif i == 40 then
                        session.server.wildcard_subscription_available = (v == 1)
                    elseif i == 41 then
                        session.server.subscription_identifiers_available = (v == 1)
                    elseif i == 42 then
                        session.server.shared_subscription_available = (v == 1)
                    end
                end
                if self.id == '' then
                    return self:_hangup(130, 'no client id in CONNACK')             -- Protocol Error
                end
                for i = 1, #session.queue do
                    local id = session.queue[i]
                    local pt = session.ptype[id]
                    if pt == 'publish' then
                        local t = session.publish[id]
                        local options = t[3]
                        local f = 0x8 | (options.qos << 1) | (options.retain and 0x1 or 0x0)
                        self:_send_publish(f, id, t[1], t[2], t[4])
                    elseif pt == 'pubrel' then
                        self:_send_pubrel(id, 0)
                    end
                end
                for i = 1, #session.rqueue do
                    local id = session.rqueue[i]
                    self:_send_pubrec(id)
                end
                session.ralias = {}
            end
            self:_connect(ret.rc, ret.sp, ret.props)
        end
    elseif ptype == 0x3 then
        if not self.connack_received then
            return self:_hangup(130, 'CONNACK not received')                    -- Protocol Error
        end
        ret, msg = self:_parse_publish(s, flags)
        if ret then
            local session = self.session
            local qos = ret.qos
            local id = ret.id
            local topic = ret.topic
            local payload = ret.payload
            local props = ret.props
            local rc = 0        -- Success
            local max_qos = -1
            for j = 1, #props, 2 do
                local i, v = props[j], props[j+1]
                if     i == 1 then
                    if v == 1 and not utf8_len(payload) then
                        rc = 153        -- Payload format invalid
                    end
                elseif i == 11 then
                    local sqos = session.subid[v]
                    if not sqos then
                        return self:_hangup(130, 'unknown Subscription Identifier ' .. tostring(v) .. ' in PUBLISH')    -- Protocol Error
                    end
                    if sqos > max_qos then
                        max_qos = sqos
                    end
                elseif i == 35 then
                    if v == 0 or v > session.client.topic_alias_maximum then
                        return self:_hangup(148, 'topic alias invalid')         -- Topic Alias invalid
                    end
                    if topic ~= '' then
                        session.ralias[v] = topic
                    else
                        topic = session.ralias[v]
                        if not topic then
                            return self:_hangup(130, 'unknown Topic Alias ' .. tostring(v) .. ' in PUBLISH')    -- Protocol Error
                        end
                        ret.topic = topic
                    end
                end
            end
            if max_qos == -1 then
                max_qos = session.client.maximum_qos
            end
            if qos > max_qos then
                return self:_hangup(155, 'QoS not supported')                   -- QoS not supported
            end
            if not valid_topic_name(topic, true) then
                rc = 144        -- Topic Name invalid
            end
            if qos == 0 then
                self:_message(topic, payload, props)
            elseif qos == 1 then
                self:_message(topic, payload, props)
                self:_send_puback(id, rc)
            else -- qos == 2
                if #session.rqueue >= session.client.receive_maximum then
                    return self:_hangup(147, 'receive maximum exceeded')        -- Receive Maximum exceeded
                end
                if not session.pubrec[id] then
                    self:_message(topic, payload, props)
                end
                insert(session.rqueue, id)
                session.pubrec[id] = true
                self:_send_pubrec(id, rc)
            end
        end
    elseif ptype == 0x4 then
        if not self.connack_received then
            return self:_hangup(130, 'CONNACK not received')                    -- Protocol Error
        end
        ret, msg = self:_parse_puback(s)
        if ret then
            local id = ret.id
            local session = self.session
            if not self.session_present and not session.publish[id] then
                return self:_hangup(130, 'mismatch for PUBLISH/PUBACK')         -- Protocol Error
            end
            session.publish[id] = nil
            session.ptype[id] = nil
            remove_id(session.queue, id)
        end
    elseif ptype == 0x5 then
        if not self.connack_received then
            return self:_hangup(130, 'CONNACK not received')                    -- Protocol Error
        end
        ret, msg = self:_parse_pubrec(s)
        if ret then
            local id = ret.id
            local session = self.session
            if not self.session_present and not session.publish[id] then
                return self:_hangup(130, 'mismatch for PUBLISH/PUBREC')         -- Protocol Error
            end
            local rc = session.publish[id] and 0 or 146                         -- Packet Identifier not found
            remove_id(session.queue, id)
            insert(session.queue, id)
            session.ptype[id] = 'pubrel'
            session.pubrel[id] = session.publish[id]
            session.publish[id] = nil
            self:_send_pubrel(id, rc)
        end
    elseif ptype == 0x6 then
        if not self.connack_received then
            return self:_hangup(130, 'CONNACK not received')                    -- Protocol Error
        end
        ret, msg = self:_parse_pubrel(s)
        if ret then
            local id = ret.id
            local session = self.session
            if not self.session_present and not session.pubrec[id] then
                return self:_hangup(130, 'mismatch for PUBREC/PUBREL')          -- Protocol Error
            end
            local rc = session.pubrec[id] and 0 or 146                          -- Packet Identifier not found
            session.pubrec[id] = nil
            remove_id(session.rqueue, id)
            self:_send_pubcomp(id, rc)
        end
    elseif ptype == 0x7 then
        if not self.connack_received then
            return self:_hangup(130, 'CONNACK not received')                    -- Protocol Error
        end
        ret, msg = self:_parse_pubcomp(s)
        if ret then
            local id = ret.id
            local session = self.session
            if not self.session_present and not session.pubrel[id] then
                return self:_hangup(130, 'mismatch for PUBREL/PUBCOMP')         -- Protocol Error
            end
            session.pubrel[id] = nil
            session.ptype[id] = nil
            remove_id(session.queue, id)
        end
    elseif ptype == 0x9 then
        if not self.connack_received then
            return self:_hangup(130, 'CONNACK not received')                    -- Protocol Error
        end
        ret, msg = self:_parse_suback(s)
        if ret then
            local id = ret.id
            local payload = ret.payload
            local session = self.session
            local t = session.subscribe[id] or {}
            local list = t[1] or {}
            if #list ~= 2 * #payload then
                return self:_hangup(130, 'mismatch for SUBSCRIBE/SUBACK')       -- Protocol Error
            end
            for i = 1, #payload do
                local topic = list[2 * i - 1]
                session.subscribe[topic] = payload[i]
            end
            session.subscribe[id] = nil
            session.ptype[id] = nil
            remove_id(session.queue, id)
        end
    elseif ptype == 0xB then
        if not self.connack_received then
            return self:_hangup(130, 'CONNACK not received')                    -- Protocol Error
        end
        ret, msg = self:_parse_unsuback(s)
        if ret then
            local id = ret.id
            local session = self.session
            local t = session.unsubscribe[id] or {}
            local list = t[1] or {}
            if #list ~= #ret.payload then
                return self:_hangup(130, 'mismatch for UNSUBSCRIBE/UNSUBACK')   -- Protocol Error
            end
            for i = 1, #list do
                local topic = list[i]
                session.subscribe[topic] = nil
            end
            session.unsubscribe[id] = nil
            session.ptype[id] = nil
            remove_id(session.queue, id)
        end
    elseif ptype == 0xD then
        if not self.connack_received then
            return self:_hangup(130, 'CONNACK not received')                    -- Protocol Error
        end
        ret, msg = self:_parse_pingresp()
    elseif ptype == 0xE then
        ret, msg = self:_parse_disconnect(s)
        if ret then
            self:_disconnect(ret.rc, ret.props)
        end
    elseif ptype == 0xF then
        if self.connack_received then
            return self:_hangup(130, 'CONNACK already received')                -- Protocol Error
        end
        ret, msg = self:_parse_auth(s)
    else
        return self:_hangup(129, format('invalid packet control: %01X%01X', ptype, flags))      -- Malformed Packet
    end
    return ret, msg
end

local function new (t)
    if type(t) ~= 'table' then
        typeerror('new', 1, t, 'table')
    end
    local socket = t.socket
    if not socket or not valid_socket(socket) then
        error("invalid socket (" .. tostring(socket) .. ") in table to 'new'")
    end
    local logger = t.logger
    if logger and not valid_logger(logger) then
        error("invalid logger (" .. tostring(logger) .. ") in table to 'new'")
    end
    local on_connect = t.on_connect
    if on_connect and type(on_connect) ~= 'function' then
        error("invalid on_connect (" .. tostring(on_connect) .. ") in table to 'new'")
    end
    local on_message = t.on_message
    if on_message and type(on_message) ~= 'function' then
        error("invalid on_message (" .. tostring(on_message) .. ") in table to 'new'")
    end
    local on_disconnect = t.on_disconnect
    if on_disconnect and type(on_disconnect) ~= 'function' then
        error("invalid on_disconnect (" .. tostring(on_disconnect) .. ") in table to 'new'")
    end
    local on_error = t.on_error
    if on_error and type(on_error) ~= 'function' then
        error("invalid on_error (" .. tostring(on_error) .. ") in table to 'new'")
    end

    local obj = {
        logger        = logger,
        socket        = socket,
        on_connect    = on_connect,
        on_message    = on_message,
        on_disconnect = on_disconnect,
        on_error      = on_error,
        session       = new_session(),
    }
    setmetatable(obj, {
        __index = mt,
    })
    return obj
end

local m = {}

function m.match (name, filter)
    if type(name) ~= 'string' then
        typeerror('match', 1, name, 'string')
    end
    if not valid_topic_name(name, true) then
        error("invalid Topic Name to 'match'")
    end
    if type(filter) ~= 'string' then
        typeerror('match', 2, filter, 'string')
    end
    if not valid_topic_filter(filter) then
        error("invalid Topic Filter to 'match'")
    end

    local iter_n = utf8_codes(name)
    local i, c = iter_n(name, 0)
    local iter_f = utf8_codes(filter)
    local j, p = iter_f(filter, 0)
    if (p == 36 and c ~= 36) or (c == 36 and p ~= 36) then      -- $
        return false
    end
    while j do
        if p ~= c then
            if p == 35 then             -- #
                return true
            elseif p == 43 then         -- +
                j, p = iter_f(filter, j)
                while i and c ~= 47 do  -- /
                    i, c = iter_n(name, i)
                end
            else
                return false
            end
        else
            i, c = iter_n(name, i)
            j, p = iter_f(filter, j)
            if not i and p == 47 then   -- /
                local _, _p = iter_f(filter, j)
                if _p == 35 then        -- #
                    return true         -- foo matching foo/#
                end
            end
        end
    end
    return not i
end

m.PORT = '1883'
m.PORT_TLS = '8883'

m.PROP = {
    PAYLOAD_FORMAT_INDICATOR     = 1,
    MESSAGE_EXPIRY_INTERVAL      = 2,
    CONTENT_TYPE                 = 3,
    RESPONSE_TOPIC               = 8,
    CORRELATION_DATA             = 9,
    SUBSCRIPTION_IDENTIFIER      = 11,
    SESSION_EXPIRY_INTERVAL      = 17,
    ASSIGNED_CLIENT_IDENTIFIER   = 18,
    SERVER_KEEP_ALIVE            = 19,
    AUTHENTICATION_METHOD        = 21,
    AUTHENTICATION_DATA          = 22,
    REQUEST_PROBLEM_INFORMATION  = 23,
    WILL_DELAY_INTERVAL          = 24,
    REQUEST_RESPONSE_INFORMATION = 25,
    RESPONSE_INFORMATION         = 26,
    SERVER_REFERENCE             = 28,
    REASON_STRING                = 31,
    RECEIVE_MAXIMUM              = 33,
    TOPIC_ALIAS_MAXIMUM          = 34,
    TOPIC_ALIAS                  = 35,
    MAXIMUM_QOS                  = 36,
    RETAIN_AVAILABLE             = 37,
    USER_PROPERTY                = 38,
    MAXIMUM_PACKET_SIZE          = 39,
    WILDCARD_SUB_AVAILABLE       = 40,
    SUBSCRIPTION_ID_AVAILABLE    = 41,
    SHARED_SUB_AVAILABLE         = 42,
}

setmetatable(m, {
    __call = function (_, t) return new(t) end
})

m._NAME = ...
m._VERSION = "0.3.2"
m._DESCRIPTION = "lua-mqtt : client library for MQTT 5"
m._COPYRIGHT = "Copyright (c) 2022-2024 Francois Perrad"
return m
--
-- This library is licensed under the terms of the MIT/X11 license,
-- like Lua itself.
--
